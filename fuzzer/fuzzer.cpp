#include <string>
#include <string.h>
#include <cstring>
#include <cstdio>
#include <cstdint>
#include <cstdlib>

#include <api.h>
#include <value.h>

class BinaryReader
{
public:
    class EndOfStream : public std::runtime_error
    {
    public:
        EndOfStream() :
                std::runtime_error("End of stream")
        {}
    };

    BinaryReader() {}

    BinaryReader(const uint8_t* data, size_t size) :
            data((const char*)data, size)
    {
        this->data[size - 1] = '\0';
    }

    std::string readString()
    {
        if (data.size() - cursor < 1)
            throw EndOfStream();

        std::string result(data.c_str() + cursor);
        cursor += result.size() + 1;
        return result;
    }

    template<typename T>
    T read()
    {
        if (data.size() - cursor < sizeof(T))
            throw EndOfStream();

        T result = *((T*)(data.c_str() + cursor));
        cursor += sizeof(T);
        return result;
    }

private:
    std::string data;
    int cursor = 0;
};

BinaryReader input;

int read_value(value_t* return_value)
{
    memset(return_value, 0, sizeof(value_t));
    return_value->type = input.read<type_id_t>();
    return_value->properties = input.read<int8_t>();
    switch(return_value->type)
    {
        case Value::INTEGER: return_value->int_value = input.read<int64_t>(); break;
        case Value::FLOAT: return_value->float_value = input.read<double>(); break;
        case Value::BOOLEAN: return_value->bool_value = input.read<int8_t>(); break;
        case Value::STRING:
        {
            std::string str = input.readString();
            size_t len = str.size() + 1;
            return_value->str_value = (char*) malloc(len);
            memcpy(return_value->str_value, str.c_str(), len);
            break;
        }
        case Value::DATE:
        {
            return_value->date.year = input.read<int16_t>();
            return_value->date.month = input.read<int8_t>();
            return_value->date.day = input.read<int8_t>();
        }
        case Value::LOCATION:
        {
            return_value->location.country = input.read<int64_t>();
            return_value->location.province = input.read<int64_t>();
        }
        case Value::GPS:
        {
            return_value->gps.latitude = input.read<double>();
            return_value->gps.longitude = input.read<double>();
        }
        default: break;
    }

    return 0;
}

int get_ind_value(int indicator_id, value_t* return_value)
{
    try
    {
        return read_value(return_value);
    }
    catch (const BinaryReader::EndOfStream&)
    {
        return USER_ERROR;
    }
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) 
{
    if (size < 1) return 0;

    input = BinaryReader(data, size);
    std::string formula = input.readString();

    callbacks_t callbacks;
    memset(&callbacks, 0, sizeof(callbacks_t));
    callbacks.get_ind_value = get_ind_value;
    void* interpreter = create_interpreter(&callbacks);

    parse_result_t parse_result;
    parse_formula(interpreter, formula.c_str(), &parse_result);
    if (parse_result.ast == nullptr)
    {
        destroy_error(&parse_result.error);
        destroy_interpreter(interpreter);
        return 0;
    }
    destroy_error(&parse_result.error);

    collect_result_t collect_result;
    collect_requirements(interpreter, parse_result.ast, 1, 1, 1, &collect_result);
    destroy_error(&collect_result.error);

    analyze_result_t analyze_result;
    indicator_id_t slave_indicator_id = 0;
    analyze_formula(interpreter, parse_result.ast, slave_indicator_id, &analyze_result);
    destroy_error(&analyze_result.error);
    destroy_error(&analyze_result.report);

    evaluate_result_t evaluate_result;
    evaluate_formula(interpreter, parse_result.ast, &evaluate_result);
    destroy_error(&evaluate_result.error);
    destroy_value(&evaluate_result.value);

    destroy_ast(parse_result.ast);
    destroy_interpreter(interpreter);

    return 0;
}
