#include <sstream>
#include <parser.h>
#include <evaluator.h>
#include <emscripten/bind.h>

using namespace emscripten;

Value marshal_value(const val& src) {
    std::string type = src.typeOf().as<std::string>();
    if (type == "number")
        return Value(src.as<double>());
    if (type == "string")
        return Value(src.as<std::string>().c_str());
    return Value();
}

val unmarshal_value(const Value& src) {
    if (src.type() == Value::INTEGER) return val((long)src.getInt());
    if (src.type() == Value::FLOAT) return val(src.getFloat());
    if (src.type() == Value::STRING) return val(src.getString());
    return val::undefined();
}

struct DataProviderWrapper : wrapper<DataProvider> {
    EMSCRIPTEN_WRAPPER(DataProviderWrapper);

    Value getIndicatorValue(int indicator_id, int position) const {
        val value = call<val>("getIndicatorValue", indicator_id, position);
        return marshal_value(value);
    }

    Value getExtIndicatorValue(int indicator_id, Date from, Date to, int position) const {
        val value = call<val>("getExtIndicatorValue", indicator_id, from, to, position);
        return marshal_value(value);
    }

    Value getExtScorecardValue(int scorecard_id, int position) const {
        val value = call<val>("getExtScorecardValue", scorecard_id, position);
        return marshal_value(value);
    }
};

EMSCRIPTEN_BINDINGS(formula_binding) {
    class_<Parser>("Parser")
        .constructor()
        .function("parse", optional_override([](Parser& self, const std::string& formula){
            val result = val::object();
            try {
                result.set("ast", self.parse(formula.c_str()).release());
                return result;
            }
            catch (const ParseException& e) {
                result.set("error", e.what());
                result.set("position", e.position);
                return result;
            }
        }), allow_raw_pointers())
        ;
    function("evaluate", optional_override([](const Expression& expression, DataProvider* data_provider){
        Value result = Evaluator::evaluate(expression, data_provider);
        return unmarshal_value(result);
    }), allow_raw_pointers());
    class_<Expression>("Expression");
    class_<DataProvider>("DataProvider")
        .function("getIndicatorValue", &DataProvider::getIndicatorValue, pure_virtual())
        .function("getExtIndicatorValue", &DataProvider::getExtIndicatorValue, pure_virtual())
        .function("getExtScorecardValue", &DataProvider::getExtScorecardValue, pure_virtual())
        .allow_subclass<DataProviderWrapper>("DataProviderWrapper")
        ;
}
