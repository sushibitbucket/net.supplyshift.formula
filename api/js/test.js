lib = require(process.cwd() + '/libformula.js');
parser = new lib.Parser();

function evaluate(formula, answers) {
    dataProvider = lib.DataProvider.implement({
        getIndicatorValue: function(indicatorId, position) {
            return answers[indicatorId];
        }
    });

    console.log('\nInput:', formula);
    result = parser.parse(formula);
    if (result.error) {
        console.log('Error:', result.error, 'at', result.position);
        return;
    }
    value = lib.evaluate(result.ast, dataProvider);
    console.log('Result:', value);
    return value;
}

console.assert(evaluate("2 + 2.5", {}) === 4.5);
console.assert(evaluate("2 + IND(123)", {123: 10}) === 12);
console.assert(evaluate("IND(1)", {1: 'Hello'}) === 'Hello');
console.assert(parser.parse("2 / test").position === 4);
