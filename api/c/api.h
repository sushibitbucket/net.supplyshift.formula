//
// Created by aegorov on 15.08.17.
//

#ifndef FORMULA_API_H
#define FORMULA_API_H

#include <inttypes.h>

#if defined(__GNUC__)
#define CDECL __attribute__((__cdecl__))
#else
#define CDECL __cdecl
#endif

#define FORMULA_API CDECL

extern "C" {

#pragma pack(push, 1)

typedef int8_t type_id_t;
typedef int32_t indicator_id_t;
typedef int32_t scorecard_id_t;

struct _value_t;

typedef struct
{
    int32_t size;
    struct _value_t* values;
} value_array_t;

typedef struct
{
    int16_t year;
    int8_t month;
    int8_t day;
} value_date_t;

typedef struct
{
    int64_t country;
    int64_t province;
} value_location_t;

typedef struct
{
    double latitude;
    double longitude;
} value_gps_t;

typedef struct _value_t
{
    type_id_t type; //! @see Value::Type
    int8_t properties; //! bitfield @see Value::Property
    union
    {
        int64_t int_value;
        double float_value;
        int8_t bool_value;
        char* str_value;
        value_array_t array;
        value_array_t multi_select;
        value_date_t date;
        value_location_t location;
        value_gps_t gps;
    };
} value_t;

typedef enum
{
    SUCCESS = 0,
    OUT_OF_MEMORY = 1,
    PARSE_ERROR = 2, // syntax error (parentheses mismatch or unknown tokens)
    NO_CALLBACK_ERROR = 4,
    MARSHAL_ERROR = 5, // error occured during values marshaling (i.e wrong value type id).
    INTERNAL_ERROR = 7, // used when something unexpected is happen. It's a library bug if it's returned.
    WRONG_TYPE = 9, // happens in formulas like "IND(1) + 1" when numeric indicator 1 has no answer (blank)
    DIVIDE_BY_ZERO = 10,
    USER_ERROR = 256 // users can return custom error codes from callbacks starting from this offset
} error_code_e;

typedef enum
{
    ANALYSIS_OK = 0,
    ANALYSIS_WRONG_ARGUMENTS,
    ANALYSIS_TYPE_MISMATCH,
    ANALYSIS_ALLOWED_VALUES_MISMATCH
} analysis_result_e;

typedef struct
{
    int32_t code; // error code, depending on context, it's error_code_e or analysis_result_e.
    char* message;
    int16_t annotation_offset;
    int16_t annotation_length;
    union {
        struct {
            indicator_id_t indicator_id; // indicator which answer caused an error, -1 if not set
        } wrong_type;
        struct {
            indicator_id_t indicator_id; // indicator which is zero, -1 if not set
        } divide_by_zero;
    };
} formula_error_t;

typedef struct
{
    formula_error_t error; // code is error_code_e
    void* ast;
} parse_result_t;

typedef struct
{
    formula_error_t error; // code is error_code_e
    value_t value;
} evaluate_result_t;

typedef struct
{
    formula_error_t error; // code is error_code_e
    formula_error_t report; // code is analysis_result_e
} analyze_result_t;

typedef struct
{
    indicator_id_t indicator_id;
    value_date_t from; // zero date when from argument is elided
    value_date_t to; // zero date when to argument is elided
} extind_arguments_t;

typedef struct
{
    const indicator_id_t* ind;
    int32_t ind_count;

    const extind_arguments_t* extind;
    int32_t extind_count;

    const scorecard_id_t* extscr;
    int32_t extscr_count;
} requirements_t;

typedef struct
{
    formula_error_t error; // code is error_code_e
    requirements_t requirements;
} collect_result_t;

typedef struct
{
    /*
     * Answers data type produced by the indicator. 
     * It's based on metric question type (look for full mapping table in README).
     */
    type_id_t type;
    
    /*
     * Used when type is ARRAY or MULTI_SELECT, otherwise should be NONE
     */
    type_id_t element_type;
    
    /*
     * Allowed indicator values. 
     * Should also contain special values like Blank, NotApplicable, NoneOfAbove if it's allowed.
     */
    value_array_t allowed_values;
} indicator_info_t;

/*
 * This callback is called when IND function is being evaluated (evaluate_formula).
 * value_t instance written to result will be destroyed by the library.
 *
 * \return 0 on success. Any other value will be forwarded to formula_result_t.code as it is.
 */
typedef int32_t (CDECL* get_ind_value_t)(indicator_id_t indicator_id, value_t* result);

/*
 * This callback is called when EXTIND function is being evaluated (evaluate_formula).
 * value_t instance written to result will be destroyed by the library.
 *
 * \param from can be null if EXTIND doesn't have this argument.
 * \param to can be null if EXTIND doesn't have this argument.
 * \return 0 on success. Any other value will be forwarded to formula_result_t.code as it is.
 */
typedef int32_t (CDECL* get_extind_value_t)(indicator_id_t indicator_id, value_date_t* from, value_date_t* to, value_t* result);

/*
 * This callback is called when EXTSCR function is being evaluated (evaluate_formula).
 * value_t instance written to result will be destroyed by the library.
 *
 * \return 0 on success. Any other value will be forwarded to formula_result_t.code as it is.
 */
typedef int32_t (CDECL* get_extscr_value_t)(scorecard_id_t scorecard_id, value_t* result);

/*
 * This callback is called for all indicators used in IND and EXTIND functions during
 * formula validation (analyze_formula).
 * value_t instances written to result will be destroyed by the library.
 *
 * \return 0 on success. Any other value will be forwarded to formula_result_t.code as it is.
 */
typedef int32_t (CDECL* get_indicator_info_t)(indicator_id_t indicator_id, indicator_info_t* result);

typedef struct
{
    get_ind_value_t get_ind_value;
    get_extind_value_t get_extind_value;
    get_extscr_value_t get_extscr_value;
    get_indicator_info_t get_indicator_info;
} callbacks_t;

#pragma pack(pop)

/*
 * Create interpreter instance. It should be destroyed using destroy_interpreter function.
 * \param callbacks non-null pointer to callbacks_t structure.
 */
void* FORMULA_API create_interpreter(callbacks_t* callbacks);

/*
 * Parse formula.
 * Returned ast and error should be destroyed by the caller using destroy_ast and destroy_error.
 *
 * In case of success result->ast will store valid ast instance and result->error.code will be equal to 0.
 * Otherwise one of the following error codes will be returned via result->error.code:
 * - PARSE_ERROR
 * - OUT_OF_MEMORY
 * - INTERNAL_ERROR
 */
void FORMULA_API parse_formula(void* interpreter, const char* formula, parse_result_t* result);

/*
 * Evaluate formula.
 * Returned value and error should be destroyed by the caller using destroy_value and destroy_error.
 * It does require following callbacks:
 * - get_ind_value (if IND function is used)
 * - get_extind_value (if EXTIND function is used)
 * - get_extscr_value (if EXTSCR function is used)
 *
 * In case of success result->value will store calculated result and result->error.code will be equal to 0.
 * Otherwise one of the following error codes will be returned via result->error.code:
 * - WRONG_TYPE
 * - DIVIDE_BY_ZERO
 * - EVALUATE_ERROR
 * - MARSHAL_ERROR
 * - OUT_OF_MEMORY
 * - INTERNAL_ERROR
 * - Any other value returned by callbacks.
 */
void FORMULA_API evaluate_formula(void* interpreter, void* ast, evaluate_result_t* result);

/*
 * Analyze (validate) formula.
 * Returned report and error should be destroyed by the caller using destroy_error.
 * It does require get_indicator_info callback if IND or EXTIND functions are used in formula.
 *
 * In case of success result->report will store analysis result (analysis_result_e)
 * and result->error.code will be equal to 0.
 * Otherwise one of the following error codes will be returned via result->error.code:
 * - OUT_OF_MEMORY
 * - INTERNAL_ERROR
 * - Any other value returned by callbacks.
 */
void FORMULA_API analyze_formula(void* interpreter, void* ast, indicator_id_t slave_indicator_id, analyze_result_t* result);

/*
 * Collect all formula requirements (IND, EXTIND, EXTSCR).
 * \param collect_ind should be equal to 1 or 0
 * \param collect_extind should be equal to 1 or 0
 * \param collect_extscr should be equal to 1 or 0
 *
 * collect_result_t::error should be destroyed by the caller using destroy_error function.
 * collect_result_t::requirements structure is owned by the interpreter, and will become invalid
 * on next collect_requirements call or destroy_interpreter.
 */
void FORMULA_API collect_requirements(void* interpreter, void* ast, int8_t collect_ind, int8_t collect_extind,
                                      int8_t collect_extscr, collect_result_t* result);

/*
 * Destroy interpreter instance created with create_interpreter.
 */
void FORMULA_API destroy_interpreter(void* interpreter);

/*
 * Destroy ast instance create by parse_formula.
 */
void FORMULA_API destroy_ast(void* ast);

/*
 * Destroy formula_error_t instance. Use it to destroy all formula_error_t instances returned by interpreter.
 */
void FORMULA_API destroy_error(formula_error_t* error);

/*
 * Destroy value_t instance. Use it to destroy all value_t instances returned by interpreter.
 */
void FORMULA_API destroy_value(value_t* value);

/*
 * Use this function to allocate memory chunks which will be passed to interpreter.
 * It's needed in order to guarantee that malloc and free functions used are from the same runtime library.
 */
void* FORMULA_API allocate(int32_t size);

/*
 * Returns interpreter version (parser + evaluator).
 * It's NOT library version. It doesn't related to analyzer or API.
 */
int32_t FORMULA_API get_interpreter_version();

} // extern "C"

#endif //FORMULA_API_H
