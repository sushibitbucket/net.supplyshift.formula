#include <cstring>
#include <algorithm>

#include "api.h"
#include <parser.h>
#include <evaluator.h>
#include <semantic_analyzer.h>
#include "requirements_collector.h"
#include "marshaling.h"
#include "version.h"

static_assert(sizeof(double) == 8, "double should have 64 bits size");

class CallbackException : public std::runtime_error
{
public:
    explicit CallbackException(const char* what, int code, int position) :
            std::runtime_error(what),
            code(code),
            position(position)
    {}

    const int code;
    const int position;
};

void destroy_array(value_array_t* array)
{
    for (size_t i = 0; i < array->size; ++i)
        destroy_value(array->values + i);
    free(array->values);
    memset(array, 0, sizeof(value_array_t));
}

class Interpreter : public DataProvider, public TypeProvider
{
public:
    explicit Interpreter(callbacks_t callbacks) :
        callbacks(callbacks)
    {}

    Value getIndicatorValue(int indicator_id, int position) const override
    {
        value_t value;
        memset(&value, 0, sizeof(value_t));
        if (callbacks.get_ind_value == nullptr)
            throw CallbackException("get_ind_value callback is null", NO_CALLBACK_ERROR, position);

        int error = callbacks.get_ind_value(indicator_id, &value);
        if (error != 0)
            throw CallbackException("get_ind_value failed", error, position);

        Value result = convertValue(value);
        destroy_value(&value);
        return result;
    }

    Value getExtIndicatorValue(int indicator_id, Date from, Date to, int position) const override
    {
        value_t value;
        memset(&value, 0, sizeof(value_t));
        if (callbacks.get_extind_value == nullptr)
            throw CallbackException("get_extind_value callback is null", NO_CALLBACK_ERROR, position);

        value_date_t from_date{from.year, from.month, from.day};
        value_date_t to_date{to.year, to.month, to.day};
        int error = callbacks.get_extind_value(indicator_id, &from_date, &to_date, &value);
        if (error != 0)
            throw CallbackException("get_extind_value failed", error, position);

        Value result = convertValue(value);
        destroy_value(&value);
        return result;
    }

    Value getExtScorecardValue(int scorecard_id, int position) const override
    {
        value_t value;
        memset(&value, 0, sizeof(value_t));
        if (callbacks.get_extscr_value == nullptr)
            throw CallbackException("get_extscr_value callback is null", NO_CALLBACK_ERROR, position);

        int error = callbacks.get_extscr_value(scorecard_id, &value);
        if (error != 0)
            throw CallbackException("get_extscr_value failed", error, position);

        Value result = convertValue(value);
        destroy_value(&value);
        return result;
    }

    Trait getIndicatorInfo(int indicator_id, int position) const override
    {
        if (callbacks.get_indicator_info == nullptr)
            throw CallbackException("get_indicator_info callback is null", NO_CALLBACK_ERROR, position);

        indicator_info_t info;
        memset(&info, 0, sizeof(info));
        int error = callbacks.get_indicator_info(indicator_id, &info);

        if (error != 0)
            throw CallbackException("get_indicator_info failed", error, position);

        auto type = static_cast<Value::Type>(info.type);
        if (!Value::isValidType(type))
            throw CallbackException("get_indicator_info has returned an invalid type id", error, position);

        auto element_type = static_cast<Value::Type>(info.element_type);
        if (!Value::isValidType(element_type))
            throw CallbackException("get_indicator_info has returned an invalid element_type id", error, position);

        std::vector<Value> allowed_values(info.allowed_values.size);
        for (size_t i = 0; i < info.allowed_values.size; ++i)
            allowed_values[i] = convertValue(info.allowed_values.values[i]);
        destroy_array(&info.allowed_values);
        return {type, allowed_values, {}, element_type};
    }

    Parser parser;
    RequirementsCollector requirements_collector;
    callbacks_t callbacks;
};

void* create_interpreter(callbacks_t* callbacks)
{
    try
    {
        return new Interpreter(*callbacks);
    }
    catch(const std::bad_alloc&)
    {
        return nullptr;
    }
}

void parse_formula(void* interpreter_ptr, const char* formula, parse_result_t* result)
{
    auto interpreter = static_cast<Interpreter*>(interpreter_ptr);
    memset(result, 0, sizeof(parse_result_t));
    try
    {
        auto expression = interpreter->parser.parse(formula);
        result->ast = expression.release();
    }
    catch(const ParseException& e)
    {
        result->error.code = PARSE_ERROR;
        result->error.message = strdup(e.what());
        result->error.annotation_offset = e.position;
        result->error.annotation_length = e.length;
    }
    catch(const std::bad_alloc& e)
    {
        result->error.code = OUT_OF_MEMORY;
        result->error.message = strdup(e.what());
    }
    catch (const std::exception& e)
    {
        result->error.code = INTERNAL_ERROR;
        result->error.message = strdup(e.what());
    }
}

indicator_id_t getIndicatorId(const Expression* expression)
{
    auto ind_expr = dynamic_cast<const IndicatorExpression*>(expression);
    if (ind_expr == nullptr)
        return -1;

    return ind_expr->indicator_id;
}

void evaluate_formula(void* interpreter_ptr, void* ast, evaluate_result_t* result_ptr)
{
    memset(result_ptr, 0, sizeof(evaluate_result_t));
    auto interpreter = static_cast<Interpreter*>(interpreter_ptr);
    auto expression = static_cast<Expression*>(ast);
    DataProvider* data_provider = interpreter;
    try
    {
        Value result = Evaluator::evaluate(*expression, data_provider);
        result_ptr->value = convertValue(result);
    }
    catch(const InvalidOperatorException& e)
    {
        result_ptr->error.code = WRONG_TYPE;
        result_ptr->error.message = strdup(e.what());
        result_ptr->error.wrong_type.indicator_id = getIndicatorId(e.left.getSource());
        if (result_ptr->error.wrong_type.indicator_id == -1)
            result_ptr->error.wrong_type.indicator_id = getIndicatorId(e.right.getSource());
    }
    catch(const InvalidOperatorValueException& e)
    {
        result_ptr->error.code = WRONG_TYPE;
        result_ptr->error.message = strdup(e.what());
        result_ptr->error.wrong_type.indicator_id = getIndicatorId(e.value.getSource());
    }
    catch(const DivideByZeroException& e)
    {
        result_ptr->error.code = DIVIDE_BY_ZERO;
        result_ptr->error.message = strdup(e.what());
        result_ptr->error.divide_by_zero.indicator_id = getIndicatorId(e.expression);
    }
    catch(const CallbackException& e)
    {
        result_ptr->error.code = e.code;
        result_ptr->error.message = strdup(e.what());
    }
    catch(const MarshalException& e)
    {
        result_ptr->error.code = MARSHAL_ERROR;
        result_ptr->error.message = strdup(e.what());
    }
    catch(const std::bad_alloc& e)
    {
        result_ptr->error.code = OUT_OF_MEMORY;
        result_ptr->error.message = strdup(e.what());
    }
    catch (const std::exception& e)
    {
        result_ptr->error.code = INTERNAL_ERROR;
        result_ptr->error.message = strdup(e.what());
    }
}

void analyze_formula(void* interpreter_ptr, void* ast, indicator_id_t slave_indicator_id, analyze_result_t* result_ptr)
{
    memset(result_ptr, 0, sizeof(analyze_result_t));
    auto interpreter = static_cast<Interpreter*>(interpreter_ptr);
    auto expression = static_cast<Expression*>(ast);
    TypeProvider* type_provider = interpreter;

    try
    {
        SemanticAnalyzer analyzer(type_provider);
        analyzer.analyze(*expression, slave_indicator_id);
    }
    catch (const AllowedValuesMismatchException& e)
    {
        result_ptr->report.code = ANALYSIS_ALLOWED_VALUES_MISMATCH;
        result_ptr->report.message = strdup(e.what());
        result_ptr->report.annotation_offset = e.position;
    }
    catch (const TypeMismatchException& e)
    {
        result_ptr->report.code = ANALYSIS_TYPE_MISMATCH;
        result_ptr->report.message = strdup(e.what());
        result_ptr->report.annotation_offset = e.position;
    }
    catch (const WrongArgumentException& e)
    {
        result_ptr->report.code = ANALYSIS_WRONG_ARGUMENTS;
        result_ptr->report.message = strdup(e.what());
        result_ptr->report.annotation_offset = e.position;
    }
    catch (const CallbackException& e)
    {
        result_ptr->error.code = e.code;
        result_ptr->error.message = strdup(e.what());
        result_ptr->error.annotation_offset = e.position;
    }
    catch(const std::bad_alloc& e)
    {
        result_ptr->error.code = OUT_OF_MEMORY;
        result_ptr->error.message = strdup(e.what());
    }
    catch (const std::exception& e)
    {
        result_ptr->error.code = INTERNAL_ERROR;
        result_ptr->error.message = strdup(e.what());
    }
}

void collect_requirements(void* interpreter_ptr, void* ast, int8_t collect_ind, int8_t collect_extind,
                          int8_t collect_extscr, collect_result_t* result)
{
    memset(result, 0, sizeof(collect_result_t));
    auto interpreter = static_cast<Interpreter*>(interpreter_ptr);
    auto expression = static_cast<Expression*>(ast);

    try
    {
        interpreter->requirements_collector.setCollectInd(collect_ind != 0);
        interpreter->requirements_collector.setCollectExtInd(collect_extind != 0);
        interpreter->requirements_collector.setCollectExtScr(collect_extscr != 0);
        interpreter->requirements_collector.collect(*expression);

        const std::vector<indicator_id_t>& ind = interpreter->requirements_collector.getInd();
        if (!ind.empty())
        {
            result->requirements.ind_count = ind.size();
            result->requirements.ind = ind.data();
        }

        const std::vector<extind_arguments_t>& extind = interpreter->requirements_collector.getExtInd();
        if (!extind.empty())
        {
            result->requirements.extind_count = extind.size();
            result->requirements.extind = extind.data();
        }

        const std::vector<scorecard_id_t>& extscr = interpreter->requirements_collector.getExtScr();
        if (!extscr.empty())
        {
            result->requirements.extscr_count = extscr.size();
            result->requirements.extscr = extscr.data();
        }
    }
    catch(const std::bad_alloc& e)
    {
        result->error.code = OUT_OF_MEMORY;
        result->error.message = strdup(e.what());
    }
    catch (const std::exception& e)
    {
        result->error.code = INTERNAL_ERROR;
        result->error.message = strdup(e.what());
    }
}

void destroy_interpreter(void* interpreter_ptr)
{
    auto interpreter = static_cast<Interpreter*>(interpreter_ptr);
    delete interpreter;
}

void destroy_ast(void* ast)
{
    auto expression = static_cast<Expression*>(ast);
    delete expression;
}

void destroy_error(formula_error_t* error)
{
    free(error->message);
    memset(error, 0, sizeof(formula_error_t));
}

void destroy_value(value_t* value)
{
    switch (value->type)
    {
        case Value::Type::STRING:
            free(value->str_value);
            break;
        case Value::Type::ARRAY:
            destroy_array(&value->array);
            break;
        case Value::Type::MULTI_SELECT:
            destroy_array(&value->multi_select);
            break;
        default:
            break;
    }
    memset(value, 0, sizeof(value_t));
}

void* allocate(int32_t size)
{
	return malloc(size);
}

int32_t get_interpreter_version()
{
    return INTERPRETER_VERSION;
}
