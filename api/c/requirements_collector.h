#ifndef FORMULA_REQUIREMENTS_COLLECTOR_H
#define FORMULA_REQUIREMENTS_COLLECTOR_H

#include "expression_visitor.h"
#include "exceptions.h"
#include "evaluator.h"
#include "marshaling.h"
#include "api.h"

class RequirementsCollector : public RecursiveExpressionVisitor
{
public:
    void setCollectInd(bool collect_ind)
    {
        this->collect_ind = collect_ind;
    }

    void setCollectExtInd(bool collect_extind)
    {
        this->collect_extind = collect_extind;
    }

    void setCollectExtScr(bool collect_extscr)
    {
        this->collect_extscr = collect_extscr;
    }

    void collect(const Expression& expression)
    {
        ind_list.clear();
        extind_list.clear();
        extscr_list.clear();
        expression.visit(*this);
    }

    const std::vector<indicator_id_t>& getInd() const
    {
        return ind_list;
    }

    const std::vector<extind_arguments_t>& getExtInd() const
    {
        return extind_list;
    }

    const std::vector<scorecard_id_t>& getExtScr() const
    {
        return extscr_list;
    }

private:
    void visit(const IndicatorExpression& ind) override
    {
        if (!collect_ind)
            return;

        ind_list.push_back(ind.indicator_id);
    }

    void visit(const ExtIndicatorExpression& extind) override
    {
        if (!collect_extind)
            return;

        extind_arguments_t extind_arguments;

        extind_arguments.indicator_id = extind.indicator_id;

        extind_arguments.from.year = extind.from.year;
        extind_arguments.from.month = extind.from.month;
        extind_arguments.from.day = extind.from.day;

        extind_arguments.to.year = extind.to.year;
        extind_arguments.to.month = extind.to.month;
        extind_arguments.to.day = extind.to.day;

        extind_list.push_back(extind_arguments);
    }

    void visit(const ExtScrExpression& extscr) override
    {
        if (!collect_extscr)
            return;

        extscr_list.push_back(extscr.scorecard_id);
    }

private:
    bool collect_ind = true;
    std::vector<indicator_id_t> ind_list;

    bool collect_extind = true;
    std::vector<extind_arguments_t> extind_list;

    bool collect_extscr = true;
    std::vector<scorecard_id_t> extscr_list;
};

#endif //FORMULA_REQUIREMENTS_COLLECTOR_H
