//
// Created by aegorov on 17.08.17.
//

#ifndef FORMULA_MARSHALING_H
#define FORMULA_MARSHALING_H

#include <cstring>
#include <sstream>

#include "api.h"
#include <value.h>

class MarshalException : public std::runtime_error
{
public:
    explicit MarshalException(const char* what) :
            std::runtime_error(what)
    {}
};

value_t convertValue(const Value& src)
{
    value_t dst;
    dst.type = static_cast<int>(src.type());
    dst.properties = src.getProperties();
    switch (src.type())
    {
        case Value::Type::NONE:
            break;
        case Value::Type::INTEGER:
            dst.int_value = src.getInt();
            break;
        case Value::Type::FLOAT:
            dst.float_value = src.getFloat();
            break;
        case Value::Type::BOOLEAN:
            dst.bool_value = static_cast<int>(src.getBool());
            break;
        case Value::Type::STRING:
            dst.str_value = strdup(src.getString());
            if (dst.str_value == nullptr)
                throw std::bad_alloc();
            break;
        case Value::Type::ARRAY:
        {
            Value* array = src.getElements();
            dst.array.size = src.getElementCount();
            dst.array.values = (value_t*)malloc(sizeof(value_t) * dst.array.size);
            if (dst.array.values == nullptr)
                throw std::bad_alloc();
            for (size_t i = 0; i < dst.array.size; ++i)
                dst.array.values[i] = convertValue(array[i]);
            break;
        }
        case Value::Type::MULTI_SELECT:
        {
            Value* array = src.getElements();
            dst.multi_select.size = src.getElementCount();
            dst.multi_select.values = (value_t*)malloc(sizeof(value_t) * dst.multi_select.size);
            if (dst.multi_select.values == nullptr)
                throw std::bad_alloc();
            for (size_t i = 0; i < dst.multi_select.size; ++i)
                dst.multi_select.values[i] = convertValue(array[i]);
            break;
        }
        case Value::Type::BLANK:
        case Value::Type::NOT_APPLICABLE:
        case Value::Type::NONE_OF_ABOVE:
            return dst;
        case Value::Type::DATE:
            dst.date.year = src.getDate().year;
            dst.date.month = src.getDate().month;
            dst.date.day = src.getDate().day;
            return dst;
        case Value::Type::LOCATION:
            dst.location.country = src.getCountry();
            dst.location.province = src.getProvince();
            return dst;
        case Value::Type::GPS:
            dst.gps.latitude = src.getLatitude();
            dst.gps.longitude = src.getLongitude();
            return dst;
        default:
        {
            std::stringstream ss;
            ss << "Unknown type id " << dst.type;
            throw MarshalException(ss.str().c_str());
        }
    }
    return dst;
}

Value convertValue(const value_t& src)
{
    auto src_type = static_cast<Value::Type>(src.type);
    switch (src_type)
    {
        case Value::Type::NONE:
            return Value();
        case Value::Type::INTEGER:
            return Value::createInt(src.int_value);
        case Value::Type::FLOAT:
            return Value(src.float_value);
        case Value::Type::BOOLEAN:
            return Value(src.bool_value == 1);
        case Value::Type::STRING:
            return Value(src.str_value);
        case Value::Type::ARRAY:
        {
            Value* values = new Value[src.array.size];
            for (size_t i = 0; i < src.array.size; ++i)
                values[i] = convertValue(src.array.values[i]);
            return Value(values, src.array.size, Value::ARRAY);
        }
        case Value::Type::MULTI_SELECT:
        {
            Value* values = new Value[src.multi_select.size];
            for (size_t i = 0; i < src.multi_select.size; ++i)
                values[i] = convertValue(src.multi_select.values[i]);
            return Value(values, src.multi_select.size, Value::MULTI_SELECT);
        }
        case Value::Type::BLANK:
            return Value::Blank;
        case Value::Type::NOT_APPLICABLE:
            return Value::NotApplicable;
        case Value::Type::NONE_OF_ABOVE:
            return Value::NoneOfAbove;
        case Value::Type::DATE:
            return Value(Date{src.date.year, src.date.month, src.date.day});
        case Value::Type::LOCATION:
            return Value::createLocation(src.location.country, src.location.province);
        case Value::Type::GPS:
            return Value::createGps(src.gps.latitude, src.gps.longitude);
        default:
        {
            std::stringstream ss;
            ss << "Unknown type id " << src.type;
            throw MarshalException(ss.str().c_str());
        }
    }
}

#endif //FORMULA_MARSHALING_H
