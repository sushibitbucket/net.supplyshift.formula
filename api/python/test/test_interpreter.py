from datetime import date

import pytest
from mock import Mock

from supplyshift_formula import (FormulaException, Interpreter, NotApplicable, NoneOfAbove, Blank, ValueProperty,
                                 MultiSelect, Gps, Location, WrongTypeException, DivideByZeroException, IDataProvider)


@pytest.mark.parametrize("formula, expected", [
    # basic types syntax
    ('50', 50),
    ('+50', 50),
    ('-50', -50),
    ('50.54', 50.54),
    ('+50.54', 50.54),
    ('-50.54', -50.54),
    ('true', True),
    ('false', False),
    ('{}', []),
    ('{11, 22, 33}', [11, 22, 33]),
    ('{1, "2", 3}', [1, '2', 3]),

    # operator +
    ('3 + 5', 8),
    ('3.3 + 5', 8.3),
    ('-50 + 30', -20),
    ('+50 + 30', 80),
    ('+50 + 30.3', 80.3),
    ('+50.5 + 30.3', 80.8),

    # operator -
    ('3 - 5', -2),
    ('3.3 - 5', -1.7),
    ('-50 - 30', -80),
    ('+50 - 30', 20),
    ('+50 - 30.3', 19.7),
    ('+50.5 - 30.3', 20.2),

    # operator *
    ('3 * 5', 3 * 5),
    ('3.3 * 5', 3.3 * 5),
    ('-50 * 30', -50 * 30),
    ('+50 * 30', 50 * 30),
    ('+50 * 30.3', 50 * 30.3),
    ('+50.5 * 30.3', 50.5 * 30.3),

    # operator /
    ('3 / 5', 3.0 / 5.0),
    ('3.3 / 5', 3.3 / 5),
    ('-50 / 30', -50.0 / 30),
    ('+50 / 30', 50.0 / 30.0),
    ('+50 / 30.3', 50 / 30.3),
    ('+50.5 / 30.3', 50.5 / 30.3),

    # operator =, !=
    ('3 = 3', True),
    ('3 = 3.0', True),
    ('3.0 = 3.0', True),
    ('3.0 = 3', True),
    ('3 != 3', False),
    ('3 != 3.0', False),
    ('3.0 != 3.0', False),
    ('3.0 != 3', False),
    ('{11, 22, 33} = {11, 22, 33}', True),
    ('{11, 22, 33} != {11, 33}', True),
    ('NA = NA', True),
    ('NOA = NOA', True),
    ('BLANK = BLANK', True),
    ('"Test" = 123', False),

    # functions
    ('IF(123 = 123, 333, 555)', 333),
    ('IF(123 = 12, 333, 555)', 555),
    ('AND(true, 1=2)', False),
    ('AND(true, 2=2)', True),
    ('OR(true, 1=2)', True),
    ('OR(false, 1=2)', False),
    ('OR(true, 2=2)', True),
    ('OR(false, 2=2)', True),
    ('SUM({1, 2, 3})', 6),
    ('SUM(1, 2, 3)', 6),
    ('AVG({1, 2, 3})', 2.0),
    ('AVG(1, 2, 3)', (1 + 2 + 3) / 3.0),
    ('COUNT({1, 2, 3})', 3),
    ('COUNT({1, "test", 3})', 3),
    ('COUNT(1, 2, 3)', 3),

    # other
    ('true || false', True),
    ('{11, (22 + 2)*2, "123" = "45"}', [11, 48, False]),
    ('123 + 12 + 100 * (2 - 17) + 50', -1315),
])
def test_evaluate_approx(formula, expected):
    interpreter = Interpreter()
    ast = interpreter.parse(formula)

    # Act
    result, properties = interpreter.evaluate(ast)

    # Assert
    assert result == pytest.approx(expected)
    assert type(result) is type(expected)


@pytest.mark.parametrize("formula, expected", [
    # basic syntax types
    ('"hello world"', 'hello world'),

    # special values
    ('{12, {NA, 56, {7, BLANK}}}', [12, [NotApplicable, 56, [7, Blank]]]),
    ('NA', NotApplicable),
    ('NOA', NoneOfAbove),
    ('BLANK', Blank),
    ('NA + 100', NotApplicable),
    ('NA + 100.5', NotApplicable),
    ('NA + false', NotApplicable),
    ('NA + "test"', NotApplicable),
    ('NA + {1, 2}', NotApplicable),
    ('NA + NOA', NotApplicable),
    ('NA + BLANK', NotApplicable),
    ('100 + NA', NotApplicable),
    ('100.5 + NA', NotApplicable),
    ('false + NA', NotApplicable),
    ('"test" + NA', NotApplicable),
    ('{1, 2} + NA', NotApplicable),
    ('NOA + NA', NotApplicable),
    ('BLANK + NA', NotApplicable),
    ('NA - 100', NotApplicable),
    ('NA - 100.5', NotApplicable),
    ('NA - false', NotApplicable),
    ('NA - "test"', NotApplicable),
    ('NA - {1, 2}', NotApplicable),
    ('NA - NOA', NotApplicable),
    ('NA - BLANK', NotApplicable),
    ('100 - NA', NotApplicable),
    ('100.5 - NA', NotApplicable),
    ('false - NA', NotApplicable),
    ('"test" - NA', NotApplicable),
    ('{1, 2} - NA', NotApplicable),
    ('NOA - NA', NotApplicable),
    ('BLANK - NA', NotApplicable),
    ('NA * 100', NotApplicable),
    ('NA * 100.5', NotApplicable),
    ('NA * false', NotApplicable),
    ('NA * "test"', NotApplicable),
    ('NA * {1, 2}', NotApplicable),
    ('NA * NOA', NotApplicable),
    ('NA * BLANK', NotApplicable),
    ('100 * NA', NotApplicable),
    ('100.5 * NA', NotApplicable),
    ('false * NA', NotApplicable),
    ('"test" * NA', NotApplicable),
    ('{1, 2} * NA', NotApplicable),
    ('NOA * NA', NotApplicable),
    ('BLANK * NA', NotApplicable),
    ('NA / 100', NotApplicable),
    ('NA / 100.5', NotApplicable),
    ('NA / false', NotApplicable),
    ('NA / "test"', NotApplicable),
    ('NA / {1, 2}', NotApplicable),
    ('NA / NOA', NotApplicable),
    ('NA / BLANK', NotApplicable),
    ('100 / NA', NotApplicable),
    ('100.5 / NA', NotApplicable),
    ('false / NA', NotApplicable),
    ('"test" / NA', NotApplicable),
    ('{1, 2} / NA', NotApplicable),
    ('NOA / NA', NotApplicable),
    ('BLANK / NA', NotApplicable),
    ('DATE("8/12/2017")', date(2017, 8, 12)),
    ('DATE("2/29/2020")', date(2020, 2, 29)),
])
def test_evaluate_strict(formula, expected):
    interpreter = Interpreter()
    ast = interpreter.parse(formula)

    # Act
    result, properties = interpreter.evaluate(ast)

    # Assert
    assert result == expected
    assert type(result) is type(expected)


@pytest.mark.parametrize("formula, expected, expected_properties", [
    ('NA', NotApplicable, 0),
    ('HIDDEN', Blank, ValueProperty.HIDDEN),
    ('USERINPUT', Blank, ValueProperty.USERINPUT),
    ('HIDDEN & USERINPUT', Blank, ValueProperty.HIDDEN | ValueProperty.USERINPUT),
    ('NA & HIDDEN', NotApplicable, ValueProperty.HIDDEN),
    ('NA & USERINPUT', NotApplicable, ValueProperty.USERINPUT),
    ('NA & HIDDEN & USERINPUT', NotApplicable, ValueProperty.HIDDEN | ValueProperty.USERINPUT),
    ('IF(NA=NA, NA&HIDDEN, USERINPUT)', NotApplicable, ValueProperty.HIDDEN),
])
def test_evaluate_properties(formula, expected, expected_properties):
    interpreter = Interpreter()
    ast = interpreter.parse(formula)

    # Act
    result, properties = interpreter.evaluate(ast)

    # Assert
    assert result == expected
    assert type(result) is type(expected)
    assert properties == expected_properties


@pytest.mark.parametrize("formula", [
    '3+',
    'tue',
    '+50+30..5',
    'true or or false',
    '"hello  + "world"',
    '{1, "2, 3}',
    'IF(123 = 123, 333)',
    'MIN()',
    '123 / 0',
    'SUM({1, "test", 3})',
    'AVG({1, "test", 3})',
    'DATE("2/29/2017")',
    'IND(IND(1))',
])
def test_evaluate_invalid(formula):
    with pytest.raises(FormulaException):
        interpreter = Interpreter()
        ast = interpreter.parse(formula)
        interpreter.evaluate(ast)


@pytest.mark.parametrize('formula, expected, answers', [
    ('IND(123)', 100, {123: 100}),
    ('IND(1)', 'Yes', {1: 'Yes'}),
    ('IF(IND(1)="Yes", IND(2)+IND(3), IND(2))', 50, {
        1: 'Yes',
        2: 20,
        3: 30,
    }),
    ('IND(1)', NotApplicable, {1: NotApplicable}),
    ('IND(1)', NoneOfAbove, {1: NoneOfAbove}),
    ('IND(1)', Blank, {1: Blank}),
    ('IND(1)', [1, 2, 3], {1: [1, 2, 3]}),
    ('IND(1)', Gps(latitude=56.435167, longitude=85.129623), {1: Gps(latitude=56.435167, longitude=85.129623)}),
    ('IND(1)', Location(country=10, province=20), {1: Location(country=10, province=20)}),
])
def test_evaluate_ind(formula, expected, answers):
    data_provider = Mock(IDataProvider)
    data_provider.get_ind_value = answers.get

    interpreter = Interpreter()
    interpreter.data_provider = data_provider
    ast = interpreter.parse(formula)

    # Act
    result, properties = interpreter.evaluate(ast)

    # Assert
    assert result == expected
    assert type(result) is type(expected)


@pytest.mark.parametrize('formula, expected, answers', [
    ('EXTIND(1)', [1, 2, 3, 4], {1: [
        (1, date(2017, 1, 1)),
        (2, date(2017, 7, 1)),
        (3, date(2017, 8, 1)),
        (4, date(2017, 9, 1)),
    ]}),
    ('EXTIND(1, DATE("6/1/2017"))', [2, 3, 4], {1: [
        (1, date(2017, 1, 1)),
        (2, date(2017, 7, 1)),
        (3, date(2017, 8, 1)),
        (4, date(2017, 9, 1)),
    ]}),
    ('EXTIND(1, DATE("6/1/2017"), DATE("8/1/2017"))', [2, 3], {1: [
        (1, date(2017, 1, 1)),
        (2, date(2017, 7, 1)),
        (3, date(2017, 8, 1)),
        (4, date(2017, 9, 1)),
    ]}),
    ('COUNT(EXTIND(1))', 2, {1: [
        (MultiSelect(['a', 'b']), date(2017, 7, 1)),
        (MultiSelect(['c', 'd']), date(2017, 7, 2)),
        (NotApplicable, date(2017, 8, 1)),
    ]}),
])
def test_evaluate_extind(formula, expected, answers):
    def get_extind_value(indicator_id, from_date, to_date):
        values = answers[indicator_id]
        values = [v for v, d in values
                  if (from_date is None or d >= from_date) and
                  (to_date is None or d <= to_date)]
        return values

    data_provider = Mock(IDataProvider)
    data_provider.get_extind_value = get_extind_value

    interpreter = Interpreter()
    interpreter.data_provider = data_provider
    ast = interpreter.parse(formula)

    # Act
    result, properties = interpreter.evaluate(ast)

    # Assert
    assert result == expected
    assert type(result) is type(expected)


@pytest.mark.parametrize('formula, expected, answers', [
    ('EXTSCR(1)', [1, 2, 3], {1: [1, 2, 3]}),
])
def test_evaluate_extscr(formula, expected, answers):
    data_provider = Mock(IDataProvider)
    data_provider.get_extscr_value = answers.get

    interpreter = Interpreter()
    interpreter.data_provider = data_provider
    ast = interpreter.parse(formula)

    # Act
    result, properties = interpreter.evaluate(ast)

    # Assert
    assert result == expected
    assert type(result) is type(expected)


@pytest.mark.parametrize('formula, expected_exception, expected_indicator_id, answers', [
    ('IND(1) * 3 / IND(2)', DivideByZeroException, 2, {1: 10, 2: 0}),
    ('IND(1) * 3 % IND(2)', DivideByZeroException, 2, {1: 10, 2: 0}),
])
def test_divide_by_zero(formula, expected_exception, expected_indicator_id, answers):
    data_provider = Mock(IDataProvider)
    data_provider.get_ind_value = answers.get

    interpreter = Interpreter()
    interpreter.data_provider = data_provider
    ast = interpreter.parse(formula)

    # Act
    with pytest.raises(expected_exception) as e:
        interpreter.evaluate(ast)

    # Assert
    assert e.value.indicator_id == expected_indicator_id


@pytest.mark.parametrize('formula, answers, expected_indicator_id', [
    ('IND(1) * 3 + IND(2)', {1: 10, 2: Blank}, 2),
    ('IND(1) + IND(2)', {1: 10, 2: Blank}, 1),
    ('-IND(1)', {1: Blank}, 1),
])
def test_evaluate_wrong_type(formula, answers, expected_indicator_id):
    data_provider = Mock(IDataProvider)
    data_provider.get_ind_value = answers.get

    interpreter = Interpreter()
    interpreter.data_provider = data_provider
    ast = interpreter.parse(formula)

    # Act
    with pytest.raises(WrongTypeException) as e:
        interpreter.evaluate(ast)

    # Assert
    assert e.value.indicator_id == expected_indicator_id
