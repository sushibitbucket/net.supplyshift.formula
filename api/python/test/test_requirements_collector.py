from datetime import date

import pytest
from supplyshift_formula import Interpreter
from supplyshift_formula.marshaling import Requirements


@pytest.mark.parametrize('expression, expected', [
    ('IND(1)', Requirements(ind=[1])),
    ('EXTIND(1)', Requirements(extind=[(1, None, None)])),
    ('EXTIND(1, DATE("10/8/2015"))', Requirements(extind=[(1, date(2015, 10, 8), None)])),
    ('EXTIND(1, DATE("10/8/2015"), DATE("7/5/2014"))', Requirements(extind=[(1, date(2015, 10, 8), date(2014, 7, 5))])),
    ('EXTSCR(1)', Requirements(extscr=[1])),
])
def test_collect_requirements(expression, expected):
    interpreter = Interpreter()
    ast = interpreter.parse(expression)

    # Act
    requirements = interpreter.collect_requirements(ast)

    # Assert
    assert requirements == expected
