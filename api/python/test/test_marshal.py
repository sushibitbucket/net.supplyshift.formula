from datetime import date

import pytest
from supplyshift_formula import Blank, NotApplicable, NoneOfAbove, Gps, Location, MultiSelect
from supplyshift_formula.api import value_t
from supplyshift_formula.marshaling import marshal_value, unmarshal_value


@pytest.mark.parametrize('input_value', [
    123,
    -123,
    123.0,
    -123.0,
    'test string',
    '',
    [],
    [1, 2, 3],
    [1, 'hello', 3],
    True,
    False,
    Blank,
    NotApplicable,
    NoneOfAbove,
    date(2017, 10, 10),
    MultiSelect(['hello', 'world']),
    MultiSelect([1, 2, 3]),
    Location(country=10, province=20),
    Gps(latitude=56.435167, longitude=85.129623),
])
def test_marshal(input_value):
    val = value_t()

    # Act
    marshal_value(input_value, val)
    result = unmarshal_value(val)

    # Assert
    assert result == input_value
    assert type(result) is type(input_value)
