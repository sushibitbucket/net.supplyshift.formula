import pytest
from supplyshift_formula import Interpreter


@pytest.mark.parametrize("formula, expected, answers", [
    ('2', 2, {}),
    ('2+2+2', 6, {}),
    ('123 + 12 + 100 * (2 - 17) + 50', -1315, {}),
    ('IF(50=50, 123, 456)', 123, {}),
    ('{11, (22 + 2)*2, "123" = "45"}', [11, 48, False], {}),
    ('{12, {34, 56, {7, 8}}}', [12, [34, 56, [7, 8]]], {}),
    ('COUNT({1, "test", 3})', 3, {}),
    # external data
    ('IND(123)', 100, {123: 100}),
    ('IND(1)', 'Yes', {1: 'Yes'}),
    ('IF(IND(1)="Yes", IND(2)+IND(3), IND(2))', 50, {
        1: 'Yes',
        2: 20,
        3: 30,
    }),
    ('IND(2)', [1, 2, 3], {2: [1, 2, 3]}),
    ('IND(3)', [1, 2, 3] * 100, {3: [1, 2, 3] * 100}),
])
def test_evaluate_benchmark(benchmark, formula, expected, answers):
    interpreter = Interpreter()
    interpreter.get_ind_value = answers.get

    def parse_and_evaluate():
        ast = interpreter.parse(formula)
        return interpreter.evaluate(ast)

    # Act
    result, properties = benchmark(parse_and_evaluate)

    # Assert
    assert result == expected
    assert type(result) is type(expected)
