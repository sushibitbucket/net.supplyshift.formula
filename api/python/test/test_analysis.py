import pytest
from mock import Mock

from supplyshift_formula import (Interpreter, AnalysisResult, InvalidIndicatorIdException, Blank, NotApplicable,
                                 IndicatorInfo, Location, Gps, IIndicatorInfoProvider)


@pytest.mark.parametrize('formula, indicators_info', [
    ('IND(1)', {
        0: IndicatorInfo(int, allowed_values=[]),
        1: IndicatorInfo(int, allowed_values=[]),
    }),
    ('IND(1)', {
        0: IndicatorInfo(str, allowed_values=[]),
        1: IndicatorInfo(str, allowed_values=[]),
    }),
    ('(IF(AND(IND(11)!=NA, IND(11)!=BLANK), IND(11), 3.14) + IFERROR(IND(22)/IND(33), 1)) * IF(OR(IND(44)="Cert1", IND(44)="Cert2"), 1, 200)', {
        0: IndicatorInfo(float, allowed_values=[Blank, NotApplicable]),
        11: IndicatorInfo(float, allowed_values=[Blank, NotApplicable]),
        22: IndicatorInfo(float, allowed_values=[Blank, NotApplicable]),
        33: IndicatorInfo(float, allowed_values=[Blank, NotApplicable]),
        44: IndicatorInfo(str, allowed_values=[Blank, NotApplicable]),
    }),
    ('CONTAINS(IND(1), {"a", "b", "c"})', {
        0: IndicatorInfo(bool, allowed_values=[]),
        1: IndicatorInfo(list, element_type=str),
    }),
])
def test_analyze(formula, indicators_info):
    info_provider = Mock(IIndicatorInfoProvider)
    info_provider.get_indicator_info = indicators_info.get

    interpreter = Interpreter()
    interpreter.indicator_info_provider = info_provider
    ast = interpreter.parse(formula)

    # Act
    result = interpreter.analyze(ast, slave_indicator_id=0)

    # Assert
    assert result.code == AnalysisResult.OK


@pytest.mark.parametrize('formula, expected_code, expected_position, indicators_info', [
    ('3 + "123"', AnalysisResult.TYPE_MISMATCH, 2, {
        0: IndicatorInfo(str, allowed_values=[]),
    }),
    ('IND(1)=IND(2)', AnalysisResult.ALLOWED_VALUES_MISMATCH, 6, {
        0: IndicatorInfo(str, allowed_values=[]),
        1: IndicatorInfo(str, ['world', 'bar']),
        2: IndicatorInfo(str, [b'test', b'hello']),
    }),
    ('3 + 3', AnalysisResult.TYPE_MISMATCH, 0, {
        0: IndicatorInfo(str, allowed_values=[]),
    }),
    ('"test"', AnalysisResult.TYPE_MISMATCH, 0, {
        0: IndicatorInfo(int, allowed_values=[]),
    }),
    ('CONTAINS(IND(1), {"a", "b", "c"})', AnalysisResult.TYPE_MISMATCH, 0, {
        0: IndicatorInfo(bool, allowed_values=[]),
        1: IndicatorInfo(list, element_type=int),
    }),
    ('IND(1)', AnalysisResult.TYPE_MISMATCH, 0, {
        0: IndicatorInfo(Location),
        1: IndicatorInfo(Gps),
    }),
])
def test_analyze_invalid(formula, expected_code, expected_position, indicators_info):
    info_provider = Mock(IIndicatorInfoProvider)
    info_provider.get_indicator_info = indicators_info.get

    interpreter = Interpreter()
    interpreter.indicator_info_provider = info_provider
    ast = interpreter.parse(formula)

    # Act
    result = interpreter.analyze(ast, slave_indicator_id=0)
    print(result.message)

    # Assert
    assert result.code == expected_code
    assert result.position == expected_position


@pytest.mark.parametrize('formula, expected_annotation_offset', [
    ('IND(1)', 0),
])
def test_analyze_invalid_indicator_id(formula, expected_annotation_offset):
    # Arrange
    def get_invalid_indicator_id(indicator_id):
        raise InvalidIndicatorIdException

    info_provider = Mock(IIndicatorInfoProvider)
    info_provider.get_indicator_info = get_invalid_indicator_id

    interpreter = Interpreter()
    interpreter.indicator_info_provider = info_provider
    ast = interpreter.parse(formula)

    with pytest.raises(InvalidIndicatorIdException) as e:
        # Act
        interpreter.analyze(ast, slave_indicator_id=0)

    # Assert
    assert e.value.annotation_offset == expected_annotation_offset
