from abc import abstractmethod, ABCMeta
from collections import Counter
from ctypes import byref

from .constants import ErrorCode
from .exceptions import *
from .api import (formula_lib, callbacks_t, get_ind_value_t, get_extind_value_t, get_extscr_value_t,
                  parse_result_t, analyze_result_t, evaluate_result_t, get_indicator_info_t, collect_result_t)
from .marshaling import (marshal_value, marshal_type, marshal_array, unmarshal_value, unmarshal_date,
                         unmarshal_requirements)

CALLBACK_EXCEPTION = ErrorCode.USER_ERROR + 1


def check_error(error, default_exception, expression):
    if error.code == ErrorCode.SUCCESS:
        return

    exceptions_map = {
        CALLBACK_EXCEPTION: CallbackException,
        ErrorCode.INVALID_INDICATOR_ID: InvalidIndicatorIdException,
        ErrorCode.WRONG_TYPE: WrongTypeException,
        ErrorCode.DIVIDE_BY_ZERO: DivideByZeroException,
        ErrorCode.MARSHAL_ERROR: MarshalException,
    }

    message = error.message
    if message is not None:
        message = message.decode()
    if error.annotation_length != 0 or error.annotation_offset != 0:
        message += '\n' + expression
        message += '\n' + ' ' * error.annotation_offset
        message += '^' + '~' * (error.annotation_length - 1)

    exception_type = exceptions_map.get(error.code, default_exception)
    exception = exception_type(message)
    exception.annotation_offset = error.annotation_offset
    exception.annotation_length = error.annotation_length

    if error.code == ErrorCode.WRONG_TYPE:
        if error.payload.wrong_type.indicator_id != -1:
            exception.indicator_id = error.payload.wrong_type.indicator_id
    elif error.code == ErrorCode.DIVIDE_BY_ZERO and error.payload.divide_by_zero.indicator_id != -1:
        exception.indicator_id = error.payload.divide_by_zero.indicator_id

    raise exception


class Ast:
    def __init__(self, ast_pointer, expression):
        self.pointer = ast_pointer
        self.expression = expression

    def __del__(self):
        formula_lib.destroy_ast(self.pointer)


class IndicatorInfo:
    def __init__(self, data_type, allowed_values=None, element_type=None):
        self.data_type = data_type
        self.allowed_values = allowed_values or []
        self.element_type = element_type

        if self.data_type is list and element_type is None:
            raise FormulaException('Invalid IndicatorInfo arguments, element_type shouldn`t be None when data_type is a list')

    def __repr__(self):
        element_type_info = '[{}]'.format(self.element_type.__name__) if self.element_type is not None else ''
        return 'IndicatorInfo{{{}{}, {}}}'.format(self.data_type.__name__, element_type_info, self.allowed_values)

    def __eq__(self, other):
        if not isinstance(other, IndicatorInfo):
            return False

        return (self.data_type == other.data_type and
                Counter(self.allowed_values) == Counter(other.allowed_values) and
                self.element_type == other.element_type)


class AnalysisResult:
    OK = 0
    WRONG_ARGUMENTS = 1
    TYPE_MISMATCH = 2
    ALLOWED_VALUES_MISMATCH = 3

    def __init__(self, code, position, message):
        self.code = code
        self.position = position
        self.message = message


class IDataProvider:
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_ind_value(self, indicator_id):
        raise NotImplementedError()

    @abstractmethod
    def get_extind_value(self, indicator_id, from_date, to_date):
        raise NotImplementedError()

    @abstractmethod
    def get_extscr_value(self, scorecard_id):
        raise NotImplementedError()


class IIndicatorInfoProvider:
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_indicator_info(self, indicator_id):
        raise NotImplementedError()


class Interpreter:
    def __init__(self):
        self.data_provider = None  # instance of DataProvider
        self.indicator_info_provider = None  # instance of IndicatorInfoProvider
        self.callbacks = self._create_callbacks()  # store callbacks to avoid garbage collection
        self.instance = formula_lib.create_interpreter(byref(self.callbacks))

    def _create_callbacks(self):
        callbacks = callbacks_t()

        def get_ind_value(indicator_id, value_ptr):
            if not self.data_provider:
                return ErrorCode.NO_CALLBACK_ERROR
            try:
                value = self.data_provider.get_ind_value(indicator_id)
                marshal_value(value, value_ptr.contents)
                return ErrorCode.SUCCESS
            except MarshalException:
                return ErrorCode.MARSHAL_ERROR
            except InvalidIndicatorIdException:
                return ErrorCode.INVALID_INDICATOR_ID
            except Exception as e:
                print('Unexpected exception in get_ind_value: ' + str(e))
                return CALLBACK_EXCEPTION
        callbacks.get_ind_value = get_ind_value_t(get_ind_value)

        def get_extind_value(indicator_id, from_, to_, value_ptr):
            if not self.data_provider:
                return ErrorCode.NO_CALLBACK_ERROR
            try:
                from_date = unmarshal_date(from_.contents) if from_.contents.year != 0 else None
                to_date = unmarshal_date(to_.contents) if to_.contents.year != 0 else None
                value = self.data_provider.get_extind_value(indicator_id, from_date, to_date)
                marshal_value(value, value_ptr.contents)
                return ErrorCode.SUCCESS
            except MarshalException:
                return ErrorCode.MARSHAL_ERROR
            except InvalidIndicatorIdException:
                return ErrorCode.INVALID_INDICATOR_ID
            except Exception as e:
                print('Unexpected exception in get_extind_value: ' + str(e))
                return CALLBACK_EXCEPTION
        callbacks.get_extind_value = get_extind_value_t(get_extind_value)

        def get_extscr_value(scorecard_id, value_ptr):
            if not self.data_provider:
                return ErrorCode.NO_CALLBACK_ERROR
            try:
                value = self.data_provider.get_extscr_value(scorecard_id)
                marshal_value(value, value_ptr.contents)
                return ErrorCode.SUCCESS
            except MarshalException:
                return ErrorCode.MARSHAL_ERROR
            except Exception as e:
                print('Unexpected exception in get_extscr_value: ' + str(e))
                return CALLBACK_EXCEPTION
        callbacks.get_extscr_value = get_extscr_value_t(get_extscr_value)

        def get_indicator_info(indicator_id, info_ptr):
            if not self.indicator_info_provider:
                return ErrorCode.NO_CALLBACK_ERROR
            try:
                info = self.indicator_info_provider.get_indicator_info(indicator_id)
                if not isinstance(info, IndicatorInfo):
                    raise Exception('Interpreter.get_indicator_info should return IndicatorInfo instance')
                info_ptr.contents.type = marshal_type(info.data_type)
                info_ptr.contents.element_type = marshal_type(info.element_type)
                marshal_array(info.allowed_values, info_ptr.contents.allowed_values)
                return ErrorCode.SUCCESS
            except MarshalException:
                return ErrorCode.MARSHAL_ERROR
            except InvalidIndicatorIdException:
                return ErrorCode.INVALID_INDICATOR_ID
            except Exception as e:
                print('Unexpected exception in get_indicator_info: ' + str(e))
                return CALLBACK_EXCEPTION
        callbacks.get_indicator_info = get_indicator_info_t(get_indicator_info)

        return callbacks

    def parse(self, formula):
        parse_result = parse_result_t()
        encoded_formula = formula.encode() if isinstance(formula, str) else formula
        formula_lib.parse_formula(self.instance, encoded_formula, byref(parse_result))
        try:
            check_error(parse_result.error, ParseException, formula)
            return Ast(parse_result.ast, formula)
        finally:
            formula_lib.destroy_error(byref(parse_result.error))

    def evaluate(self, ast):
        evaluate_result = evaluate_result_t()
        formula_lib.evaluate_formula(self.instance, ast.pointer, byref(evaluate_result))
        try:
            check_error(evaluate_result.error, EvaluateException, ast.expression)
            value = unmarshal_value(evaluate_result.value)
            properties = int(evaluate_result.value.properties)
            return value, properties
        finally:
            formula_lib.destroy_value(byref(evaluate_result.value))
            formula_lib.destroy_error(byref(evaluate_result.error))

    def analyze(self, ast, slave_indicator_id):
        analyze_result = analyze_result_t()
        formula_lib.analyze_formula(self.instance, ast.pointer, slave_indicator_id, byref(analyze_result))
        try:
            check_error(analyze_result.error, AnalyzeException, ast.expression)
            code = int(analyze_result.report.code)
            position = int(analyze_result.report.annotation_offset)
            message = analyze_result.report.message
            if message is not None:
                message = message.decode()
            result = AnalysisResult(code, position, message)
            return result
        finally:
            formula_lib.destroy_error(byref(analyze_result.error))
            formula_lib.destroy_error(byref(analyze_result.report))

    def collect_requirements(self, ast):
        collect_result = collect_result_t()
        formula_lib.collect_requirements(self.instance, ast.pointer, 1, 1, 1, byref(collect_result))
        try:
            check_error(collect_result.error, CollectRequirementsException, ast.expression)
            return unmarshal_requirements(collect_result.requirements)
        finally:
            formula_lib.destroy_error(byref(collect_result.error))

    def destroy(self):
        formula_lib.destroy_interpreter(self.instance)

    def __del__(self):
        if self.instance:
            self.destroy()
