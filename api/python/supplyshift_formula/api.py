import os
from ctypes import (CDLL, Structure, Union, POINTER, c_double, c_char_p, c_void_p, CFUNCTYPE,
                    c_int8, c_int16, c_int32, c_int64, c_size_t)
from ctypes.util import find_library


type_id_t = c_int8
indicator_id_t = c_int32
scorecard_id_t = c_int32

class value_t(Structure):
    _pack_ = 1


class value_array_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('size', c_int32),
        ('values', POINTER(value_t)),
    ]


class value_date_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('year', c_int16),
        ('month', c_int8),
        ('day', c_int8),
    ]


class value_location_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('country', c_int64),
        ('province', c_int64),
    ]


class value_gps_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('latitude', c_double),
        ('longitude', c_double),
    ]


class value_t_union(Union):
    _pack_ = 1
    _fields_ = [
        ('int_value', c_int64),
        ('float_value', c_double),
        ('bool_value', c_int8),
        ('str_value', c_char_p),
        ('array', value_array_t),
        ('multi_select', value_array_t),
        ('date', value_date_t),
        ('location', value_location_t),
        ('gps', value_gps_t),
    ]


value_t._anonymous_ = ('union',)
value_t._fields_ = [
    ('type', type_id_t),
    ('properties', c_int8),
    ('union', value_t_union),
]


class wrong_type(Structure):
    _pack_ = 1
    _fields_ = [
        ('indicator_id', indicator_id_t),
    ]


class divide_by_zero(Structure):
    _pack_ = 1
    _fields_ = [
        ('indicator_id', indicator_id_t),
    ]


class error_payload_union_t(Union):
    _pack_ = 1
    _fields_ = [
        ('wrong_type', wrong_type),
        ('divide_by_zero', divide_by_zero),
    ]


class formula_error_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('code', c_int32),
        ('message', c_char_p),
        ('annotation_offset', c_int16),
        ('annotation_length', c_int16),
        ('payload', error_payload_union_t),
    ]


class parse_result_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('error', formula_error_t),
        ('ast', c_void_p),
    ]


class evaluate_result_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('error', formula_error_t),
        ('value', value_t),
    ]


class analyze_result_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('error', formula_error_t),
        ('report', formula_error_t),
    ]


class indicator_info_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('type', type_id_t),
        ('element_type', type_id_t),
        ('allowed_values', value_array_t),
    ]


class extind_arguments_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('indicator_id', indicator_id_t),
        ('from_', value_date_t),
        ('to_', value_date_t),
    ]


class requirements_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('ind', POINTER(indicator_id_t)),
        ('ind_count', c_int32),
        ('extind', POINTER(extind_arguments_t)),
        ('extind_count', c_int32),
        ('extscr', POINTER(scorecard_id_t)),
        ('extscr_count', c_int32),
    ]


class collect_result_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('error', formula_error_t),
        ('requirements', requirements_t),
    ]

get_ind_value_t = CFUNCTYPE(c_int32, indicator_id_t, POINTER(value_t))
get_extind_value_t = CFUNCTYPE(c_int32, indicator_id_t, POINTER(value_date_t), POINTER(value_date_t), POINTER(value_t))
get_extscr_value_t = CFUNCTYPE(c_int32, scorecard_id_t, POINTER(value_t))
get_indicator_info_t = CFUNCTYPE(c_int32, indicator_id_t, POINTER(indicator_info_t))


class callbacks_t(Structure):
    _pack_ = 1
    _fields_ = [
        ('get_ind_value', get_ind_value_t),
        ('get_extind_value', get_extind_value_t),
        ('get_extscr_value', get_extscr_value_t),
        ('get_indicator_info', get_indicator_info_t),
    ]

libc = CDLL(find_library("c"))
libc.malloc.argtypes = [c_size_t]
libc.malloc.restype = c_void_p

lib_dir = os.environ.get('SUPPLYSHIFT_FORMULA_DIR', os.path.dirname(__file__))
path = os.path.join(lib_dir, 'libformula.so')
formula_lib = CDLL(path)

formula_lib.create_interpreter.argtypes = [POINTER(callbacks_t)]
formula_lib.create_interpreter.restype = c_void_p

formula_lib.parse_formula.argtypes = [c_void_p, c_char_p, POINTER(parse_result_t)]
formula_lib.parse_formula.restype = None

formula_lib.evaluate_formula.argtypes = [c_void_p, c_void_p, POINTER(evaluate_result_t)]
formula_lib.evaluate_formula.restype = None

formula_lib.analyze_formula.argtypes = [c_void_p, c_void_p, indicator_id_t, POINTER(analyze_result_t)]
formula_lib.analyze_formula.restype = None

formula_lib.destroy_interpreter.argtypes = [c_void_p]
formula_lib.destroy_interpreter.restype = None

formula_lib.destroy_ast.argtypes = [c_void_p]
formula_lib.destroy_ast.restype = None

formula_lib.destroy_error.argtypes = [POINTER(formula_error_t)]
formula_lib.destroy_error.restype = None

formula_lib.destroy_value.argtypes = [POINTER(value_t)]
formula_lib.destroy_value.restype = None

formula_lib.get_interpreter_version.argtypes = []
formula_lib.get_interpreter_version.restype = c_int32

formula_lib.collect_requirements.argtypes = [c_void_p, c_void_p, c_int8, c_int8, c_int8, POINTER(collect_result_t)]
formula_lib.collect_requirements.restype = None

interpreter_version = int(formula_lib.get_interpreter_version())
