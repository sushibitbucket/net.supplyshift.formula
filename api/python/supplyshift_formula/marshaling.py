from ctypes import create_string_buffer, memmove, c_char_p, sizeof, POINTER, cast, c_int
from datetime import date

from .constants import (NONE, INTEGER, FLOAT, BOOLEAN, STRING, ARRAY, NOT_APPLICABLE, NONE_OF_ABOVE, BLANK, DATE,
                        MULTI_SELECT, GPS, LOCATION)
from .api import libc, value_t
from .exceptions import MarshalException


class NotApplicableType:
    def __repr__(self):
        return 'NotApplicable'

    def __eq__(self, other):
        return isinstance(other, NotApplicableType)

    def __hash__(self):
        return hash(NotApplicableType)


class NoneOfAboveType:
    def __repr__(self):
        return 'NoneOfAbove'

    def __eq__(self, other):
        return isinstance(other, NoneOfAboveType)

    def __hash__(self):
        return hash(NoneOfAboveType)


class BlankType:
    def __repr__(self):
        return 'Blank'

    def __eq__(self, other):
        return isinstance(other, BlankType)

    def __hash__(self):
        return hash(BlankType)

NotApplicable = NotApplicableType()
NoneOfAbove = NoneOfAboveType()
Blank = BlankType()


class MultiSelect:
    def __init__(self, values):
        self.values = values

    def __repr__(self):
        return 'MultiSelect{}'.format(self.values)

    def __eq__(self, other):
        if not isinstance(other, MultiSelect):
            return False

        return self.values == other.values


class Gps:
    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude

    def __repr__(self):
        return 'Gps{{{gps.latitude}, {gps.longitude}}}'.format(gps=self)

    def __eq__(self, other):
        if not isinstance(other, Gps):
            return False

        return self.latitude == other.latitude and self.longitude == other.longitude


class Location:
    def __init__(self, country, province):
        self.country = country
        self.province = province

    def __repr__(self):
        return 'Location{{{loc.country}, {loc.province}}}'.format(loc=self)

    def __eq__(self, other):
        if not isinstance(other, Location):
            return False

        return self.country == other.country and self.province == other.province


def marshal_value(src, dst):
    if src is None:
        dst.type = NONE
    elif isinstance(src, float):
        dst.type = FLOAT
        dst.float_value = src
    elif isinstance(src, bool):
        dst.type = BOOLEAN
        dst.bool_value = src
    elif isinstance(src, int):
        dst.type = INTEGER
        dst.int_value = src
    elif isinstance(src, (str, bytes)):
        if isinstance(src, str):
            src = src.encode()
        dst.type = STRING
        char_str = create_string_buffer(src)
        char_pointer = libc.malloc(len(char_str))
        memmove(char_pointer, char_str, len(char_str))
        dst.str_value = c_char_p(char_pointer)
    elif isinstance(src, list):
        dst.type = ARRAY
        marshal_array(src, dst.array)
    elif isinstance(src, MultiSelect):
        dst.type = MULTI_SELECT
        marshal_array(src.values, dst.multi_select)
    elif src is NotApplicable:
        dst.type = NOT_APPLICABLE
    elif src is NoneOfAbove:
        dst.type = NONE_OF_ABOVE
    elif src is Blank:
        dst.type = BLANK
    elif isinstance(src, date):
        dst.type = DATE
        marshal_date(src, dst.date)
    elif isinstance(src, Gps):
        dst.type = GPS
        dst.gps.latitude = src.latitude
        dst.gps.longitude = src.longitude
    elif isinstance(src, Location):
        dst.type = LOCATION
        dst.location.country = src.country
        dst.location.province = src.province
    else:
        raise MarshalException('Value "' + str(src) + '" marshaling is not implemented')


def unmarshal_value(value):
    if value.type == NONE:
        return None
    if value.type == INTEGER:
        return int(value.int_value)
    if value.type == FLOAT:
        return float(value.float_value)
    if value.type == BOOLEAN:
        return value.bool_value == 1
    if value.type == STRING:
        return value.str_value.decode()
    if value.type == ARRAY:
        result = []
        for i in range(value.array.size):
            result.append(unmarshal_value(value.array.values[i]))
        return result
    if value.type == MULTI_SELECT:
        values = []
        for i in range(value.multi_select.size):
            values.append(unmarshal_value(value.multi_select.values[i]))
        return MultiSelect(values)
    if value.type == NOT_APPLICABLE:
        return NotApplicable
    if value.type == NONE_OF_ABOVE:
        return NoneOfAbove
    if value.type == BLANK:
        return Blank
    if value.type == DATE:
        return unmarshal_date(value.date)
    if value.type == GPS:
        return Gps(value.gps.latitude, value.gps.longitude)
    if value.type == LOCATION:
        return Location(value.location.country, value.location.province)
    raise MarshalException('Unknown value type ' + str(value.type))


def marshal_array(src, dst):
    dst.size = len(src)
    values_pointer = libc.malloc(sizeof(value_t) * len(src))
    dst.values = cast(values_pointer, POINTER(value_t))
    for i in range(len(src)):
        marshal_value(src[i], dst.values[i])


def marshal_date(src, dst):
    dst.year = src.year
    dst.month = src.month
    dst.day = src.day


def unmarshal_date(src):
    return date(src.year, src.month, src.day)


types = {
    None: NONE,
    int: INTEGER,
    float: FLOAT,
    bool: BOOLEAN,
    str: STRING,
    list: ARRAY,
    NotApplicable: NOT_APPLICABLE,
    NoneOfAbove: NONE_OF_ABOVE,
    Blank: BLANK,
    date: DATE,
    MultiSelect: MULTI_SELECT,
    Gps: GPS,
    Location: LOCATION,
}

reverse_types = {v: k for k, v in types.items()}


def marshal_type(src):
    if src not in types:
        raise MarshalException('Type "' + str(src) + '" marshaling is not implemented')
    return types[src]


def unmarshal_type(type_id):
    if type_id not in reverse_types:
        raise MarshalException('Type "' + str(type_id) + '" marshaling is not implemented')
    return reverse_types[type_id]


class Requirements:
    def __init__(self, ind=None, extind=None, extscr=None):
        self.ind = ind or []
        self.extind = extind or []
        self.extscr = extscr or []

    def __repr__(self):
        return 'Requirements{{ind={req.ind}, extind={req.extind}, extscr={req.extscr}}}'.format(req=self)

    def __eq__(self, other):
        return self.ind == other.ind and self.extind == other.extind and self.extscr == other.extscr


def unmarshal_requirements(src):
    reqs = Requirements()
    for i in range(src.ind_count):
        reqs.ind.append(src.ind[i])
    for i in range(src.extind_count):
        extind = src.extind[i]
        from_ = unmarshal_date(extind.from_) if extind.from_.year != 0 else None
        to_ = unmarshal_date(extind.to_) if extind.to_.year != 0 else None
        reqs.extind.append((extind.indicator_id, from_, to_))
    for i in range(src.extscr_count):
        reqs.extscr.append(src.extscr[i])
    return reqs
