class FormulaException(Exception):
    def __init__(self, *args, **kwargs):
        super(FormulaException, self).__init__(*args, **kwargs)
        self.annotation_offset = 0
        self.annotation_length = 0


class MarshalException(FormulaException):
    pass


class ParseException(FormulaException):
    pass


class AnalyzeException(ParseException):
    pass


class EvaluateException(FormulaException):
    pass


class WrongTypeException(EvaluateException):
    def __init__(self, *args, **kwargs):
        super(WrongTypeException, self).__init__(*args, **kwargs)
        self.indicator_id = None


class DivideByZeroException(EvaluateException):
    def __init__(self, *args, **kwargs):
        super(DivideByZeroException, self).__init__(*args, **kwargs)
        self.indicator_id = None


class CallbackException(FormulaException):
    pass


class InvalidIndicatorIdException(CallbackException):
    pass


class CollectRequirementsException(FormulaException):
    pass
