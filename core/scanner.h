//
// Created by aegorov on 12.08.17.
//

#ifndef FORMULA_SCANNER_H
#define FORMULA_SCANNER_H

struct Token
{
    enum class Type
    {
        LEFT_P,
        RIGHT_P,
        LEFT_CB,
        RIGHT_CB,
        LESS,
        GREATER,
        LESS_OR_EQUAL,
        GREATER_OR_EQUAL,
        EQUAL,
        NOT_EQUAL,
        PLUS,
        MINUS,
        DIVIDE,
        MOD,
        TIMES,
        AND,
        OR,
        TRUE,
        FALSE,
        INTEGER,
        FLOAT,
        STRING,
        SPACE,
        END,
        IDENTIFIER,
        NA,
        NOA,
        BLANK,
        COMMA,
        BITWISE_AND,
        HIDDEN,
        USERINPUT
    };

    Type type;
    const char* lexeme = nullptr;
    int pos = 0;
    int len = 0;
};

Token::Type scan(const char** cursor, const char** lexeme);

inline Token scan(const char* expression, const char** cursor)
{
    Token token;
    token.type = scan(cursor, &token.lexeme);
    token.len = *cursor - token.lexeme;
    token.pos = token.lexeme - expression;
    return token;
}

#endif //FORMULA_SCANNER_H
