#include <sstream>
#include "semantic_analyzer.h"
#include "collections_operations.h"

static BinaryOperatorTable filterOperatorTable(const BinaryOperatorTable& impl,
                                               std::initializer_list<Value::Type> types)
{
    BinaryOperatorTable result;

    for (Value::Type type1 : types)
    {
        for (Value::Type type2 : types)
        {
            BinaryOperator op = impl.get(type1, type2);
            if (op.func != nullptr)
            {
                result.add(type1, type2, op);
            }
        }
    }

    return result;
}

static ComparisonOperatorTable filterOperatorTable(const ComparisonOperatorTable& impl,
                                                   std::initializer_list<Value::Type> types)
{
    ComparisonOperatorTable result;

    for (Value::Type type1 : types)
    {
        for (Value::Type type2 : types)
        {
            ComparisonOperator::Func func = impl.get(type1, type2);
            if (func != nullptr)
            {
                result.add(type1, type2, func);
            }
        }
    }

    return result;
}

struct AllowedOps
{
    static BinaryOperatorTable sum;
    static BinaryOperatorTable mult;
    static BinaryOperatorTable diff;
    static BinaryOperatorTable div;
    static BinaryOperatorTable mod;
};

BinaryOperatorTable AllowedOps::sum = filterOperatorTable(Ops::sum, {Value::INTEGER, Value::FLOAT});
BinaryOperatorTable AllowedOps::mult = filterOperatorTable(Ops::mult, {Value::INTEGER, Value::FLOAT});
BinaryOperatorTable AllowedOps::diff = filterOperatorTable(Ops::diff, {Value::INTEGER, Value::FLOAT});
BinaryOperatorTable AllowedOps::div = filterOperatorTable(Ops::div, {Value::INTEGER, Value::FLOAT});
BinaryOperatorTable AllowedOps::mod = filterOperatorTable(Ops::mod, {Value::INTEGER, Value::FLOAT});

static bool isCompatible(Value::Type type1, Value::Type type2)
{
    if (type1 == type2)
        return true;

    if (type1 == Value::Type::NOT_APPLICABLE || type2 == Value::Type::NOT_APPLICABLE)
        return true;

    if (type1 == Value::Type::BLANK || type2 == Value::Type::BLANK)
        return true;

    if (type1 == Value::Type::NONE_OF_ABOVE || type2 == Value::Type::NONE_OF_ABOVE)
        return true;

    if (type1 == Value::Type::INTEGER && type2 == Value::Type::FLOAT)
        return true;

    if (type1 == Value::Type::FLOAT && type2 == Value::Type::INTEGER)
        return true;

    return false;
}

static bool isCollection(Value::Type type)
{
    return type == Value::ARRAY || type == Value::MULTI_SELECT;
}

static bool isCompatible(const Trait& left, const Trait& right)
{
    if (isCollection(left.type) && isCollection(right.type))
        return isCompatible(left.element_type, right.element_type);
    else
        return isCompatible(left.type, right.type);
}

static bool valuesFitsTo(const Trait& left, const Trait& right, const Value** wrong_value)
{
    if (left.type == Value::NOT_APPLICABLE && !contains(right.allowed_values, Value::NotApplicable))
    {
        if (!left.values.empty())
            *wrong_value = left.values.data();
        return false;
    }

    if (left.type == Value::NONE_OF_ABOVE && !contains(right.allowed_values, Value::NoneOfAbove))
    {
        if (!left.values.empty())
            *wrong_value = left.values.data();
        return false;
    }

    if (left.type == Value::BLANK && !contains(right.allowed_values, Value::Blank))
    {
        if (!left.values.empty())
            *wrong_value = left.values.data();
        return false;
    }

    if (!left.values.empty())
    {
        if (!right.allowed_values_limited)
            return true;

        for (const Value& value : left.values)
        {
            if (!contains(right.allowed_values, value))
            {
                *wrong_value = &value;
                return false;
            }
        }

        return true;
    }

    if (!left.allowed_values_limited || !right.allowed_values_limited)
        return true;

    return intersects(left.allowed_values, right.allowed_values);
}

static bool allowedValuesMatches(const Trait& left, const Trait& right, const Value** wrong_value)
{
    return valuesFitsTo(left, right, wrong_value) && valuesFitsTo(right, left, wrong_value);
}

static void unwrap_array(Trait& trait)
{
    if (trait.type == Value::ARRAY)
    {
        trait.type = trait.element_type;
        trait.element_type = Value::NONE;
    }
}

Traits SemanticAnalyzer::analyze(const Expression& expression, int32_t slave_indicator_id)
{
    Trait slave_trait = getIndicatorInfo(slave_indicator_id, 0);
    is_matrix_section = slave_trait.type == Value::ARRAY;
    unwrap_array(slave_trait);

    analyze(expression);

    for (const Trait& result_trait : result_traits)
    {
        if (!isCompatible(result_trait, slave_trait))
        {
            int position = 0;
            if (!result_trait.values.empty())
            {
                const Expression* source = result_trait.values[0].getSource();
                if (source != nullptr)
                    position = source->position;
            }

            throw TypeMismatchException((std::string("Formula result type ") + to_string(result_trait) +
                                         " is not compatible with slave indicator " +
                                         to_string(slave_trait)).c_str(), position);
        }

        const Value* wrong_value = nullptr;
        if (!allowedValuesMatches(result_trait, slave_trait, &wrong_value))
        {
            int position = wrong_value != nullptr && wrong_value->getSource() != nullptr ? wrong_value->getSource()->position : 0;
            throw AllowedValuesMismatchException((std::string("Allowed values mismatch:") +
                                                  "\nSlave: " + to_string(slave_trait) +
                                                  "\nResult: " + to_string(result_trait)).c_str(), position);
        }
    }

    return result_traits;
}

Traits SemanticAnalyzer::analyze(const Expression& expression)
{
    result_traits.clear();
    expression.visit(*this);
    return result_traits;
}

void SemanticAnalyzer::visit(const Constant& constant)
{
    result_traits.clear();
    Value value = constant.value;
    value.setSource(&constant);
    result_traits.push_back(Trait{value.type(), {}, {value}});
}

void SemanticAnalyzer::visit(const SumExpression& expression)
{
    visitArithmeticOperator(expression, *expression.left, *expression.right, AllowedOps::sum, "+");
}

void SemanticAnalyzer::visit(const MultiplyExpression& expression)
{
    visitArithmeticOperator(expression, *expression.left, *expression.right, AllowedOps::mult, "*");
}

void SemanticAnalyzer::visit(const DiffExpression& expression)
{
    visitArithmeticOperator(expression, *expression.left, *expression.right, AllowedOps::diff, "-");
}

void SemanticAnalyzer::visit(const DivideExpression& expression)
{
    visitArithmeticOperator(expression, *expression.left, *expression.right, AllowedOps::div, "/");
}

void SemanticAnalyzer::visit(const ModExpression& expression)
{
    visitArithmeticOperator(expression, *expression.left, *expression.right, AllowedOps::mod, "%");
}

void SemanticAnalyzer::visit(const IfExpression& expression)
{
    Traits condition_traits = analyze(*expression.condition);
    for (const auto& condition_trait : condition_traits)
    {
        if (condition_trait.type != Value::BOOLEAN)
        {
            throw TypeMismatchException("IF function should take boolean expression as first argument",
                                        expression.condition->position);
        }
    }

    Traits then_traits = analyze(*expression.thenBody);
    Traits else_traits = analyze(*expression.elseBody);
    merge_traits(then_traits, else_traits);
    result_traits = std::move(then_traits);
}

void SemanticAnalyzer::visit(const IfErrorExpression& expression)
{
    Traits body_traits = analyze(*expression.expr);
    Traits then_traits = analyze(*expression.thenBody);
    merge_traits(body_traits, then_traits);
    result_traits = std::move(body_traits);
}

void SemanticAnalyzer::visit(const EqualExpression& expression)
{
    visitEqualOperator(expression, *expression.left, *expression.right, "=");
}

void SemanticAnalyzer::visit(const NotEqualExpression& expression)
{
    visitEqualOperator(expression, *expression.left, *expression.right, "!=");
}

void SemanticAnalyzer::visit(const LessExpression& expression)
{
    visitCompareOperator(expression, *expression.left, *expression.right, Ops::less, "<");
}

void SemanticAnalyzer::visit(const LessOrEqualExpression& expression)
{
    visitCompareOperator(expression, *expression.left, *expression.right, Ops::less, "<=");
}

void SemanticAnalyzer::visit(const GreaterExpression& expression)
{
    visitCompareOperator(expression, *expression.left, *expression.right, Ops::less, ">");
}

void SemanticAnalyzer::visit(const GreaterOrEqualExpression& expression)
{
    visitCompareOperator(expression, *expression.left, *expression.right, Ops::less, ">=");
}

void SemanticAnalyzer::visit(const OrExpression& expression)
{
    Traits left_traits = analyze(*expression.left);
    Traits right_traits = analyze(*expression.right);
    for (const auto& left_trait : left_traits)
    {
        for (const auto& right_trait : right_traits)
        {
            bool left_boolean = left_trait.type == Value::BOOLEAN;
            bool right_boolean = right_trait.type == Value::BOOLEAN;
            if (!left_boolean || !right_boolean)
            {
                int position = left_boolean ? expression.right->position : expression.left->position;
                throw TypeMismatchException((std::string("Operator || takes boolean operands only, got ") +
                                             Value::typeName(left_trait.type) + " and " +
                                             Value::typeName(right_trait.type)).c_str(), position
                );
            }
        }
    }

    result_traits = {{Value::Type::BOOLEAN}};
}

void SemanticAnalyzer::visit(const AndExpression& expression)
{
    Traits left_traits = analyze(*expression.left);
    Traits right_traits = analyze(*expression.right);
    for (const auto& left_trait : left_traits)
    {
        for (const auto& right_trait : right_traits)
        {
            bool left_boolean = left_trait.type == Value::BOOLEAN;
            bool right_boolean = right_trait.type == Value::BOOLEAN;
            if (!left_boolean || !right_boolean)
            {
                int position = left_boolean ? expression.right->position : expression.left->position;
                throw TypeMismatchException((std::string("Operator && takes boolean operands only, got ") +
                                             Value::typeName(left_trait.type) + " and " +
                                             Value::typeName(right_trait.type)).c_str(), position
                );
            }
        }
    }
    result_traits = {{Value::Type::BOOLEAN}};
}

void SemanticAnalyzer::visit(const NegateExpression& expression)
{
    Traits traits = analyze(*expression.value);
    result_traits.clear();
    for (auto& trait : traits)
    {
        if (trait.type != Value::INTEGER && trait.type != Value::FLOAT)
        {
            throw TypeMismatchException((std::string("Unary operator - can be applied to numbers only, got ") +
                                         Value::typeName(trait.type)).c_str(), expression.value->position);
        }
        append_trait(result_traits, {trait.type});
    }
}

void SemanticAnalyzer::visit(const ExtScrExpression& expression)
{
    result_traits = {{Value::Type::ARRAY}};
}

void SemanticAnalyzer::visit(const ArrayExpression& expression)
{
    std::vector<Value> values;
    Value::Type element_type = Value::NONE;
    for (const auto& item_expr : expression.values)
    {
        auto item = dynamic_cast<Constant*>(item_expr.get());
        if (item == nullptr)
        {
            throw WrongArgumentException("Array should contain only constants", item_expr->position);
        }
        if (element_type == Value::NONE)
        {
            element_type = item->value.type();
        }
        else if (element_type != item->value.type())
        {
            throw WrongArgumentException("Array should contain items with the same type", item_expr->position);
        }
        Value item_value = item->value;
        item_value.setSource(item);
        values.push_back(item_value);
    }
    result_traits = {{Value::ARRAY, {}, values, element_type}};
}

void SemanticAnalyzer::visit(const AndFuncExpression& expression)
{
    for (const auto& arg : expression.args)
    {
        Traits arg_traits = analyze(*arg);
        for (const auto& arg_trait : arg_traits)
        {
            if (arg_trait.type != Value::BOOLEAN)
                throw TypeMismatchException("AND function takes only boolean arguments", arg->position);
        }
    }
    result_traits = {{Value::Type::BOOLEAN}};
}

void SemanticAnalyzer::visit(const OrFuncExpression& expression)
{
    for (const auto& arg : expression.args)
    {
        Traits arg_traits = analyze(*arg);
        for (const auto& arg_trait : arg_traits)
        {
            if (arg_trait.type != Value::BOOLEAN)
                throw TypeMismatchException("OR function takes only boolean arguments", arg->position);
        }
    }
    result_traits = {{Value::Type::BOOLEAN}};
}

void SemanticAnalyzer::visit(const SumFuncExpression& expression)
{
    for (const auto& arg : expression.args)
    {
        Traits arg_traits = analyze(*arg);
        for (const auto& arg_trait : arg_traits)
        {
            if (arg_trait.type != Value::ARRAY)
                throw TypeMismatchException("SUM function takes only collection arguments", arg->position);

        }
    }
    result_traits = {{Value::Type::FLOAT}};
}

void SemanticAnalyzer::visit(const AvgFuncExpression& expression)
{
    for (const auto& arg : expression.args)
    {
        Traits arg_traits = analyze(*arg);
        for (const auto& arg_trait : arg_traits)
        {
            if (arg_trait.type != Value::ARRAY)
                throw TypeMismatchException("AVG function takes only collection arguments", arg->position);
        }
    }
    result_traits = {{Value::Type::FLOAT}};
}

void SemanticAnalyzer::visit(const CountFuncExpression& expression)
{
    for (const auto& arg : expression.args)
    {
        Traits arg_traits = analyze(*arg);
        for (const auto& arg_trait : arg_traits)
        {
            if (arg_trait.type != Value::ARRAY)
                throw TypeMismatchException("COUNT function takes only collection arguments", arg->position);
        }
    }
    result_traits = {{Value::Type::INTEGER}};
}

void SemanticAnalyzer::visit(const ContainsExpression& expression)
{
    Traits subset_traits = analyze(*expression.subset);
    Traits superset_traits = analyze(*expression.superset);

    for (const auto& subset_trait : subset_traits)
    {
        for (const auto& superset_trait : superset_traits)
        {
            if (!isCollection(subset_trait.type))
                throw TypeMismatchException("CONTAINS function takes only collection arguments", expression.subset->position);

            if (!isCollection(superset_trait.type))
                throw TypeMismatchException("CONTAINS function takes only collection arguments", expression.superset->position);

            if (!isCompatible(subset_trait.element_type, superset_trait.element_type))
            {
                std::stringstream ss;
                ss << "Can't apply CONTAINS function to " << subset_trait << " and " << superset_trait;
                throw TypeMismatchException(ss.str().c_str(), expression.position);
            }

            const Value* wrong_value = nullptr;
            if (!allowedValuesMatches(subset_trait, superset_trait, &wrong_value))
            {
                int position = wrong_value != nullptr && wrong_value->getSource() != nullptr ? wrong_value->getSource()->position : expression.position;
                throw AllowedValuesMismatchException((std::string("Allowed values mismatch:") +
                                                      "\nSubset: " + to_string(subset_trait) +
                                                      "\nSuperset: " + to_string(superset_trait)).c_str(), position);
            }
        }
    }

    result_traits = {{Value::Type::BOOLEAN}};
}

void SemanticAnalyzer::visit(const AddPropertiesExpression& expression)
{
    Traits left_traits = analyze(*expression.left);
    Traits right_traits = analyze(*expression.right);
    auto right = dynamic_cast<Constant*>(expression.right.get());
    if (right == nullptr || right->value.type() != Value::Type::BLANK)
        throw TypeMismatchException("Operator & should have property constant as right operand", expression.right->position);
    result_traits = left_traits;
}

void SemanticAnalyzer::visit(const IndicatorExpression& ind)
{
    result_traits = {getIndicatorInfo(ind.indicator_id, ind.position)};
    if (is_matrix_section)
    {
        for (Trait& trait : result_traits)
            unwrap_array(trait);
    }
}

void SemanticAnalyzer::visit(const ExtIndicatorExpression& extind)
{
    Trait indicator_trait = getIndicatorInfo(extind.indicator_id, extind.position);

    Value::Type type = indicator_trait.type == Value::ARRAY ? indicator_trait.element_type : indicator_trait.type;

    result_traits = {{Value::Type::ARRAY, {}, {}, type}};
}

void SemanticAnalyzer::visitArithmeticOperator(const Expression& expr, const Expression& left, const Expression& right,
                                               const BinaryOperatorTable& table, const char* opName)
{
    Traits left_traits = analyze(left);
    Traits right_traits = analyze(right);
    result_traits.clear();

    for (const Trait& left_trait : left_traits)
    {
        for (const Trait& right_trait : right_traits)
        {
            BinaryOperator op = table.get(left_trait.type, right_trait.type);
            if (op.func == nullptr || !isCompatible(left_trait.type, right_trait.type))
            {
                throw TypeMismatchException(
                        (std::string("Operator ") + opName + " is not implemented for types " +
                         Value::typeName(left_trait.type) + " and " +
                         Value::typeName(right_trait.type)).c_str(), expr.position);
            }
            append_trait(result_traits, {op.result});
        }
    }
}

void SemanticAnalyzer::visitEqualOperator(const Expression& expr, const Expression& left, const Expression& right,
                                          const char* opName)
{
    Traits left_traits = analyze(left);
    Traits right_traits = analyze(right);

    result_traits = {{Value::BOOLEAN}};
    for (const Trait& left_trait : left_traits)
    {
        for (const Trait& right_trait : right_traits)
        {
            ComparisonOperator::Func func = Ops::equal.get(left_trait.type, right_trait.type);
            if (func == nullptr || !isCompatible(left_trait, right_trait))
            {
                throw TypeMismatchException((std::string("Not compatible types for compare operation: ") +
                                             to_string(left_trait) + " and " +
                                             to_string(right_trait)).c_str(), expr.position);
            }

            const Value* wrong_value = nullptr;
            if (!allowedValuesMatches(left_trait, right_trait, &wrong_value))
            {
                throw AllowedValuesMismatchException((std::string("Allowed values mismatch:") +
                                                      "\nLeft: " + to_string(left_trait) +
                                                      "\nRight: " + to_string(right_trait)).c_str(), expr.position);
            }
        }
    }
}

void SemanticAnalyzer::visitCompareOperator(const Expression& expr, const Expression& left, const Expression& right,
                                            const ComparisonOperatorTable& table, const char* opName)
{
    Traits left_traits = analyze(left);
    Traits right_traits = analyze(right);

    result_traits = {{Value::BOOLEAN}};
    for (const Trait& left_trait : left_traits)
    {
        for (const Trait& right_trait : right_traits)
        {
            bool operatorFound = false;

            ComparisonOperator::Func func = table.get(left_trait.type, right_trait.type);
            if (func != nullptr && isCompatible(left_trait.type, right_trait.type))
                operatorFound = true;

            if (!operatorFound)
            {
                throw TypeMismatchException((std::string("Not compatible types for compare operation: ") +
                                             Value::typeName(left_trait.type) + " and " +
                                             Value::typeName(right_trait.type)).c_str(), expr.position);
            }
        }
    }
}

Trait SemanticAnalyzer::getIndicatorInfo(int indicator_id, int position) const
{
    if (type_provider == nullptr)
        throw AnalysisException("No type provider", position);

    return type_provider->getIndicatorInfo(indicator_id, position);
}
