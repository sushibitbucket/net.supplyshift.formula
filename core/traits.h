#ifndef FORMULA_TRAITS_H
#define FORMULA_TRAITS_H

#include <vector>
#include "value.h"

class Trait
{
public:
    Trait(Value::Type type,
          const std::vector<Value>& allowed_values = {},
          const std::vector<Value>& values = {},
          Value::Type element_type = Value::NONE
    );

    Value::Type type;
    Value::Type element_type; // used when type is ARRAY
    std::vector<Value> allowed_values;
    std::vector<Value> values;
    bool allowed_values_limited = false;
};

bool operator==(const Trait& left, const Trait& right);
std::ostream& operator<<(std::ostream& os, const Trait& trait);

typedef std::vector<Trait> Traits;

std::string to_string(const Trait& trait);
std::string to_string(const Traits& type_set);

void append_trait(Traits& traits, const Trait& trait);
void merge_traits(Traits& a, const Traits& b);

#endif //FORMULA_TRAITS_H
