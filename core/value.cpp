//
// Created by aegorov on 14.08.17.
//

#include <iostream>
#include "value.h"
#include "exceptions.h"

Value Value::NotApplicable(Value::Type::NOT_APPLICABLE);
Value Value::NoneOfAbove(Value::Type::NONE_OF_ABOVE);
Value Value::Blank(Value::Type::BLANK);

const char* Value::typeName(Value::Type type)
{
    static const char* NAMES[] = {
        "NONE",
        "INTEGER",
        "FLOAT",
        "BOOLEAN",
        "STRING",
        "ARRAY",
        "NOT_APPLICABLE",
        "NONE_OF_ABOVE",
        "BLANK",
        "DATE",
        "MULTI_SELECT",
        "LOCATION",
        "GPS"
    };

    static_assert(sizeof(NAMES) / sizeof(NAMES[0]) == TYPE_COUNT, "Not all types names are declared");

    if (!isValidType(type))
        return "UNKNOWN";

    int type_id = (int)type;
    return NAMES[type_id];
}

bool Value::isValidType(Value::Type type)
{
    return type >= Value::Type::NONE && type < Value::Type::TYPE_COUNT;
}

Value::Value(const Value& other)
{
    *this = other;
}

Value& Value::operator=(const Value& other)
{
    switch (m_type)
    {
        case STRING:
            free(str_value);
            break;
        case ARRAY:
        case MULTI_SELECT:
            delete[] elements.values;
            break;
        default:break;
    }

    m_type = other.m_type;
    properties = other.properties;
    source = other.source;

    switch (m_type)
    {
        case NONE:break;
        case NOT_APPLICABLE:break;
        case NONE_OF_ABOVE:break;
        case BLANK:break;
        case INTEGER:
            int_value = other.int_value;
            break;
        case FLOAT:
            float_value = other.float_value;
            break;
        case BOOLEAN:
            bool_value = other.bool_value;
            break;
        case ARRAY:
        case MULTI_SELECT:
            elements.size = other.elements.size;
            elements.values = new Value[elements.size];
            for (int i = 0; i < elements.size; ++i)
                elements.values[i] = other.elements.values[i];
            break;
        case STRING:
            str_value = strdup(other.str_value);
            break;
        case DATE:
            date = other.date;
            break;
        case LOCATION:
            location = other.location;
            break;
        case GPS:
            gps = other.gps;
            break;
        default:
            throw EvaluateException("Value assignment operator is not implemented for the type");
    }
    return *this;
}

Value::~Value()
{
    switch (m_type)
    {
        case Value::STRING:
            free(str_value);
            break;
        case Value::ARRAY:
        case Value::MULTI_SELECT:
            delete[] elements.values;
            break;
        default:break;
    }
}

int64_t Value::getInt() const
{
    if (m_type != Type::INTEGER)
        throw EvaluateException("Value is not an integer");
    return int_value;
}

double Value::getFloat() const
{
    if (m_type != Type::FLOAT)
        throw EvaluateException("Value is not a float");
    return float_value;
}

bool Value::getBool() const
{
    if (m_type != Type::BOOLEAN)
        throw EvaluateException("Value is not a bool");
    return bool_value;
}

const char* Value::getString() const
{
    if (m_type != Type::STRING)
        throw EvaluateException("Value is not a string");
    return str_value;
}

Value* Value::getElements() const
{
    if (m_type != Type::ARRAY && m_type != MULTI_SELECT)
        throw EvaluateException("Value is not an array");
    return elements.values;
}

int32_t Value::getElementCount() const
{
    if (m_type != Type::ARRAY && m_type != MULTI_SELECT)
        throw EvaluateException("Value is not an array or multiselect");
    return elements.size;
}

const Date& Value::getDate() const
{
    if (m_type != Type::DATE)
        throw EvaluateException("Value is not a date");
    return date;
}

int64_t Value::getCountry() const
{
    if (m_type != Type::LOCATION)
        throw EvaluateException("Value is not a location");
    return location.country;
}

int64_t Value::getProvince() const
{
    if (m_type != Type::LOCATION)
        throw EvaluateException("Value is not a location");
    return location.province;
}

double Value::getLatitude() const
{
    if (m_type != Type::GPS)
        throw EvaluateException("Value is not a gps coordinates");
    return gps.latitude;
}

double Value::getLongitude() const
{
    if (m_type != Type::GPS)
        throw EvaluateException("Value is not a gps coordinates");
    return gps.longitude;
}

std::ostream& operator<<(std::ostream& os, const Value& v)
{
    switch (v.type())
    {
        case Value::Type::INTEGER:
            return os << v.getInt();

        case Value::Type::FLOAT:
            return os << v.getFloat();

        case Value::Type::BOOLEAN:
            return os << (v.getBool() ? "true" : "false");

        case Value::Type::STRING:
            return os << "\"" << v.getString() << "\"";

        case Value::Type::ARRAY:
        case Value::Type::MULTI_SELECT:
        {
            os << "[";
            auto array = v.getElements();
            int32_t size = v.getElementCount();
            if (size != 0)
            {
                os << array[0];
                for (int i = 1; i < size; ++i)
                    os << ", " << array[i];
            }
            os << "]";
            return os;
        }

        case Value::Type::NONE:
            return os << "None";

        case Value::Type::NOT_APPLICABLE:
            return os << "NotApplicable";

        case Value::Type::NONE_OF_ABOVE:
            return os << "NoneOfAbove";

        case Value::Type::BLANK:
            return os << "Blank";

        case Value::Type::DATE:
        {
            const Date& date = v.getDate();
            return os << date.month << "/" << date.day << "/" << date.year;
        }

        case Value::Type::LOCATION:
            return os << "Location";

        case Value::Type::GPS:
            return os << "Gps";

        default:
            return os << "Type" << static_cast<int>(v.type());
    }
}

std::ostream& operator<<(std::ostream& os, const std::vector<Value>& values)
{
    os << "[";
    if (!values.empty())
    {
        os << values[0];
        for (size_t i = 1; i < values.size(); ++i)
        {
            const Value& value = values[i];
            os << ", " << value;
        }
    }
    os << "]";
    return os;
}
