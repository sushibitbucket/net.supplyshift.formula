//
// Created by aegorov on 17.08.17.
//

#ifndef FORMULA_DATA_PROVIDER_H
#define FORMULA_DATA_PROVIDER_H

#include "value.h"

class DataProvider
{
public:
    virtual ~DataProvider() = default;
    virtual Value getIndicatorValue(int indicator_id, int position) const = 0;
    virtual Value getExtIndicatorValue(int indicator_id, Date from, Date to, int position) const = 0;
    virtual Value getExtScorecardValue(int scorecard_id, int position) const = 0;
};

#endif //FORMULA_DATA_PROVIDER_H
