//
// Created by aegorov on 17.08.17.
//

#ifndef FORMULA_EXPRESSION_VISITOR_H
#define FORMULA_EXPRESSION_VISITOR_H

#include "expression.h"

class ExpressionVisitor
{
public:
    virtual void visit(const Constant&) = 0;
    virtual void visit(const SumExpression&) = 0;
    virtual void visit(const MultiplyExpression&) = 0;
    virtual void visit(const DiffExpression&) = 0;
    virtual void visit(const DivideExpression&) = 0;
    virtual void visit(const ModExpression&) = 0;
    virtual void visit(const IfExpression&) = 0;
    virtual void visit(const IfErrorExpression&) = 0;
    virtual void visit(const EqualExpression&) = 0;
    virtual void visit(const NotEqualExpression&) = 0;
    virtual void visit(const LessExpression&) = 0;
    virtual void visit(const LessOrEqualExpression&) = 0;
    virtual void visit(const GreaterExpression&) = 0;
    virtual void visit(const GreaterOrEqualExpression&) = 0;
    virtual void visit(const OrExpression&) = 0;
    virtual void visit(const AndExpression&) = 0;
    virtual void visit(const NegateExpression&) = 0;
    virtual void visit(const IndicatorExpression&) = 0;
    virtual void visit(const ExtIndicatorExpression&) = 0;
    virtual void visit(const ExtScrExpression&) = 0;
    virtual void visit(const ArrayExpression&) = 0;
    virtual void visit(const AndFuncExpression&) = 0;
    virtual void visit(const OrFuncExpression&) = 0;
    virtual void visit(const SumFuncExpression&) = 0;
    virtual void visit(const AvgFuncExpression&) = 0;
    virtual void visit(const CountFuncExpression&) = 0;
    virtual void visit(const ContainsExpression&) = 0;
    virtual void visit(const AddPropertiesExpression&) = 0;
};

class RecursiveExpressionVisitor : public ExpressionVisitor
{
public:
    void visit(const Constant& constant) override
    {}

    void visit(const SumExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const MultiplyExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const DivideExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const DiffExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const ModExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const EqualExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const NotEqualExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const LessExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const LessOrEqualExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const GreaterExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const GreaterOrEqualExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const OrExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const AndExpression& op) override
    {
        op.left->visit(*this);
        op.right->visit(*this);
    }

    void visit(const IfExpression& cond) override
    {
        cond.condition->visit(*this);
        cond.thenBody->visit(*this);
        cond.elseBody->visit(*this);
    }

    void visit(const IfErrorExpression& cond) override
    {
        cond.expr->visit(*this);
        cond.thenBody->visit(*this);
    }

    void visit(const NegateExpression& negate) override
    {
        negate.value->visit(*this);
    }

    void visit(const IndicatorExpression& ind) override
    {}

    void visit(const ExtIndicatorExpression& extind) override
    {}

    void visit(const ExtScrExpression& extscr) override
    {}

    void visit(const ArrayExpression& array) override
    {
        for (const auto& val : array.values)
            val->visit(*this);
    }

    void visit(const AndFuncExpression& call) override
    {
        for (const auto& arg : call.args)
            arg->visit(*this);
    }

    void visit(const AddPropertiesExpression& expr) override
    {
        expr.left->visit(*this);
        expr.right->visit(*this);
    }

    void visit(const OrFuncExpression& call) override
    {
        for (const auto& arg : call.args)
            arg->visit(*this);
    }

    void visit(const SumFuncExpression& call) override
    {
        for (const auto& arg : call.args)
            arg->visit(*this);
    }

    void visit(const AvgFuncExpression& call) override
    {
        for (const auto& arg : call.args)
            arg->visit(*this);
    }

    void visit(const CountFuncExpression& call) override
    {
        for (const auto& arg : call.args)
            arg->visit(*this);
    }

    void visit(const ContainsExpression& expr) override
    {
        expr.subset->visit(*this);
        expr.superset->visit(*this);
    }
};

#endif //FORMULA_EXPRESSION_VISITOR_H
