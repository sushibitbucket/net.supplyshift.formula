#ifndef FORMULA_COLLECTIONS_OPERATIONS_H
#define FORMULA_COLLECTIONS_OPERATIONS_H

#include "value.h"
#include <algorithm>

bool contains(const std::vector<Value>& v, const Value& value);

bool contains(const Value* v, size_t size, const Value& value);

bool contains(const Value* superset, size_t superset_size,
              const Value* subset, size_t subset_size);

bool contains(const std::vector<Value>& superset,
              const std::vector<Value>& subset);

bool intersects(const Value* a, size_t a_size,
                const Value* b, size_t b_size);

bool intersects(const std::vector<Value>& a,
                const std::vector<Value>& b);

#endif //FORMULA_COLLECTIONS_OPERATIONS_H
