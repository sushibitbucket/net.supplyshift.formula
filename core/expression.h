//
// Created by aegorov on 13.08.17.
//

#ifndef FORMULA_EXPRESSION_H
#define FORMULA_EXPRESSION_H

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "value.h"

class ExpressionVisitor;

class Expression
{
public:
    explicit Expression(int position) : position(position) {}
    virtual ~Expression() = default;
    virtual void visit(ExpressionVisitor& visitor) const = 0;

    const int position;
};

class Constant : public Expression
{
public:
    explicit Constant(int position, Value value) :
            Expression(position),
            value(std::move(value))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    const Value value;
};

class SumExpression : public Expression
{
public:
    SumExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class MultiplyExpression : public Expression
{
public:
    MultiplyExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class DiffExpression : public Expression
{
public:
    DiffExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class EqualExpression : public Expression
{
public:
    EqualExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class NotEqualExpression : public Expression
{
public:
    NotEqualExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class LessExpression : public Expression
{
public:
    LessExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class LessOrEqualExpression : public Expression
{
public:
    LessOrEqualExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class GreaterExpression : public Expression
{
public:
    GreaterExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class GreaterOrEqualExpression : public Expression
{
public:
    GreaterOrEqualExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class OrExpression : public Expression
{
public:
    OrExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class AndExpression : public Expression
{
public:
    AndExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class AddPropertiesExpression : public Expression
{
public:
    AddPropertiesExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class DivideExpression : public Expression
{
public:
    DivideExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class ModExpression : public Expression
{
public:
    ModExpression(int position, std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
            Expression(position),
            left(std::move(left)),
            right(std::move(right))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class IfExpression : public Expression
{
public:
    IfExpression(int position,
                 std::unique_ptr<Expression> condition,
                 std::unique_ptr<Expression> thenBody,
                 std::unique_ptr<Expression> elseBody) :
            Expression(position),
            condition(std::move(condition)),
            thenBody(std::move(thenBody)),
            elseBody(std::move(elseBody))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> condition;
    std::unique_ptr<Expression> thenBody;
    std::unique_ptr<Expression> elseBody;
};

class IfErrorExpression : public Expression
{
public:
    IfErrorExpression(int position,
                      std::unique_ptr<Expression> expr,
                      std::unique_ptr<Expression> thenBody) :
            Expression(position),
            expr(std::move(expr)),
            thenBody(std::move(thenBody))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> expr;
    std::unique_ptr<Expression> thenBody;
};

class NegateExpression : public Expression
{
public:
    explicit NegateExpression(int position, std::unique_ptr<Expression> value) :
            Expression(position),
            value(std::move(value))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> value;
};

class IndicatorExpression : public Expression
{
public:
    explicit IndicatorExpression(int position, int32_t indicator_id) :
            Expression(position),
            indicator_id(indicator_id)
    {}

    void visit(ExpressionVisitor& visitor) const override;

    const int32_t indicator_id;
};

class ExtIndicatorExpression : public Expression
{
public:
    explicit ExtIndicatorExpression(int position, int32_t indicator_id, Date from = {0, 0, 0}, Date to = {0, 0, 0}) :
            Expression(position),
            indicator_id(indicator_id),
            from(from),
            to(to)
    {}

    void visit(ExpressionVisitor& visitor) const override;

    const int32_t indicator_id;
    const Date from;
    const Date to;
};

class ExtScrExpression : public Expression
{
public:
    explicit ExtScrExpression(int position, int32_t scorecard_id) :
            Expression(position),
            scorecard_id(scorecard_id)
    {}

    void visit(ExpressionVisitor& visitor) const override;

    const int32_t scorecard_id;
};

class ArrayExpression : public Expression
{
public:
    explicit ArrayExpression(int position, std::vector<std::unique_ptr<Expression>> values) :
            Expression(position),
            values(std::move(values))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::vector<std::unique_ptr<Expression>> values;
};

class AndFuncExpression : public Expression
{
public:
    explicit AndFuncExpression(int position, std::vector<std::unique_ptr<Expression>> args) :
            Expression(position),
            args(std::move(args))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::vector<std::unique_ptr<Expression>> args;
};

class OrFuncExpression : public Expression
{
public:
    explicit OrFuncExpression(int position, std::vector<std::unique_ptr<Expression>> args) :
            Expression(position),
            args(std::move(args))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::vector<std::unique_ptr<Expression>> args;
};

class SumFuncExpression : public Expression
{
public:
    explicit SumFuncExpression(int position, std::vector<std::unique_ptr<Expression>> args) :
            Expression(position),
            args(std::move(args))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::vector<std::unique_ptr<Expression>> args;
};

class AvgFuncExpression : public Expression
{
public:
    explicit AvgFuncExpression(int position, std::vector<std::unique_ptr<Expression>> args) :
            Expression(position),
            args(std::move(args))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::vector<std::unique_ptr<Expression>> args;
};

class CountFuncExpression : public Expression
{
public:
    explicit CountFuncExpression(int position, std::vector<std::unique_ptr<Expression>> args) :
            Expression(position),
            args(std::move(args))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::vector<std::unique_ptr<Expression>> args;
};

class ContainsExpression : public Expression
{
public:
    explicit ContainsExpression(int position, std::unique_ptr<Expression> subset, std::unique_ptr<Expression> superset) :
            Expression(position),
            subset(std::move(subset)),
            superset(std::move(superset))
    {}

    void visit(ExpressionVisitor& visitor) const override;

    std::unique_ptr<Expression> subset;
    std::unique_ptr<Expression> superset;
};

#endif //FORMULA_EXPRESSION_H
