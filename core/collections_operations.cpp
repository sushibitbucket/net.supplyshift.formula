#include "collections_operations.h"
#include "operators.h"

bool contains(const std::vector<Value>& v, const Value& value)
{
    return std::find(begin(v), end(v), value) != v.end();
}

bool contains(const Value* v, size_t size, const Value& value)
{
    const Value* end = v + size;
    return std::find(v, end, value) != end;
}

bool contains(const Value* superset, size_t superset_size, const Value* subset, size_t subset_size)
{
    if (subset_size == 0)
        return false;
    for (size_t i = 0; i < subset_size; ++i)
    {
        if (!contains(superset, superset_size, subset[i]))
            return false;
    }
    return true;
}

bool contains(const std::vector<Value>& superset, const std::vector<Value>& subset)
{
    return contains(superset.data(), superset.size(),
                    subset.data(), subset.size());
}

bool intersects(const Value* a, size_t a_size, const Value* b, size_t b_size)
{
    for (size_t i = 0; i < b_size; ++i)
    {
        if (contains(a, a_size, b[i]))
            return true;
    }
    return false;
}

bool intersects(const std::vector<Value>& a, const std::vector<Value>& b)
{
    return intersects(a.data(), a.size(), b.data(), b.size());
}
