#ifndef FORMULA_SEMANTIC_ANALYZER_H
#define FORMULA_SEMANTIC_ANALYZER_H

#include "expression_visitor.h"
#include "operators.h"
#include "type_provider.h"
#include "traits.h"


class SemanticAnalyzer : public ExpressionVisitor
{
public:
    explicit SemanticAnalyzer(TypeProvider* type_provider = nullptr) : type_provider(type_provider)
    {
    }

    Traits analyze(const Expression& expression, int32_t slave_indicator_id);

    Traits analyze(const Expression& expression);

private:
    void visit(const Constant& constant) override;

    void visit(const SumExpression& expression) override;

    void visit(const MultiplyExpression& expression) override;

    void visit(const DiffExpression& expression) override;

    void visit(const DivideExpression& expression) override;

    void visit(const ModExpression& expression) override;

    void visit(const IfExpression& expression) override;

    void visit(const IfErrorExpression& expression) override;

    void visit(const EqualExpression& expression) override;

    void visit(const NotEqualExpression& expression) override;

    void visit(const LessExpression& expression) override;

    void visit(const LessOrEqualExpression& expression) override;

    void visit(const GreaterExpression& expression) override;

    void visit(const GreaterOrEqualExpression& expression) override;

    void visit(const OrExpression& expression) override;

    void visit(const AndExpression& expression) override;

    void visit(const NegateExpression& expression) override;

    void visit(const ExtScrExpression& expression) override;

    void visit(const ArrayExpression& expression) override;

    void visit(const AndFuncExpression& expression) override;

    void visit(const OrFuncExpression& expression) override;

    void visit(const SumFuncExpression& expression) override;

    void visit(const AvgFuncExpression& expression) override;

    void visit(const CountFuncExpression& expression) override;

    void visit(const ContainsExpression& expression) override;

    void visit(const AddPropertiesExpression& expression) override;

    void visit(const IndicatorExpression& ind) override;

    void visit(const ExtIndicatorExpression& extind) override;

    void visitArithmeticOperator(const Expression& expr, const Expression& left, const Expression& right,
                                 const BinaryOperatorTable& table, const char* opName);

    void visitEqualOperator(const Expression& expr, const Expression& left, const Expression& right, const char* opName);

    void visitCompareOperator(const Expression& expr, const Expression& left, const Expression& right,
                              const ComparisonOperatorTable& table, const char* opName);

    Trait getIndicatorInfo(int indicator_id, int position) const;

private:
    Traits result_traits;
    TypeProvider* type_provider = nullptr;
    bool is_matrix_section = false;
};

#endif //FORMULA_SEMANTIC_ANALYZER_H
