//
// Created by aegorov on 14.08.17.
//

#ifndef FORMULA_EXCEPTIONS_H
#define FORMULA_EXCEPTIONS_H

#include <stdexcept>
#include "scanner.h"

class Expression;

class ParseException : public std::runtime_error
{
public:
    explicit ParseException(const char* what, int position, int length = 1) :
            std::runtime_error(what),
            position(position),
            length(length)
    {}

    explicit ParseException(const char* what, const Token& token) :
            std::runtime_error(what),
            position(token.pos),
            length(token.len)
    {}

    const int position;
    const int length;
};

class WrongArgumentCountException : public ParseException
{
public:
    explicit WrongArgumentCountException(const char* what, const Token& token) :
            ParseException(what, token)
    {}

    explicit WrongArgumentCountException(const char* what, int position, int length) :
            ParseException(what, position, length)
    {}
};

class UnknownFunctionException : public ParseException
{
public:
    explicit UnknownFunctionException(const char* what, const Token& token) :
            ParseException(what, token)
    {}

    explicit UnknownFunctionException(const char* what, int position, int length) :
            ParseException(what, position, length)
    {}
};

class EvaluateException : public std::runtime_error
{
public:
    explicit EvaluateException(const char* what) :
            std::runtime_error(what)
    {}
};

class NoDataProviderException : public EvaluateException
{
public:
    explicit NoDataProviderException(const char* what) :
            EvaluateException(what)
    {}
};

class AnalysisException : public std::runtime_error
{
public:
    explicit AnalysisException(const char* what, int position) :
            std::runtime_error(what),
            position(position)
    {}

    const int position;
};

class TypeMismatchException : public AnalysisException
{
public:
    explicit TypeMismatchException(const char* what, int position) :
            AnalysisException(what, position)
    {}
};

class WrongArgumentException : public AnalysisException
{
public:
    explicit WrongArgumentException(const char* what, int position) :
            AnalysisException(what, position)
    {}
};

class AllowedValuesMismatchException : public AnalysisException
{
public:
    explicit AllowedValuesMismatchException(const char* what, int position) :
            AnalysisException(what, position)
    {}
};

class DivideByZeroException : public EvaluateException
{
public:
    explicit DivideByZeroException(const Expression* zero_expression) :
            EvaluateException("Divide by zero"),
            expression(zero_expression)
    {}

    const Expression* expression;
};

class InvalidOperatorException : public EvaluateException
{
public:
    explicit InvalidOperatorException(Value left, Value right) :
            EvaluateException("Invalid operator"),
            left(left),
            right(right)
    {}

    const Value left;
    const Value right;
};

class InvalidOperatorValueException : public EvaluateException
{
public:
    explicit InvalidOperatorValueException(Value value) :
            EvaluateException("Invalid operator value"),
            value(value)
    {}

    const Value value;
};

#endif //FORMULA_EXCEPTIONS_H
