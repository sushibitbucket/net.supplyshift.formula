#ifndef FORMULA_TYPE_PROVIDER_H
#define FORMULA_TYPE_PROVIDER_H

#include "value.h"
#include "traits.h"

class TypeProvider
{
public:
    virtual ~TypeProvider() = default;
    virtual Trait getIndicatorInfo(int indicator_id, int position) const = 0;
};

#endif //FORMULA_TYPE_PROVIDER_H
