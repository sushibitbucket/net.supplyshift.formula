#ifndef FORMULA_EVALUATOR_H
#define FORMULA_EVALUATOR_H

#include "expression.h"
#include "expression_visitor.h"
#include "exceptions.h"
#include "value.h"
#include "operators.h"
#include "data_provider.h"
#include "collections_operations.h"

class Evaluator : public ExpressionVisitor
{
public:
    static Value evaluate(const Expression& expression, DataProvider* data_provider = nullptr);

protected:
    explicit Evaluator(const DataProvider* data_provider = nullptr);

    Value eval(const Expression& expr);

    void visit(const Constant& constant) override;

    void visit(const SumExpression& op) override;

    void visit(const MultiplyExpression& op) override;

    void visit(const DivideExpression& op) override;

    void visit(const DiffExpression& op) override;

    void visit(const ModExpression& op) override;

    void visit(const EqualExpression& op) override;

    void visit(const NotEqualExpression& op) override;

    void visit(const LessExpression& op) override;

    void visit(const LessOrEqualExpression& op) override;

    void visit(const GreaterExpression& op) override;

    void visit(const GreaterOrEqualExpression& op) override;

    void visit(const OrExpression& op) override;

    void visit(const AndExpression& op) override;

    void visit(const IfExpression& cond) override;

    void visit(const IfErrorExpression& cond) override;

    void visit(const NegateExpression& negate) override;

    void visit(const IndicatorExpression& ind) override;

    void visit(const ExtIndicatorExpression& extind) override;

    void visit(const ExtScrExpression& extscr) override;

    void visit(const ArrayExpression& expression) override;

    void visit(const AndFuncExpression& call) override;

    void visit(const OrFuncExpression& call) override;

    void visit(const SumFuncExpression& call) override;

    void visit(const CountFuncExpression& call) override;

    void visit(const AvgFuncExpression& call) override;

    void visit(const ContainsExpression& expr) override;

    void visit(const AddPropertiesExpression& expression) override;

private:
    int calcCount(const std::vector<std::unique_ptr<Expression>>& args);

    Value calcSum(const std::vector<std::unique_ptr<Expression>>& args);

private:
    Value result;
    const DataProvider* data_provider;
};

#endif //FORMULA_EVALUATOR_H
