#include <scanner.h>

Token::Type scan(const char** cursor, const char** lexeme)
{
    const char *marker;
    if (lexeme)
        *lexeme = *cursor;

/*!re2c
    re2c:define:YYCTYPE  = "unsigned char";
    re2c:define:YYCURSOR = *cursor;
    re2c:define:YYMARKER = marker;
    re2c:yyfill:enable   = 0;
    re2c:yych:conversion = 1;
    re2c:indent:top      = 1;

    LEFT_P = '(' ;
    RIGHT_P = ')' ;
    LEFT_CB = '{' ;
    RIGHT_CB = '}' ;
    LESS = '<' ;
    GREATER = '>' ;
    LESS_OR_EQUAL = '<=' ;
    GREATER_OR_EQUAL = '>=' ;
    EQUAL = '=' ;
    NOT_EQUAL = '!=' ;
    PLUS = '+' ;
    MINUS = '-' ;
    DIVIDE = '/' ;
    MOD = '%' ;
    TIMES = '*' ;
    AND = '&&' ;
    BITWISE_AND = '&' ;
    OR = '||' ;
    NOT = '!' ;
    TRUE = 'true' ;
    FALSE = 'false' ;
    INTEGER = [0-9]+ ;
    FLOAT = [0-9]+ ('.' [0-9]*)? ;
    STRING = ('"' ( [^\x00"\\] | '\\\\' | '\\"' )* '"') | ('\'' ( [^\x00'\\] | '\\\\' | '\\\'' )* '\'') ;
    SPACE = [ \t\r\n]* ;
    END = "\x00" ;
    NA = 'NA' ;
    NOA = 'NOA' ;
    BLANK = 'BLANK' ;
    IDENTIFIER = [a-zA-Z_][a-zA-Z0-9_]* ;
    COMMA = ',' ;
    HIDDEN = 'HIDDEN' ;
    USERINPUT = 'USERINPUT' ;

    LEFT_P           { return Token::Type::LEFT_P; }
    RIGHT_P          { return Token::Type::RIGHT_P; }
    LEFT_CB          { return Token::Type::LEFT_CB; }
    RIGHT_CB         { return Token::Type::RIGHT_CB; }
    LESS             { return Token::Type::LESS; }
    GREATER          { return Token::Type::GREATER; }
    LESS_OR_EQUAL    { return Token::Type::LESS_OR_EQUAL; }
    GREATER_OR_EQUAL { return Token::Type::GREATER_OR_EQUAL; }
    EQUAL            { return Token::Type::EQUAL; }
    NOT_EQUAL        { return Token::Type::NOT_EQUAL; }
    PLUS             { return Token::Type::PLUS; }
    MINUS            { return Token::Type::MINUS; }
    DIVIDE           { return Token::Type::DIVIDE; }
    MOD              { return Token::Type::MOD; }
    TIMES            { return Token::Type::TIMES; }
    AND              { return Token::Type::AND; }
    BITWISE_AND      { return Token::Type::BITWISE_AND; }
    OR               { return Token::Type::OR; }
    TRUE             { return Token::Type::TRUE; }
    FALSE            { return Token::Type::FALSE; }
    INTEGER          { return Token::Type::INTEGER; }
    FLOAT            { return Token::Type::FLOAT; }
    STRING           { return Token::Type::STRING; }
    SPACE            { return Token::Type::SPACE; }
    NA               { return Token::Type::NA; }
    NOA              { return Token::Type::NOA; }
    BLANK            { return Token::Type::BLANK; }
    HIDDEN           { return Token::Type::HIDDEN; }
    USERINPUT        { return Token::Type::USERINPUT; }
    IDENTIFIER       { return Token::Type::IDENTIFIER; }
    COMMA            { return Token::Type::COMMA; }
    END              { return Token::Type::END; }
*/
}

