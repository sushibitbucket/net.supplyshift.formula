#include "date.h"

#include <stdio.h>

bool operator==(const Date& date1, const Date& date2)
{
    return date1.day == date2.day &&
           date1.month == date2.month &&
           date1.year == date2.year;
}

bool isLeapYear(int year)
{
    if (year % 4 != 0)
        return false;
    if (year % 100 != 0)
        return true;
    if (year % 400 != 0)
        return false;
    return true;
}

int parseDate(const char* str, Date& date)
{
    static int days_in_month[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    date = {0, 0, 0};
    int month = 0;
    int day = 0;
    int year = 0;
    sscanf(str, "%d/%d/%d", &month, &day, &year);
    if (month < 1 || month > 12)
        return 1;
    int day_count = days_in_month[month - 1];
    if (month == 2 && isLeapYear(year))
        day_count = 29;
    if (day < 1 || day > day_count)
        return 1;
    date.year = static_cast<int16_t>(year);
    date.month = static_cast<int8_t>(month);
    date.day = static_cast<int8_t>(day);
    return 0;
}
