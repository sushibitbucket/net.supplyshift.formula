//
// Created by aegorov on 17.08.17.
//

#include "expression.h"
#include "expression_visitor.h"

void Constant::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void SumExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void MultiplyExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void DiffExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void DivideExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void ModExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void EqualExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void NotEqualExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void LessExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void LessOrEqualExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void GreaterExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void GreaterOrEqualExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void OrExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void AndExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void IfExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void NegateExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void IndicatorExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void ExtIndicatorExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void ExtScrExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void ArrayExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void AndFuncExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void OrFuncExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void SumFuncExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void AvgFuncExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void CountFuncExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void IfErrorExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void ContainsExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}

void AddPropertiesExpression::visit(ExpressionVisitor& visitor) const
{
    visitor.visit(*this);
}
