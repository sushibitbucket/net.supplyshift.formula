#ifndef FORMULA_DATE_H
#define FORMULA_DATE_H

#include <cstdint>

struct Date
{
    int16_t year;
    int8_t month;
    int8_t day;
};

bool operator==(const Date& date1, const Date& date2);

int parseDate(const char* str, Date& date);

#endif //FORMULA_DATE_H
