//
// Created by aegorov on 28.08.17.
//

#include "operators.h"
#include <cmath>

Value::Type NONE = Value::Type::NONE;
Value::Type INTEGER = Value::Type::INTEGER;
Value::Type FLOAT = Value::Type::FLOAT;
Value::Type BOOLEAN = Value::Type::BOOLEAN;
Value::Type STRING = Value::Type::STRING;
Value::Type ARRAY = Value::Type::ARRAY;
Value::Type NOT_APPLICABLE = Value::Type::NOT_APPLICABLE;
Value::Type NONE_OF_ABOVE = Value::Type::NONE_OF_ABOVE;
Value::Type BLANK = Value::Type::BLANK;
Value::Type DATE = Value::Type::DATE;
Value::Type ANY = Value::Type::TYPE_COUNT;

static Value returnNotApplicable(const Value& a, const Value& b)
{
    return Value::NotApplicable;
}

BinaryOperatorTable Ops::sum{
        {INTEGER, INTEGER, INTEGER, [](const Value& a, const Value& b) {
            return Value::createInt(a.getInt() + b.getInt());
        }},
        {FLOAT, FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return Value(a.getFloat() + b.getFloat());
        }},
        {FLOAT, INTEGER, FLOAT, [](const Value& a, const Value& b) {
            return Value(a.getFloat() + b.getInt());
        }},
        {INTEGER, FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return Value(a.getInt() + b.getFloat());
        }},
        {NOT_APPLICABLE, ANY, NOT_APPLICABLE, &returnNotApplicable},
        {ANY, NOT_APPLICABLE, NOT_APPLICABLE, &returnNotApplicable},
};

static Value divide(double a, double b, const Expression* denominator)
{
    if (b == 0)
        throw DivideByZeroException(denominator);
    return Value(a / b);
}

BinaryOperatorTable Ops::div{
        {INTEGER, INTEGER, FLOAT, [](const Value& a, const Value& b) {
            return divide(a.getInt(), b.getInt(), b.getSource());
        }},
        {FLOAT, FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return divide(a.getFloat(), b.getFloat(), b.getSource());
        }},
        {FLOAT, INTEGER, FLOAT, [](const Value& a, const Value& b) {
            return divide(a.getFloat(), b.getInt(), b.getSource());
        }},
        {INTEGER, FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return divide(a.getInt(), b.getFloat(), b.getSource());
        }},
        {NOT_APPLICABLE, ANY, NOT_APPLICABLE, &returnNotApplicable},
        {ANY, NOT_APPLICABLE, NOT_APPLICABLE, &returnNotApplicable},
};

BinaryOperatorTable Ops::diff{
        {INTEGER, INTEGER, INTEGER, [](const Value& a, const Value& b) {
            return Value::createInt(a.getInt() - b.getInt());
        }},
        {FLOAT, FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return Value(a.getFloat() - b.getFloat());
        }},
        {FLOAT, INTEGER, FLOAT, [](const Value& a, const Value& b) {
            return Value(a.getFloat() - b.getInt());
        }},
        {INTEGER, FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return Value(a.getInt() - b.getFloat());
        }},
        {NOT_APPLICABLE, ANY, NOT_APPLICABLE, &returnNotApplicable},
        {ANY, NOT_APPLICABLE, NOT_APPLICABLE, &returnNotApplicable},
};

BinaryOperatorTable Ops::mult{
        {INTEGER, INTEGER, INTEGER, [](const Value& a, const Value& b) {
            return Value::createInt(a.getInt() * b.getInt());
        }},
        {FLOAT, FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return Value(a.getFloat() * b.getFloat());
        }},
        {FLOAT, INTEGER, FLOAT, [](const Value& a, const Value& b) {
            return Value(a.getFloat() * b.getInt());
        }},
        {INTEGER, FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return Value(a.getInt() * b.getFloat());
        }},
        {NOT_APPLICABLE, ANY, NOT_APPLICABLE, &returnNotApplicable},
        {ANY, NOT_APPLICABLE, NOT_APPLICABLE, &returnNotApplicable},
};

static Value modulo(int64_t a, int64_t b, const Expression* denominator)
{
    if (b == 0)
        throw DivideByZeroException(denominator);
    return Value::createInt(a % b);
}

static Value modulo(double a, double b, const Expression* denominator)
{
    if (b == 0)
        throw DivideByZeroException(denominator);
    return Value(std::fmod(a, b));
}

BinaryOperatorTable Ops::mod{
        {INTEGER, INTEGER, INTEGER, [](const Value& a, const Value& b) {
            return modulo(a.getInt(), b.getInt(), b.getSource());
        }},
        {FLOAT, FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return modulo(a.getFloat(), b.getFloat(), b.getSource());
        }},
        {FLOAT, INTEGER, FLOAT, [](const Value& a, const Value& b) {
            return modulo(a.getFloat(), (double)b.getInt(), b.getSource());
        }},
        {INTEGER, FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return modulo((double)a.getInt(), b.getFloat(), b.getSource());
        }},
        {NOT_APPLICABLE, ANY, NOT_APPLICABLE, &returnNotApplicable},
        {ANY, NOT_APPLICABLE, NOT_APPLICABLE, &returnNotApplicable},
};

BinaryOperatorTable Ops::logical_and{
        {BOOLEAN, BOOLEAN, BOOLEAN, [](const Value& a, const Value& b) {
            return Value(a.getBool() && b.getBool());
        }},
};

BinaryOperatorTable Ops::logical_or{
        {BOOLEAN, BOOLEAN, BOOLEAN, [](const Value& a, const Value& b) {
            return Value(a.getBool() || b.getBool());
        }},
};

static bool fuzzyCompare(double a, double b)
{
    return std::abs(a - b) < 1e-9;
}

ComparisonOperatorTable Ops::equal{
        {ANY, ANY, [](const Value& a, const Value& b) {
            return a.type() == b.type();
        }},
        {INTEGER, INTEGER, [](const Value& a, const Value& b) {
            return a.getInt() == b.getInt();
        }},
        {FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return fuzzyCompare(a.getFloat(), b.getFloat());
        }},
        {FLOAT, INTEGER, [](const Value& a, const Value& b) {
            return fuzzyCompare(a.getFloat(), (double)b.getInt());
        }},
        {INTEGER, FLOAT, [](const Value& a, const Value& b) {
            return fuzzyCompare((double)a.getInt(), b.getFloat());
        }},
        {BOOLEAN, BOOLEAN, [](const Value& a, const Value& b) {
            return a.getBool() == b.getBool();
        }},
        {STRING, STRING, [](const Value& a, const Value& b) {
            return strcmp(a.getString(), b.getString()) == 0;
        }},
        {DATE, DATE, [](const Value& a, const Value& b) {
            return a.getDate() == b.getDate();
        }},
        {ARRAY, ARRAY, [](const Value& a, const Value& b) {
            int32_t array1_size = a.getElementCount();
            int32_t array2_size = b.getElementCount();
            if (array1_size != array2_size)
                return false;
            Value* array1 = a.getElements();
            Value* array2 = b.getElements();
            for (int i = 0; i < array1_size; ++i)
            {
                if (array1[i] != array2[i])
                    return false;
            }
            return true;
        }},
};

ComparisonOperatorTable Ops::less{
        {INTEGER, INTEGER, [](const Value& a, const Value& b) {
            return a.getInt() < b.getInt();
        }},
        {FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return a.getFloat() < b.getFloat();
        }},
        {FLOAT, INTEGER, [](const Value& a, const Value& b) {
            return a.getFloat() < b.getInt();
        }},
        {INTEGER, FLOAT, [](const Value& a, const Value& b) {
            return a.getInt() < b.getFloat();
        }},
};

ComparisonOperatorTable Ops::less_or_equal{
        {INTEGER, INTEGER, [](const Value& a, const Value& b) {
            return a.getInt() <= b.getInt();
        }},
        {FLOAT, FLOAT, [](const Value& a, const Value& b) {
            return a.getFloat() <= b.getFloat();
        }},
        {FLOAT, INTEGER, [](const Value& a, const Value& b) {
            return a.getFloat() <= b.getInt();
        }},
        {INTEGER, FLOAT, [](const Value& a, const Value& b) {
            return a.getInt() <= b.getFloat();
        }},
};

Value operator-(const Value& value)
{
    Value::Type type = value.type();
    switch (type)
    {
        case Value::Type::INTEGER: return Value::createInt(-value.getInt());
        case Value::Type::FLOAT: return Value(-value.getFloat());
        default:
            throw InvalidOperatorValueException(value);
    }
}
