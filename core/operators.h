//
// Created by aegorov on 28.08.17.
//

#ifndef FORMULA_OPERATORS_H
#define FORMULA_OPERATORS_H

#include <iostream>
#include <string.h>
#include "value.h"
#include "exceptions.h"

struct BinaryOperator
{
    Value::Type left_type;
    Value::Type right_type;
    Value::Type result;
    Value (*func)(const Value&, const Value&);
};

class BinaryOperatorTable
{
public:
    static const int ANY = (int)Value::Type::TYPE_COUNT;
    static const int TYPE_COUNT = (int)Value::Type::TYPE_COUNT;

    BinaryOperatorTable()
    {
        size_t size = sizeof(operators[0][0]) * TYPE_COUNT * TYPE_COUNT;
        memset((void*)operators, 0, size);
    }

    explicit BinaryOperatorTable(std::initializer_list<BinaryOperator> operators) :
            BinaryOperatorTable()
    {
        for (const auto& op : operators)
            add(op.left_type, op.right_type, op);
    }

    BinaryOperator get(Value::Type left_type, Value::Type right_type) const
    {
        return operators[left_type][right_type];
    }

    Value call(const Value& a, const Value& b) const
    {
        BinaryOperator op = get(a.type(), b.type());
        if (op.func == nullptr)
            throw InvalidOperatorException(a, b);
        return op.func(a, b);
    }

    void add(Value::Type left, Value::Type right, const BinaryOperator& op)
    {
        if (left == ANY)
        {
            for (int type = 0; type < Value::TYPE_COUNT; ++type)
                add((Value::Type)type, right, op);
        }
        else if (right == ANY)
        {
            for (int type = 0; type < Value::TYPE_COUNT; ++type)
                add(left, (Value::Type)type, op);
        }
        else
        {
            operators[left][right] = op;
        }
    }

private:
    BinaryOperator operators[TYPE_COUNT][TYPE_COUNT];
};

struct ComparisonOperator
{
    typedef bool (*Func)(const Value&, const Value&);

    Value::Type left_type;
    Value::Type right_type;
    Func func;
};

class ComparisonOperatorTable
{
public:
    static const int ANY = (int)Value::Type::TYPE_COUNT;
    static const int TYPE_COUNT = (int)Value::Type::TYPE_COUNT;

    ComparisonOperatorTable()
    {
        size_t size = sizeof(operators[0][0]) * TYPE_COUNT * TYPE_COUNT;
        memset((void*)operators, 0, size);
    }

    explicit ComparisonOperatorTable(std::initializer_list<ComparisonOperator> operators) :
            ComparisonOperatorTable()
    {
        for (const auto& op : operators)
            add(op.left_type, op.right_type, op.func);
    }

    ComparisonOperator::Func get(Value::Type left_type, Value::Type right_type) const
    {
        return operators[left_type][right_type];
    }

    bool call(const Value& a, const Value& b) const
    {
        ComparisonOperator::Func func = get(a.type(), b.type());
        if (func == nullptr)
            throw InvalidOperatorException(a, b);
        return func(a, b);
    }

    void add(Value::Type left, Value::Type right, ComparisonOperator::Func func)
    {
        if (left == ANY)
        {
            for (int type = 0; type < Value::TYPE_COUNT; ++type)
                add((Value::Type)type, right, func);
        }
        else if (right == ANY)
        {
            for (int type = 0; type < Value::TYPE_COUNT; ++type)
                add(left, (Value::Type)type, func);
        }
        else
        {
            operators[left][right] = func;
        }
    }

private:
    ComparisonOperator::Func operators[TYPE_COUNT][TYPE_COUNT];
};

struct Ops
{
    static BinaryOperatorTable sum;
    static BinaryOperatorTable div;
    static BinaryOperatorTable diff;
    static BinaryOperatorTable mult;
    static BinaryOperatorTable mod;
    static BinaryOperatorTable logical_and;
    static BinaryOperatorTable logical_or;
    static ComparisonOperatorTable equal;
    static ComparisonOperatorTable less;
    static ComparisonOperatorTable less_or_equal;
};

inline Value operator+(const Value& a, const Value& b) { return Ops::sum.call(a, b); }
inline Value operator-(const Value& a, const Value& b) { return Ops::diff.call(a, b); }
inline Value operator/(const Value& a, const Value& b) { return Ops::div.call(a, b); }
inline Value operator*(const Value& a, const Value& b) { return Ops::mult.call(a, b); }
inline Value operator%(const Value& a, const Value& b) { return Ops::mod.call(a, b); }
inline Value operator&&(const Value& a, const Value& b) { return Ops::logical_and.call(a, b); }
inline Value operator||(const Value& a, const Value& b) { return Ops::logical_or.call(a, b); }
inline bool operator==(const Value& a, const Value& b) { return Ops::equal.call(a, b); }
inline bool operator!=(const Value& a, const Value& b) { return !(a == b); }
inline bool operator<(const Value& a, const Value& b) { return Ops::less.call(a, b); }
inline bool operator<=(const Value& a, const Value& b) { return Ops::less_or_equal.call(a, b); }
inline bool operator>(const Value& a, const Value& b) { return !(a <= b); }
inline bool operator>=(const Value& a, const Value& b) { return !(a < b); }
Value operator-(const Value& a);

#endif //FORMULA_OPERATORS_H
