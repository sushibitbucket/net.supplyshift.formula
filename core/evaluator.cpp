#include "evaluator.h"

Value Evaluator::evaluate(const Expression& expression, DataProvider* data_provider)
{
    Evaluator evaluator(data_provider);
    return evaluator.eval(expression);
}

Evaluator::Evaluator(const DataProvider* data_provider) :
        data_provider(data_provider)
{
}

Value Evaluator::eval(const Expression& expr)
{
    expr.visit(*this);
    result.setSource(&expr);
    return result;
}

void Evaluator::visit(const Constant& constant)
{
    result = constant.value;
}

void Evaluator::visit(const SumExpression& op)
{
    result = eval(*op.left) + eval(*op.right);
}

void Evaluator::visit(const MultiplyExpression& op)
{
    result = eval(*op.left) * eval(*op.right);
}

void Evaluator::visit(const DivideExpression& op)
{
    result = eval(*op.left) / eval(*op.right);
}

void Evaluator::visit(const DiffExpression& op)
{
    result = eval(*op.left) - eval(*op.right);
}

void Evaluator::visit(const ModExpression& op)
{
    result = eval(*op.left) % eval(*op.right);
}

void Evaluator::visit(const EqualExpression& op)
{
    result = Value(eval(*op.left) == eval(*op.right));
}

void Evaluator::visit(const NotEqualExpression& op)
{
    result = Value(eval(*op.left) != eval(*op.right));
}

void Evaluator::visit(const LessExpression& op)
{
    result = Value(eval(*op.left) < eval(*op.right));
}

void Evaluator::visit(const LessOrEqualExpression& op)
{
    result = Value(eval(*op.left) <= eval(*op.right));
}

void Evaluator::visit(const GreaterExpression& op)
{
    result = Value(eval(*op.left) > eval(*op.right));
}

void Evaluator::visit(const GreaterOrEqualExpression& op)
{
    result = Value(eval(*op.left) >= eval(*op.right));
}

void Evaluator::visit(const OrExpression& op)
{
    result = eval(*op.left) || eval(*op.right);
}

void Evaluator::visit(const AndExpression& op)
{
    result = eval(*op.left) && eval(*op.right);
}

void Evaluator::visit(const IfExpression& cond)
{
    Value condition = eval(*cond.condition);

    if (condition.type() != Value::Type::BOOLEAN)
        throw EvaluateException("Condition result is not boolean value");

    if (condition.getBool())
    {
        eval(*cond.thenBody);
    }
    else
    {
        eval(*cond.elseBody);
    }
}

void Evaluator::visit(const IfErrorExpression& cond)
{
    try
    {
        eval(*cond.expr);
    }
    catch(const EvaluateException&)
    {
        eval(*cond.thenBody);
    }
}

void Evaluator::visit(const NegateExpression& negate)
{
    result = -eval(*negate.value);
}

void Evaluator::visit(const IndicatorExpression& ind)
{
    if (data_provider == nullptr)
        throw NoDataProviderException("Can't evaluate IND function without data provider");

    result = data_provider->getIndicatorValue(ind.indicator_id, ind.position);
}

void Evaluator::visit(const ExtIndicatorExpression& extind)
{
    if (data_provider == nullptr)
        throw NoDataProviderException("Can't evaluate EXTIND function without data provider");

    result = data_provider->getExtIndicatorValue(extind.indicator_id, extind.from, extind.to, extind.position);
}

void Evaluator::visit(const ExtScrExpression& extscr)
{
    if (data_provider == nullptr)
        throw NoDataProviderException("Can't evaluate EXTSCR function without data provider");

    result = data_provider->getExtScorecardValue(extscr.scorecard_id, extscr.position);
}

void Evaluator::visit(const ArrayExpression& expression)
{
    int size = expression.values.size();
    Value* values = new Value[size];
    try
    {
        int i = 0;
        for (const auto& subExpr : expression.values)
        {
            values[i] = eval(*subExpr);
            ++i;
        }
    }
    catch(const std::exception&)
    {
        delete[] values;
        throw;
    }
    result = Value(values, size, Value::ARRAY);
}

void Evaluator::visit(const AndFuncExpression& call)
{
    Value and_result = eval(*call.args[0]);

    for (int i = 1; i < call.args.size(); ++i)
        and_result = and_result && eval(*call.args[i]);

    result = and_result;
}

void Evaluator::visit(const OrFuncExpression& call)
{
    Value or_result = eval(*call.args[0]);

    for (int i = 1; !or_result.getBool() && i < call.args.size(); ++i)
        or_result = or_result || eval(*call.args[i]);

    result = or_result;
}

void Evaluator::visit(const SumFuncExpression& call)
{
    result = calcSum(call.args);
}

void Evaluator::visit(const CountFuncExpression& call)
{
    int64_t count = calcCount(call.args);
    result = Value::createInt(count);
}

void Evaluator::visit(const AvgFuncExpression& call)
{
    int count = calcCount(call.args);
    if (count == 0)
    {
        result = Value::createInt(0);
        return;
    }

    Value sum = calcSum(call.args);
    sum = sum / Value(double(count));

    result = sum;
}

void Evaluator::visit(const ContainsExpression& expr)
{
    Value subset = eval(*expr.subset);
    Value superset = eval(*expr.superset);

    if (subset.type() != Value::Type::ARRAY && subset.type() != Value::MULTI_SELECT)
        subset = Value({subset}, Value::ARRAY);
    if (superset.type() != Value::Type::ARRAY && superset.type() != Value::MULTI_SELECT)
        superset = Value({superset}, Value::ARRAY);

    int32_t subset_size = subset.getElementCount();
    int32_t superset_size = superset.getElementCount();
    Value* subset_array = subset.getElements();
    Value* superset_array = superset.getElements();

    result = Value(contains(superset_array, superset_size, subset_array, subset_size));
}

void Evaluator::visit(const AddPropertiesExpression& expression)
{
    auto properties = eval(*expression.right).getProperties();
    eval(*expression.left);
    result.addProperties(properties);
}

int Evaluator::calcCount(const std::vector<std::unique_ptr<Expression>>& args)
{
    int count = 0;
    for (const auto& arg : args)
    {
        auto argValue = eval(*arg);
        if (argValue.type() == Value::Type::ARRAY)
        {
            Value* array = argValue.getElements();
            int size = argValue.getElementCount();
            for (int i = 0; i < size; ++i)
            {
                const auto& item = array[i];
                if (item.type() == Value::Type::NOT_APPLICABLE)
                    continue;
                count++;
            }
        }
        else
        {
            count++;
        }
    }
    return count;
}

Value Evaluator::calcSum(const std::vector<std::unique_ptr<Expression>>& args)
{
    Value sum = Value::createInt(0);

    for (const auto& arg : args)
    {
        auto argValue = eval(*arg);
        if (argValue.type() == Value::Type::ARRAY)
        {
            Value* array = argValue.getElements();
            int size = argValue.getElementCount();
            for (int i = 0; i < size; ++i)
            {
                const auto& item = array[i];
                if (item.type() == Value::Type::NOT_APPLICABLE || item.type() == Value::Type::BLANK)
                    continue;
                sum = sum + item;
            }
        }
        else
        {
            sum = sum + argValue;
        }
    }
    return sum;
}
