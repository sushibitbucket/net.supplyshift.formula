//
// Created by aegorov on 14.08.17.
//

#ifndef FORMULA_VALUE_H
#define FORMULA_VALUE_H

#include <memory.h>
#include <string>
#include <utility>
#include <vector>
#include <stdexcept>

#include "date.h"
#include <iostream>

class Expression;

class Value
{
public:
    enum Type : int8_t
    {
        NONE = 0,
        INTEGER = 1,
        FLOAT = 2,
        BOOLEAN = 3,
        STRING = 4,
        ARRAY = 5,
        NOT_APPLICABLE = 6,
        NONE_OF_ABOVE = 7,
        BLANK = 8,
        DATE = 9,
        MULTI_SELECT = 10,
        LOCATION = 11,
        GPS = 12,
        TYPE_COUNT
    };

    enum Property : uint8_t
    {
        HIDDEN = 1,
        USERINPUT = 2
    };

    static const char* typeName(Type type);
    static bool isValidType(Type type);

    static Value NotApplicable;
    static Value NoneOfAbove;
    static Value Blank;

    Value() = default;
    Value(const Value& other);
    Value& operator=(const Value& other);
    ~Value();
    explicit Value(double value) : float_value(value), m_type(Type::FLOAT) {}
    explicit Value(bool value) : bool_value(value), m_type(Type::BOOLEAN) {}
    explicit Value(const char* value) : str_value(strdup(value)), m_type(Type::STRING) {}
    explicit Value(const char* value, size_t length) : m_type(Type::STRING) 
	{
		size_t len = strlen(value);
		if (length < len)
			len = length;
		str_value = (char*)malloc(len+1);
		if (str_value == 0)
			throw std::bad_alloc();
		memcpy(str_value, value, len);
		str_value[len] = '\0';
	}
    explicit Value(Type type) : m_type(type) {}
    explicit Value(const Date& date) : m_type(Type::DATE), date(date) {}
    explicit Value(Value* values, int32_t size, Value::Type type) : m_type(type)
    {
        if (type != Type::ARRAY && type != MULTI_SELECT)
            throw std::invalid_argument("Invalid Value type specified");
        elements.values = values;
        elements.size = size;
    }
    explicit Value(std::vector<Value> values, Value::Type type) : m_type(type)
    {
        if (type != Type::ARRAY && type != MULTI_SELECT)
            throw std::invalid_argument("Invalid Value type specified");
        elements.values = new Value[values.size()];
        elements.size = values.size();
        std::copy(begin(values), end(values), elements.values);
    }

    static Value createInt(int64_t integer)
    {
        Value value;
        value.m_type = INTEGER;
        value.int_value = integer;
        return value;
    }

    static Value createLocation(int64_t country, int64_t province)
    {
        Value value;
        value.m_type = LOCATION;
        value.location.country = country;
        value.location.province = province;
        return value;
    }

    static Value createGps(double latitude, double longitude)
    {
        Value value;
        value.m_type = GPS;
        value.gps.latitude = latitude;
        value.gps.longitude = longitude;
        return value;
    }

    Type type() const { return m_type; }
    const char* typeName() const { return typeName(m_type); }
    void addProperties(uint8_t properties) { this->properties |= properties; }

    int64_t getInt() const;
    double getFloat() const;
    bool getBool() const;
    const char* getString() const;
    Value* getElements() const;
    int32_t getElementCount() const;
    const Date& getDate() const;
    int64_t getCountry() const;
    int64_t getProvince() const;
    double getLatitude() const;
    double getLongitude() const;
    const uint8_t getProperties() const { return properties; }

    void setSource(const Expression* source)
    {
        this->source = source;
    }

    const Expression* getSource() const
    {
        return source;
    }

private:
    Type m_type = Type::NONE;
    uint8_t properties = 0;
    const Expression* source = nullptr;
    union
    {
        int64_t int_value;
        double float_value;
        bool bool_value;
        Date date;
        char* str_value;
        struct {
            Value* values;
            int32_t size;
        } elements;
        struct {
            int64_t country;
            int64_t province;
        } location;
        struct {
            double latitude;
            double longitude;
        } gps;
    };
};

std::ostream& operator<<(std::ostream& os, const Value& v);
std::ostream& operator<<(std::ostream& os, const std::vector<Value>& values);

#endif //FORMULA_VALUE_H
