#include "parser.h"

bool isInfixOperator(Token::Type token)
{
    return token == Token::Type::PLUS ||
           token == Token::Type::DIVIDE ||
           token == Token::Type::TIMES ||
           token == Token::Type::MINUS ||
           token == Token::Type::MOD ||
           token == Token::Type::EQUAL ||
           token == Token::Type::NOT_EQUAL ||
           token == Token::Type::OR ||
           token == Token::Type::AND ||
           token == Token::Type::BITWISE_AND ||
           token == Token::Type::LESS ||
           token == Token::Type::GREATER ||
           token == Token::Type::LESS_OR_EQUAL ||
           token == Token::Type::GREATER_OR_EQUAL ||
           token == Token::Type::COMMA;
}

bool isUnaryOperator(Token::Type token)
{
    return token == Token::Type::PLUS || token == Token::Type::MINUS;
}

int operatorPriority(const Operator& op)
{
    switch (op.type)
    {
        case Operator::BINARY:
            switch (op.token.type)
            {
                case Token::Type::TIMES:
                case Token::Type::DIVIDE:
                case Token::Type::MOD:
                case Token::Type::BITWISE_AND:
                    return 10;
                case Token::Type::PLUS:
                case Token::Type::MINUS:
                    return 6;
                case Token::Type::OR:
                case Token::Type::AND:
                    return 5;
                case Token::Type::LESS:
                case Token::Type::LESS_OR_EQUAL:
                case Token::Type::GREATER:
                case Token::Type::GREATER_OR_EQUAL:
                    return 4;
                case Token::Type::EQUAL:
                case Token::Type::NOT_EQUAL:
                    return 3;
                case Token::Type::COMMA:
                    return 2;
                default:
                    throw ParseException("Binary operator precedence is not defined", op.token);
            }
        case Operator::UNARY:
            return 8;
        case Operator::LEFT_P:
            return 1;
        case Operator::LEFT_CB:
            return 1;
        default:
            throw ParseException("Operator precedence is not defined", op.token);
    }
}

std::unique_ptr <Expression> Parser::parse(const char* formula)
{
    this->formula = formula;
    waitingValue = true;
    output.clear();
    operator_stack.clear();
    const char* cursor = formula;

    while (true)
    {
        Token token = scan(formula, &cursor);
        if (token.type == Token::Type::END)
            break;

        if (token.len == 0)
            throw ParseException("Syntax error", token);

        parseToken(token);
    }

    flush();

    if (output.size() != 1)
        throw ParseException("Syntax error, operators missing", Token());

    return std::move(output.back());
}

void Parser::flush()
{
    while (!operator_stack.empty())
        pushOperator();
}

void Parser::parseToken(const Token& token)
{
    if (token.type == Token::Type::SPACE)
        return;

    if (waitingValue)
        parseForValue(token);
    else
        parseForOperator(token);
}

void Parser::parseForValue(const Token& token)
{
    if (token.type == Token::Type::INTEGER)
    {
        Value value = Value::createInt((int64_t)strtol(token.lexeme, nullptr, 10));
        output.emplace_back(new Constant(pos(token), value));
        waitingValue = false;
    }
    else if (token.type == Token::Type::FLOAT)
    {
        Value value(strtod(token.lexeme, nullptr));
        output.emplace_back(new Constant(pos(token), value));
        waitingValue = false;
    }
    else if (token.type == Token::Type::STRING)
    {
        Value value(token.lexeme + 1, token.len - 2);
        output.emplace_back(new Constant(pos(token), value));
        waitingValue = false;
    }
    else if (token.type == Token::Type::TRUE)
    {
        output.emplace_back(new Constant(pos(token), Value(true)));
        waitingValue = false;
    }
    else if (token.type == Token::Type::FALSE)
    {
        output.emplace_back(new Constant(pos(token), Value(false)));
        waitingValue = false;
    }
    else if (token.type == Token::Type::NA)
    {
        output.emplace_back(new Constant(pos(token), Value::NotApplicable));
        waitingValue = false;
    }
    else if (token.type == Token::Type::NOA)
    {
        output.emplace_back(new Constant(pos(token), Value::NoneOfAbove));
        waitingValue = false;
    }
    else if (token.type == Token::Type::BLANK)
    {
        output.emplace_back(new Constant(pos(token), Value::Blank));
        waitingValue = false;
    }
    else if (token.type == Token::Type::HIDDEN)
    {
        Value value = Value::Blank;
        value.addProperties(Value::HIDDEN);
        output.emplace_back(new Constant(pos(token), value));
        waitingValue = false;
    }
    else if (token.type == Token::Type::USERINPUT)
    {
        Value value = Value::Blank;
        value.addProperties(Value::USERINPUT);
        output.emplace_back(new Constant(pos(token), value));
        waitingValue = false;
    }
    else if (isUnaryOperator(token.type))
    {
        operator_stack.emplace_back(Operator::UNARY, token);
    }
    else if (token.type == Token::Type::IDENTIFIER)
    {
        operator_stack.emplace_back(Operator::VARIABLE, token);
    }
    else if (token.type == Token::Type::LEFT_P)
    {
        if (!operator_stack.empty() && operator_stack.back().type == Operator::VARIABLE)
        {
            operator_stack.back().type = Operator::FUNCTION;
            output.push_back(nullptr);
        }
        operator_stack.emplace_back(Operator::LEFT_P, token);
    }
    else if (token.type == Token::Type::LEFT_CB)
    {
        output.push_back(nullptr);
        operator_stack.emplace_back(Operator::LEFT_CB, token);
    }
    else if (token.type == Token::Type::RIGHT_P)
    {
        pushRightParenthesis(token);
    }
    else if (token.type == Token::Type::RIGHT_CB)
    {
        pushRightCurlyBracket(token);
        waitingValue = false;
    }
    else
    {
        throw ParseException("Not implemented value token type", token);
    }
}

void Parser::parseForOperator(const Token& token)
{
    if (isInfixOperator(token.type))
    {
        Operator op(Operator::BINARY, token);
        int priority = operatorPriority(op);
        while (!operator_stack.empty() && priority <= operatorPriority(operator_stack.back()))
            pushOperator();
        operator_stack.emplace_back(op);
        waitingValue = true;
    }
    else if (token.type == Token::Type::RIGHT_P)
    {
        pushRightParenthesis(token);
    }
    else if (token.type == Token::Type::RIGHT_CB)
    {
        pushRightCurlyBracket(token);
    }
    else
    {
        throw ParseException("Not implemented operator token type", token);
    }
}

void Parser::pushRightParenthesis(const Token& token)
{
    while (!operator_stack.empty() && operator_stack.back().type != Operator::LEFT_P)
        pushOperator();
    if (operator_stack.empty() || operator_stack.back().type != Operator::LEFT_P)
        throw ParseException("Parentheses mismatch", token);
    // pop left bracket
    operator_stack.pop_back();

    if (!operator_stack.empty() && operator_stack.back().type == Operator::FUNCTION)
    {
        pushFunction();
    }
}

void Parser::pushRightCurlyBracket(const Token& token)
{
    while (!operator_stack.empty() && operator_stack.back().type != Operator::LEFT_CB)
        pushOperator();
    if (operator_stack.empty() || operator_stack.back().type != Operator::LEFT_CB)
        throw ParseException("Parentheses mismatch", token);

    const Operator& left_bracket = operator_stack.back();
    operator_stack.pop_back();

    pushArray(left_bracket);
}

void Parser::pushFunction()
{
    Operator op = operator_stack.back();
    operator_stack.pop_back();

    std::string function(op.token.lexeme, op.token.len);
    std::vector<std::unique_ptr<Expression>> args;

    while (!output.empty() && output.back() != nullptr)
    {
        args.push_back(std::move(output.back()));
        output.pop_back();
    }

    // pop nullptr marker
    if (output.empty() || output.back() != nullptr)
        throw ParseException("Internal parser error, function start not found", op.token);
    output.pop_back();

    std::reverse(begin(args), end(args));

    if (function == "IF")
    {
        if (args.size() != 3)
            throw WrongArgumentCountException("IF function takes exactly 3 arguments", op.token);

        output.emplace_back(new IfExpression(pos(op.token), std::move(args[0]), std::move(args[1]), std::move(args[2])));
    }
    else if (function == "IFERROR")
    {
        if (args.size() != 2)
            throw WrongArgumentCountException("IFERROR function takes exactly 2 arguments", op.token);

        output.emplace_back(new IfErrorExpression(pos(op.token), std::move(args[0]), std::move(args[1])));
    }
    else if (function == "IND")
    {
        if (args.size() != 1)
            throw WrongArgumentCountException("IND function takes exactly 1 argument", op.token);

        auto arg = dynamic_cast<Constant*>(args[0].get());
        if (arg == nullptr || arg->value.type() != Value::Type::INTEGER)
            throw ParseException("IND function should take a single constant integer argument", args[0]->position);

        int32_t indicator_id = arg->value.getInt();

        output.emplace_back(new IndicatorExpression(pos(op.token), indicator_id));
    }
    else if (function == "EXTIND")
    {
        if (args.size() < 1 || args.size() > 3)
            throw WrongArgumentCountException("EXTIND function takes one, two or three arguments", op.token);

        auto arg0 = dynamic_cast<Constant*>(args[0].get());
        if (arg0 == nullptr || arg0->value.type() != Value::Type::INTEGER)
            throw ParseException("EXTIND function should have a constant integer as first argument", args[0]->position);
        int32_t indicator_id = arg0->value.getInt();
        Date date_from{0, 0, 0};
        Date date_to{0, 0, 0};

        if (args.size() > 1)
        {
            auto arg1 = dynamic_cast<Constant*>(args[1].get());
            if (arg1 == nullptr || arg1->value.type() != Value::Type::DATE)
                throw ParseException("EXTIND function should have a constant date as second argument", args[1]->position);
            date_from = arg1->value.getDate();
        }

        if (args.size() > 2)
        {
            auto arg2 = dynamic_cast<Constant*>(args[2].get());
            if (arg2 == nullptr || arg2->value.type() != Value::Type::DATE)
                throw ParseException("EXTIND function should have a constant date as third argument", args[2]->position);
            date_to = arg2->value.getDate();
        }

        output.emplace_back(new ExtIndicatorExpression(pos(op.token), indicator_id, date_from, date_to));
    }
    else if (function == "AND")
    {
        if (args.size() < 2)
            throw WrongArgumentCountException("AND function takes two or more arguments", op.token);

        output.emplace_back(new AndFuncExpression(pos(op.token), std::move(args)));
    }
    else if (function == "OR")
    {
        if (args.size() < 2)
            throw WrongArgumentCountException("OR function takes two or more arguments", op.token);

        output.emplace_back(new OrFuncExpression(pos(op.token), std::move(args)));
    }
    else if (function == "SUM")
    {
        if (args.size() < 1)
            throw WrongArgumentCountException("SUM function takes at least one argument", op.token);

        output.emplace_back(new SumFuncExpression(pos(op.token), std::move(args)));
    }
    else if (function == "AVG")
    {
        if (args.size() < 1)
            throw WrongArgumentCountException("AVG function takes at least one argument", op.token);

        output.emplace_back(new AvgFuncExpression(pos(op.token), std::move(args)));
    }
    else if (function == "COUNT")
    {
        if (args.size() < 1)
            throw WrongArgumentCountException("COUNT function takes at least one argument", op.token);

        output.emplace_back(new CountFuncExpression(pos(op.token), std::move(args)));
    }
    else if (function == "CONTAINS")
    {
        if (args.size() != 2)
            throw WrongArgumentCountException("CONTAINS function takes exactly 2 arguments", op.token);

        output.emplace_back(new ContainsExpression(pos(op.token), std::move(args[0]), std::move(args[1])));
    }
    else if (function == "DATE")
    {
        if (args.size() != 1)
            throw WrongArgumentCountException("DATE function takes exactly 1 argument", op.token);

        auto arg = dynamic_cast<Constant*>(args[0].get());
        if (arg == nullptr || arg->value.type() != Value::STRING)
            throw ParseException("DATE function takes only constant string argument", args[0]->position);

        Date date;
        if (parseDate(arg->value.getString(), date) != 0)
            throw ParseException("Bad date format", args[0]->position);

        output.emplace_back(new Constant(pos(op.token), Value(date)));
    }
    else if (function == "EXTSCR")
    {
        if (args.size() != 1)
            throw WrongArgumentCountException("EXTSCR function takes exactly 1 argument", op.token);

        auto arg = dynamic_cast<Constant*>(args[0].get());
        if (arg == nullptr || arg->value.type() != Value::Type::INTEGER)
            throw ParseException("EXTSCR function should have a constant integer as first argument", args[0]->position);
        int32_t scorecard_id = arg->value.getInt();

        output.emplace_back(new ExtScrExpression(pos(op.token), scorecard_id));
    }
    else
    {
        throw UnknownFunctionException((std::string("Unknown function ") + function).c_str(), op.token);
    }
}

void Parser::pushOperator()
{
    Operator op = operator_stack.back();
    operator_stack.pop_back();

    if (op.type == Operator::LEFT_P || op.type == Operator::LEFT_CB)
        throw ParseException("Parentheses mismatch", op.token);

    if (op.type == Operator::UNARY)
    {
        if (output.empty())
            throw ParseException("Unary operator should have value", op.token);

        std::unique_ptr<Expression> expr = std::move(output.back());
        output.pop_back();

        if (op.token.type == Token::Type::PLUS)
        {
            output.push_back(std::move(expr));
        }
        else if (op.token.type == Token::Type::MINUS)
        {
            auto arg = dynamic_cast<Constant*>(expr.get());
            if (arg != nullptr && (arg->value.type() == Value::FLOAT || arg->value.type() == Value::INTEGER))
            {
                output.emplace_back(new Constant(pos(op.token), -arg->value));
            }
            else
            {
                output.emplace_back(new NegateExpression(pos(op.token), std::move(expr)));
            }
        }
    }
    else if (op.type == Operator::BINARY)
    {
        if (output.size() < 2)
            throw ParseException("Binary operator should have two operands", op.token);

        std::unique_ptr<Expression> right = std::move(output.back());
        output.pop_back();

        std::unique_ptr<Expression> left = std::move(output.back());
        output.pop_back();

        if (op.token.type == Token::Type::PLUS)
        {
            output.emplace_back(new SumExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::TIMES)
        {
            output.emplace_back(new MultiplyExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::DIVIDE)
        {
            output.emplace_back(new DivideExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::MINUS)
        {
            output.emplace_back(new DiffExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::MOD)
        {
            output.emplace_back(new ModExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::EQUAL)
        {
            output.emplace_back(new EqualExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::NOT_EQUAL)
        {
            output.emplace_back(new NotEqualExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::LESS)
        {
            output.emplace_back(new LessExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::LESS_OR_EQUAL)
        {
            output.emplace_back(new LessOrEqualExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::GREATER)
        {
            output.emplace_back(new GreaterExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::GREATER_OR_EQUAL)
        {
            output.emplace_back(new GreaterOrEqualExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::OR)
        {
            output.emplace_back(new OrExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::AND)
        {
            output.emplace_back(new AndExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::BITWISE_AND)
        {
            output.emplace_back(new AddPropertiesExpression(pos(op.token), std::move(left), std::move(right)));
        }
        else if (op.token.type == Token::Type::COMMA)
        {
            output.push_back(std::move(left));
            output.push_back(std::move(right));
        }
        else
        {
            throw ParseException("Unknown binary operator", op.token);
        }
    }
    else
    {
        throw ParseException("Unknown syntax element", op.token);
    }
}

void Parser::pushArray(const Operator& left_bracket)
{
    std::vector<std::unique_ptr<Expression>> values;

    while (!output.empty() && output.back() != nullptr)
    {
        values.push_back(std::move(output.back()));
        output.pop_back();
    }

    // pop nullptr marker
    if (output.empty() || output.back() != nullptr)
        throw ParseException("Internal parser error, array start not found", Token());
    output.pop_back();

    std::reverse(begin(values), end(values));
    output.emplace_back(new ArrayExpression(pos(left_bracket.token), std::move(values)));
}

int Parser::pos(const Token& token)
{
    return token.lexeme - formula;
}
