#ifndef FORMULA_PARSER_H
#define FORMULA_PARSER_H

#include <vector>
#include <string>
#include <cstdio>
#include <exception>
#include <algorithm>

#include "scanner.h"
#include "expression.h"
#include "exceptions.h"
#include "operators.h"

struct Operator
{
    enum Type
    {
        BINARY,
        UNARY,
        FUNCTION,
        VARIABLE,
        LEFT_P,
        LEFT_CB
    };

    Operator(Type type, const Token& token) : type(type), token(token) {}

    Type type;
    Token token;
};

class Parser
{
public:
    Parser() = default;

    std::unique_ptr<Expression> parse(const char* formula);

private:
    void flush();

    void parseToken(const Token &token);

    void parseForValue(const Token& token);

    void parseForOperator(const Token& token);

    void pushRightParenthesis(const Token& token);

    void pushRightCurlyBracket(const Token& token);

    void pushFunction();

    void pushOperator();

    void pushArray(const Operator& left_bracket);

    int pos(const Token& token);

private:
    bool waitingValue = true;
    std::vector<std::unique_ptr<Expression>> output;
    std::vector<Operator> operator_stack;
    const char* formula;
};

#endif //FORMULA_PARSER_H
