#include <sstream>
#include <algorithm>

#include "traits.h"
#include "operators.h"

std::string to_string(const Trait& trait)
{
    std::stringstream ss;
    ss << trait;
    return ss.str();
}

std::string to_string(const Traits& type_set)
{
    std::string result = "{";
    int i = 0;
    for (const auto& trait : type_set)
    {
        if(i != 0)
            result += ", ";
        result += to_string(trait);
        ++i;
    }
    result += "}";
    return std::move(result);
}

bool operator==(const Trait& left, const Trait& right)
{
    return left.type == right.type &&
           left.allowed_values == right.allowed_values &&
           left.values == right.values &&
           left.element_type == right.element_type;
}

std::ostream& operator<<(std::ostream& os, const Trait& trait)
{
    os << Value::typeName(trait.type);

    if (trait.type == Value::ARRAY || trait.type == Value::MULTI_SELECT)
        os << " of " << Value::typeName(trait.element_type);

    if (!trait.allowed_values.empty())
        os << " " << trait.allowed_values;

    if (!trait.values.empty())
        os << " " << trait.values;

    return os;
}

Trait::Trait(Value::Type type,
             const std::vector<Value>& allowed_values,
             const std::vector<Value>& values,
             Value::Type element_type) :
        type(type),
        element_type(element_type),
        allowed_values(allowed_values),
        values(values)
{
    for (const Value& value : allowed_values)
    {
        if (value.type() != Value::BLANK &&
            value.type() != Value::NOT_APPLICABLE &&
            value.type() != Value::NONE_OF_ABOVE)
        {
            allowed_values_limited = true;
            break;
        }
    }
}

void append_trait(Traits& traits, const Trait& trait)
{
    if (std::find(begin(traits), end(traits), trait) == traits.end())
        traits.push_back(trait);
}

void merge_traits(Traits& a, const Traits& b)
{
    for (const auto& b_trait : b)
        append_trait(a, b_trait);
}
