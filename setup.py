import os
from distutils.core import setup
import distutils.command.build as _build
import distutils.spawn as ds

package_name = 'supplyshift_formula'
package_dir = 'api/python/supplyshift_formula'

here = os.path.abspath(os.path.dirname(__file__))

about = {}
with open(os.path.join(here, package_dir, '__version__.py')) as f:
    exec(f.read(), about)


class Build(_build.build):
    def initialize_options(self):
        _build.build.initialize_options(self)

    def run(self):
        ds.spawn(['cmake', '.', '-DCMAKE_INSTALL_PREFIX=' + package_dir, '-DCMAKE_BUILD_TYPE=Release'])
        ds.spawn(['cmake', '--build', '.', '--config', 'Release'])
        ds.spawn(['ctest', '-V'])
        ds.spawn(['make', 'install'])
        _build.build.run(self)

setup(
    name=package_name,
    version=about['__version__'],
    author='Alexander Egorov',
    author_email='alexander.egorov@quantumsoft.ru',
    packages=['supplyshift_formula'],
    package_dir={
        'supplyshift_formula': package_dir,
    },
    package_data={
        'supplyshift_formula': ['*.so'],
    },
    install_requires=[
        'cmake',
        'pytest',
    ],
    cmdclass={'build': Build},
)
