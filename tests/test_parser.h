#ifndef FORMULA_TEST_PARSER_H
#define FORMULA_TEST_PARSER_H

#include <iostream>
#include <parser.h>
#include "utils.h"

template<typename EXCEPTION>
int checkParseFail(const char* formula_ptr)
{
    std::string formula_str(formula_ptr);
    size_t pos = formula_str.find('\?');
    if (pos != std::string::npos)
        formula_str = formula_str.erase(pos, 1);
    const char* formula = formula_str.c_str();


    Parser parser;
    try
    {
        std::unique_ptr<Expression> expression = parser.parse(formula);
        std::cout << formula << std::endl;
        std::cout << "Parser didn't failed" << "\n\n";
    }
    catch (const EXCEPTION& e)
    {
        if (pos != std::string::npos && e.position != pos)
        {
            std::cout << formula << " parser failed as expected (" << e.what() << "), but position is wrong " << e.position << ", expected " << pos << "\n\n";
            printAnnotation(e.position, e.length);
            return 1;
        }

        std::cout << formula << " parser failed as expected: " << e.what() << "\n";
        return 0;
    }
    catch (const std::exception& e)
    {
        std::cout << formula << "\nUnexpected exception: " << e.what() << "\n\n";
        return 1;
    }
    return 1;
}

int testParser()
{
    int result = 0;
    result += checkParseFail<ParseException>("");
    result += checkParseFail<ParseException>("-");
    result += checkParseFail<ParseException>("2+");
    result += checkParseFail<ParseException>("3}");
    result += checkParseFail<ParseException>("{3");
    result += checkParseFail<ParseException>("3)");
    result += checkParseFail<ParseException>("(3");
    result += checkParseFail<ParseException>(":");
    result += checkParseFail<ParseException>("#");
    result += checkParseFail<ParseException>("1E999");
    result += checkParseFail<ParseException>("1 == 1");
    result += checkParseFail<ParseException>("DATE(\?123)");
    result += checkParseFail<ParseException>("DATE(\?'')");
    result += checkParseFail<ParseException>("DATE(\?'30/30/30')");
    result += checkParseFail<ParseException>("DATE(\?'2/30/2017')");
    result += checkParseFail<ParseException>("DATE(\?'2/29/2017')");
    result += checkParseFail<ParseException>("DATE(\?'2/-1/2017')");
    result += checkParseFail<ParseException>("DATE(\?'2/0/2017')");
    result += checkParseFail<ParseException>("IND(\?IND(1))");
    result += checkParseFail<ParseException>("IND(\?'123')");
    result += checkParseFail<ParseException>("EXTIND(1, DATE(\?'2/29/2017'))");
    result += checkParseFail<ParseException>("EXTIND(1, DATE('10/10/2010'), \?3)");
    result += checkParseFail<ParseException>("EXTIND(1, \?3, DATE('10/10/2010'))");
    result += checkParseFail<ParseException>("EXTSCR(\?'123')");
    result += checkParseFail<WrongArgumentCountException>("DATE()");
    result += checkParseFail<WrongArgumentCountException>("DATE('10/10/2010', '10/10/2010')");
    result += checkParseFail<WrongArgumentCountException>("IND()");
    result += checkParseFail<WrongArgumentCountException>("IND(1, 2)");
    result += checkParseFail<WrongArgumentCountException>("EXTIND()");
    result += checkParseFail<WrongArgumentCountException>("EXTIND(1, DATE('10/10/2010'), DATE('10/10/2010'), 1)");
    result += checkParseFail<WrongArgumentCountException>("EXTSCR()");
    result += checkParseFail<WrongArgumentCountException>("EXTSCR(1, 2)");
    result += checkParseFail<WrongArgumentCountException>("IF()");
    result += checkParseFail<WrongArgumentCountException>("IF(IND(33)>0)");
    result += checkParseFail<WrongArgumentCountException>("IF(IND(33)>0, 22)");
    result += checkParseFail<WrongArgumentCountException>("IFERROR()");
    result += checkParseFail<WrongArgumentCountException>("IFERROR(IND(33)/IND(22))");
    result += checkParseFail<WrongArgumentCountException>("IFERROR(IND(33)/IND(22), 1, 2)");
    result += checkParseFail<WrongArgumentCountException>("SUM()");
    result += checkParseFail<WrongArgumentCountException>("AVG()");
    result += checkParseFail<WrongArgumentCountException>("COUNT()");
    result += checkParseFail<WrongArgumentCountException>("AND()");
    result += checkParseFail<WrongArgumentCountException>("OR()");
    result += checkParseFail<WrongArgumentCountException>("CONTAINS()");
    result += checkParseFail<WrongArgumentCountException>("CONTAINS({1})");
    result += checkParseFail<WrongArgumentCountException>("CONTAINS({1}, {1, 2}, {1, 2})");
    result += checkParseFail<UnknownFunctionException>("2 + \?test(123)");
    return result;
}

#endif //FORMULA_TEST_PARSER_H
