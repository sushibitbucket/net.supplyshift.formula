#ifndef FORMULA_STUB_DATA_PROVIDER_H
#define FORMULA_STUB_DATA_PROVIDER_H

#include <vector>
#include <data_provider.h>

class StubDataProvider : public DataProvider
{
public:
    struct Answer
    {
        int indicator_id;
        Value value;
        Date date;
    };

    StubDataProvider(std::initializer_list<Answer> answers = {}) : answers(std::move(answers)) {}

    Value getIndicatorValue(int indicator_id, int position) const override
    {
        for (const auto& answer : answers)
        {
            if (answer.indicator_id == indicator_id)
            {
                return answer.value;
            }
        }
        return Value();
    }

    Value getExtIndicatorValue(int indicator_id, Date from, Date to, int position) const override
    {
        std::vector<Value> values;
        for (const auto& answer : answers)
        {
            if (answer.indicator_id == indicator_id)
            {
                values.push_back(answer.value);
            }
        }
        return Value(values, Value::ARRAY);
    }

    Value getExtScorecardValue(int scorecard_id, int position) const override
    {
        return Value({Value::createInt(scorecard_id * 10),
                     Value::createInt(scorecard_id * 20),
                     Value::createInt(scorecard_id * 30)}, Value::ARRAY);
    }

private:
    std::vector<Answer> answers;
};

#endif //FORMULA_STUB_DATA_PROVIDER_H
