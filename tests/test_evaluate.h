#ifndef FORMULA_TEST_EVALUATE_H
#define FORMULA_TEST_EVALUATE_H

#include <iostream>
#include <typeinfo>
#include <parser.h>
#include <evaluator.h>
#include "stub_data_provider.h"
#include "utils.h"

int checkEvaluate(const char* formula, const Value& expected_result, uint8_t properties = 0,
                  std::initializer_list<StubDataProvider::Answer> answers = {})
{
    StubDataProvider data_provider(answers);
    Parser parser;
    try
    {
        std::unique_ptr<Expression> expression = parser.parse(formula);
        Value result = Evaluator::evaluate(*expression, &data_provider);
        std::cout << formula << " = " << result << std::endl;
        if (result.type() != expected_result.type())
        {
            std::cout << "Result type mismatch, got " << result.typeName() << ", expected " << expected_result.typeName() << "\n\n";
            return 1;
        }
        if (result != expected_result)
        {
            std::cout << "Incorrect result, got " << result << ", expected " << expected_result << "\n\n";
            return 1;
        }
        if (result.getProperties() != properties)
        {
            std::cout << "Incorrect properties, got " << result.getProperties() << ", expected " << properties << "\n\n";
            return 1;
        }
    }
    catch(const ParseException& e)
    {
        std::cout << e.what() << std::endl;
        std::cout << formula << std::endl;
        printAnnotation(e.position, e.length);
        std::cout << "\n";
        return 1;
    }
    catch(const EvaluateException& e)
    {
        std::cout << formula << "\n" << "Evaluation exception: " << e.what() << "\n\n";
        return 1;
    }
    return 0;
}

template<typename EXCEPTION>
int checkEvaluateFail(const char* formula, std::initializer_list<StubDataProvider::Answer> answers = {})
{
    StubDataProvider data_provider(answers);
    Parser parser;
    try
    {
        std::unique_ptr<Expression> expression = parser.parse(formula);
        Value result = Evaluator::evaluate(*expression, &data_provider);

        std::cout << formula << " = " << result << std::endl;
        std::cout << "Didn't failed" << "\n\n";
    }
    catch (const EXCEPTION& e)
    {
        std::cout << formula << " failed as expected: " << e.what() << "\n";
        return 0;
    }
    catch (const std::exception& e)
    {
        std::cout << formula << "\nUnexpected exception: " << e.what() << "\n\n";
        return 1;
    }
    return 1;
}

int checkDivideByZeroAnswer(const char* formula,
                            std::initializer_list<StubDataProvider::Answer> answers,
                            int32_t expected_indicator_id)
{
    StubDataProvider data_provider(answers);
    Parser parser;
    std::unique_ptr<Expression> expression = parser.parse(formula);
    try
    {
        Value result = Evaluator::evaluate(*expression, &data_provider);

        std::cout << formula << " = " << result << std::endl;
        std::cout << "Didn't failed" << "\n\n";
    }
    catch (const DivideByZeroException& e)
    {
        auto ind_expr = dynamic_cast<const IndicatorExpression*>(e.expression);
        if (ind_expr == nullptr)
        {
            std::cout << "Expected indicator_id " << expected_indicator_id << ", got null expression in exception" << "\n\n";
            return 1;
        }
        if (ind_expr->indicator_id != expected_indicator_id)
        {
            std::cout << "Expected indicator_id " << expected_indicator_id << ", got " << ind_expr->indicator_id << "\n\n";
            return 1;
        }
        std::cout << formula << " failed as expected: " << e.what() << "\n";
        return 0;
    }
    catch (const std::exception& e)
    {
        std::cout << formula << "\nUnexpected exception: " << e.what() << "\n\n";
        return 1;
    }
    return 1;
}

template<typename EXPECTED_LEFT, typename EXPECTED_RIGHT>
int checkInvalidOperator(const char* formula,
                         std::initializer_list<StubDataProvider::Answer> answers)
{
    StubDataProvider data_provider(answers);
    Parser parser;
    std::unique_ptr<Expression> expression = parser.parse(formula);
    try
    {
        Value result = Evaluator::evaluate(*expression, &data_provider);

        std::cout << formula << " = " << result << std::endl;
        std::cout << "Didn't failed" << "\n\n";
    }
    catch (const InvalidOperatorException& e)
    {
        auto left_expr = dynamic_cast<const EXPECTED_LEFT*>(e.left.getSource());
        if (left_expr == nullptr)
        {
            std::cout << formula << " wrong left node, expected " << typeid(EXPECTED_LEFT).name() << "\n\n";
            return 1;
        }

        auto right_expr = dynamic_cast<const EXPECTED_RIGHT*>(e.right.getSource());
        if (right_expr == nullptr)
        {
            std::cout << formula << " wrong right node, expected " << typeid(EXPECTED_RIGHT).name() << "\n\n";
            return 1;
        }

        std::cout << formula << " failed as expected: " << e.what() << "\n";
        return 0;
    }
    catch (const std::exception& e)
    {
        std::cout << formula << "\nUnexpected exception: " << e.what() << "\n\n";
        return 1;
    }
    return 1;
}


int testEvaluate()
{
    int result = 0;
    result += checkEvaluate("true", Value(true));
    result += checkEvaluate("false", Value(false));
    result += checkEvaluate("true || false", Value(true));
    result += checkEvaluate("BLANK", Value::Blank);
    result += checkEvaluate("NA", Value::NotApplicable);
    result += checkEvaluate("NOA", Value::NoneOfAbove);
    result += checkEvaluate("-50", Value::createInt(-50));
    result += checkEvaluate("-50.0", Value(-50.0));
    result += checkEvaluate("-50.54", Value(-50.54));
    result += checkEvaluate("-50+30", Value::createInt(-20));
    result += checkEvaluate("+50", Value::createInt(50));
    result += checkEvaluate("+50+30", Value::createInt(80));
    result += checkEvaluate("+50+30.5", Value(80.5));
    result += checkEvaluate("50 / 20", Value(2.5));
    result += checkEvaluate("50.0 / 20", Value(2.5));
    result += checkEvaluate("'hello world'", Value("hello world"));
    result += checkEvaluate("\"hello world\"", Value("hello world"));
    result += checkEvaluate("  123", Value::createInt(123));
    result += checkEvaluate("{11, 22, 33}", Value({Value::createInt(11), Value::createInt(22), Value::createInt(33)}, Value::ARRAY));
    result += checkEvaluate("{12, {34, 56, {7, 8}}}",
                            Value({Value::createInt(12), Value({Value::createInt(34), Value::createInt(56), Value({Value::createInt(7), Value::createInt(8)}, Value::ARRAY)}, Value::ARRAY)}, Value::ARRAY));

    // operator +
    result += checkEvaluate("3 + 5", Value::createInt(8));
    result += checkEvaluate("3.3 + 5", Value(8.3));
    result += checkEvaluate("-50 + 30", Value::createInt(-20));
    result += checkEvaluate("+50 + 30", Value::createInt(80));
    result += checkEvaluate("+50 + 30.3", Value(80.3));
    result += checkEvaluate("+50.5 + 30.3", Value(80.8));
    result += checkEvaluate("NA + 100", Value::NotApplicable);
    result += checkEvaluate("NA + 100.5", Value::NotApplicable);
    result += checkEvaluate("NA + false", Value::NotApplicable);
    result += checkEvaluate("NA + 'test'", Value::NotApplicable);
    result += checkEvaluate("NA + {1, 2}", Value::NotApplicable);
    result += checkEvaluate("NA + NOA", Value::NotApplicable);
    result += checkEvaluate("NA + BLANK", Value::NotApplicable);
    result += checkEvaluate("100 + NA", Value::NotApplicable);
    result += checkEvaluate("100.5 + NA", Value::NotApplicable);
    result += checkEvaluate("false + NA", Value::NotApplicable);
    result += checkEvaluate("'test' + NA", Value::NotApplicable);
    result += checkEvaluate("{1, 2} + NA", Value::NotApplicable);
    result += checkEvaluate("NOA + NA", Value::NotApplicable);
    result += checkEvaluate("BLANK + NA", Value::NotApplicable);

    // operator -
    result += checkEvaluate("3 - 5", Value::createInt(-2));
    result += checkEvaluate("3.3 - 5", Value(-1.7));
    result += checkEvaluate("-50 - 30", Value::createInt(-80));
    result += checkEvaluate("+50 - 30", Value::createInt(20));
    result += checkEvaluate("+50 - 30.3", Value(19.7));
    result += checkEvaluate("+50.5 - 30.3", Value(20.2));
    result += checkEvaluate("NA - 100", Value::NotApplicable);
    result += checkEvaluate("NA - 100.5", Value::NotApplicable);
    result += checkEvaluate("NA - false", Value::NotApplicable);
    result += checkEvaluate("NA - 'test'", Value::NotApplicable);
    result += checkEvaluate("NA - {1, 2}", Value::NotApplicable);
    result += checkEvaluate("NA - NOA", Value::NotApplicable);
    result += checkEvaluate("NA - BLANK", Value::NotApplicable);
    result += checkEvaluate("100 - NA", Value::NotApplicable);
    result += checkEvaluate("100.5 - NA", Value::NotApplicable);
    result += checkEvaluate("false - NA", Value::NotApplicable);
    result += checkEvaluate("'test' - NA", Value::NotApplicable);
    result += checkEvaluate("{1, 2} - NA", Value::NotApplicable);
    result += checkEvaluate("NOA - NA", Value::NotApplicable);
    result += checkEvaluate("BLANK - NA", Value::NotApplicable);

    // operator *
    result += checkEvaluate("3 * 5", Value::createInt(3 * 5));
    result += checkEvaluate("3.3 * 5", Value(3.3 * 5));
    result += checkEvaluate("-50 * 30", Value::createInt(-50 * 30));
    result += checkEvaluate("+50 * 30", Value::createInt(50 * 30));
    result += checkEvaluate("+50 * 30.3", Value(50 * 30.3));
    result += checkEvaluate("+50.5 * 30.3", Value(50.5 * 30.3));
    result += checkEvaluate("NA * 100", Value::NotApplicable);
    result += checkEvaluate("NA * 100.5", Value::NotApplicable);
    result += checkEvaluate("NA * false", Value::NotApplicable);
    result += checkEvaluate("NA * 'test'", Value::NotApplicable);
    result += checkEvaluate("NA * {1, 2}", Value::NotApplicable);
    result += checkEvaluate("NA * NOA", Value::NotApplicable);
    result += checkEvaluate("NA * BLANK", Value::NotApplicable);
    result += checkEvaluate("100 * NA", Value::NotApplicable);
    result += checkEvaluate("100.5 * NA", Value::NotApplicable);
    result += checkEvaluate("false * NA", Value::NotApplicable);
    result += checkEvaluate("'test' * NA", Value::NotApplicable);
    result += checkEvaluate("{1, 2} * NA", Value::NotApplicable);
    result += checkEvaluate("NOA * NA", Value::NotApplicable);
    result += checkEvaluate("BLANK * NA", Value::NotApplicable);

    // operator %
    result += checkEvaluate("5 % 2", Value::createInt(1));
    result += checkEvaluate("5.0 % 2", Value(1.0));
    result += checkEvaluate("5 % 2.0", Value(1.0));
    result += checkEvaluate("5.0 % 2.3", Value(0.4));
    result += checkEvaluate("5.0 % NA", Value::NotApplicable);
    result += checkEvaluate("NA % NA", Value::NotApplicable);
    result += checkEvaluate("NA % 5.0", Value::NotApplicable);

    // operator =, !=
    result += checkEvaluate("3 = 3", Value(true));
    result += checkEvaluate("3 = 3.0", Value(true));
    result += checkEvaluate("3.0 = 3.0", Value(true));
    result += checkEvaluate("3.0 = 3", Value(true));
    result += checkEvaluate("3 != 3", Value(false));
    result += checkEvaluate("3 != 3.0", Value(false));
    result += checkEvaluate("3.0 != 3.0", Value(false));
    result += checkEvaluate("3.0 != 3", Value(false));
    result += checkEvaluate("{11, 22, 33} = {11, 22, 33}", Value(true));
    result += checkEvaluate("{11, 22, 33} != {11, 33}", Value(true));
    result += checkEvaluate("NA = NA", Value(true));
    result += checkEvaluate("NOA = NOA", Value(true));
    result += checkEvaluate("BLANK = BLANK", Value(true));
    result += checkEvaluate("'Test' = 123", Value(false));
    result += checkEvaluate("BLANK = NA", Value(false));
    result += checkEvaluate("BLANK = 3", Value(false));
    result += checkEvaluate("DATE('10/10/2010') = DATE('10/10/2010')", Value(true));
    result += checkEvaluate("DATE('10/1/2010') = DATE('10/10/2010')", Value(false));
    result += checkEvaluate("DATE('9/10/2010') = DATE('10/10/2010')", Value(false));
    result += checkEvaluate("DATE('10/10/2005') = DATE('10/10/2010')", Value(false));

    // operators <, >, <=, >=
    result += checkEvaluate("1 < 2", Value(true));
    result += checkEvaluate("1 < 1", Value(false));
    result += checkEvaluate("1.0 < 1", Value(false));
    result += checkEvaluate("1 < 1.0", Value(false));
    result += checkEvaluate("1.0 < 1.0", Value(false));
    result += checkEvaluate("1 > 2", Value(false));
    result += checkEvaluate("2 > 1", Value(true));
    result += checkEvaluate("1.0 > 1", Value(false));
    result += checkEvaluate("2.0 > 1", Value(true));
    result += checkEvaluate("1 > 1.0", Value(false));
    result += checkEvaluate("1 > 0.5", Value(true));
    result += checkEvaluate("1.0 > 0.5", Value(true));
    result += checkEvaluate("1 <= 2", Value(true));
    result += checkEvaluate("2 <= 1", Value(false));
    result += checkEvaluate("2.0 <= 1", Value(false));
    result += checkEvaluate("1 >= 2", Value(false));
    result += checkEvaluate("2 >= 1", Value(true));
    result += checkEvaluate("2.0 >= 1", Value(true));

    result += checkEvaluate("1 % NA", Value::NotApplicable);
    result += checkEvaluate("IF(123 = 123, 333, 555)", Value::createInt(333));
    result += checkEvaluate("IF(123 = 12, 333, 555)", Value::createInt(555));
    result += checkEvaluate("123 + 12 + 100 * (2 - 17) + 50", Value::createInt(-1315));
    result += checkEvaluate("IF(50=50, 123, 456)", Value::createInt(123));
    result += checkEvaluate("IF(50!=50, 123, 456)", Value::createInt(456));
    result += checkEvaluate("{11, (22 + 2)*2, '123' = '45'}",
                            Value({Value::createInt(11), Value::createInt(48), Value(false)}, Value::ARRAY));
    result += checkEvaluate("IND(123)", Value::createInt(500), 0, {
            {123, Value::createInt(500)},
    });
    result += checkEvaluate("IF(IND(1)=10, IND(2)+IND(3), IND(2))", Value::createInt(50), 0, {
            {1, Value::createInt(10)},
            {2, Value::createInt(20)},
            {3, Value::createInt(30)},
    });
    result += checkEvaluate("(1 = 2) && true", Value(false));
    result += checkEvaluate("true && (1 = 2)", Value(false));
    result += checkEvaluate("AND(true, 1 = 2)", Value(false));
    result += checkEvaluate("OR(true, 1=2)", Value(true));
    result += checkEvaluate("OR(false, 1=2)", Value(false));
    result += checkEvaluate("OR(true, 2=2)", Value(true));
    result += checkEvaluate("OR(false, 2=2)", Value(true));
    result += checkEvaluate("SUM({1, 2, 3})", Value::createInt(6));
    result += checkEvaluate("SUM({1, 2, 3}, {4, 5}, {1, NA, BLANK})", Value::createInt(16));
    result += checkEvaluate("SUM({1, 2, 3}, 9, {1, NA, BLANK})", Value::createInt(16));
    result += checkEvaluate("AVG({1, 2, 2}, {1, 2, 10})", Value(3.0));
    result += checkEvaluate("COUNT({1, 2, 3})", Value::createInt(3));
    result += checkEvaluate("COUNT({1, 2, 3}, 5, {1, 2})", Value::createInt(6));
    result += checkEvaluate("COUNT({1, 'test', 3})", Value::createInt(3));
    result += checkEvaluate("COUNT({1, 'test', 3}, {NA, BLANK, NOA, 1, 2})", Value::createInt(7));
    result += checkEvaluate("SUM(EXTIND(5))", Value::createInt(60), 0, {
            {5, Value::createInt(10)},
            {5, Value::createInt(20)},
            {5, Value::createInt(30)},
    });
    result += checkEvaluate("COUNT(EXTIND(5))", Value::createInt(3), 0, {
            {5, Value::createInt(10)},
            {5, Value::createInt(20)},
            {5, Value::createInt(30)},
    });
    result += checkEvaluate("COUNT(EXTIND(5))", Value::createInt(3), 0, {
            {5, Value({Value("a"), Value("b")}, Value::MULTI_SELECT)},
            {5, Value({Value("c"), Value("d")}, Value::MULTI_SELECT)},
            {5, Value({Value("a"), Value("b")}, Value::MULTI_SELECT)},
            {5, Value::NotApplicable},
    });
    result += checkEvaluate("AND(2.2>2, 22>=22, NA=NA, 34!=44)", Value(true));
    result += checkEvaluate("OR(true, 2 / 0)", Value(true));
    result += checkEvaluate("(5.5+3.0)*5.5-(3/2.0)+5%2", Value(46.25));
    result += checkEvaluate("IFERROR(1/0, 123)", Value::createInt(123));
    result += checkEvaluate("IFERROR(1, 123)", Value::createInt(1));
    result += checkEvaluate("CONTAINS({1, 2}, {1, 2, 3})", Value(true));
    result += checkEvaluate("CONTAINS({1, 2}, {1})", Value(false));
    result += checkEvaluate("CONTAINS({1, 2}, 1)", Value(false));
    result += checkEvaluate("CONTAINS(1, 1)", Value(true));
    result += checkEvaluate("CONTAINS(1, {1, 2})", Value(true));
    result += checkEvaluate("CONTAINS({'a'}, IND(1))", Value(true), 0, {
            {1, Value({Value("a"), Value("b")}, Value::MULTI_SELECT)},
    });
    result += checkEvaluate("CONTAINS({'c'}, IND(1))", Value(false), 0, {
            {1, Value({Value("a"), Value("b")}, Value::MULTI_SELECT)},
    });
    result += checkEvaluate("IF(CONTAINS({'Russia', 'USA'}, {'Russia', 'USA', 'China'}), BLANK, NA)", Value::Blank);
    result += checkEvaluate("DATE('8/12/2017')", Value(Date{2017, 8, 12}));
    result += checkEvaluate("DATE('2/29/2020')", Value(Date{2020, 2, 29}));
    result += checkEvaluate("NA & HIDDEN", Value::NotApplicable, Value::HIDDEN);
    result += checkEvaluate("NA & USERINPUT", Value::NotApplicable, Value::USERINPUT);
    result += checkEvaluate("NA & USERINPUT & HIDDEN", Value::NotApplicable, Value::HIDDEN | Value::USERINPUT);
    result += checkEvaluate("EXTSCR(1) & HIDDEN", Value({Value::createInt(10), Value::createInt(20), Value::createInt(30)}, Value::ARRAY), Value::HIDDEN);
    result += checkEvaluate("IF(1=1, NA&HIDDEN, 123)", Value::NotApplicable, Value::HIDDEN);
    result += checkEvaluate("{} = {}", Value{true});
    result += checkEvaluate("(2+2)*4", Value::createInt(16));
    result += checkEvaluate("IND(1)", Value::createLocation(1, 2), 0, {
            {1, Value::createLocation(1, 2)},
    });
    result += checkEvaluate("IND(1)", Value::createGps(1.123, 2.234), 0, {
            {1, Value::createGps(1.123, 2.234)},
    });

    result += checkEvaluateFail<DivideByZeroException>("1 / 0");
    result += checkEvaluateFail<DivideByZeroException>("1 / 0.0");
    result += checkEvaluateFail<DivideByZeroException>("1 % 0");
    result += checkEvaluateFail<DivideByZeroException>("1.0 % 0.0");
    result += checkEvaluateFail<DivideByZeroException>("{1/0}");
    result += checkEvaluateFail<EvaluateException>("1 + '123'");
    result += checkEvaluateFail<EvaluateException>("1 - '123'");
    result += checkEvaluateFail<EvaluateException>("1 / '123'");
    result += checkEvaluateFail<EvaluateException>("1 < '123'");
    result += checkEvaluateFail<EvaluateException>("1 > '123'");
    result += checkEvaluateFail<EvaluateException>("-'1'");
    result += checkEvaluateFail<EvaluateException>("BLANK + BLANK");
    result += checkEvaluateFail<EvaluateException>("'123' - '10'");
    result += checkEvaluateFail<EvaluateException>("'123' / '10'");
    result += checkEvaluateFail<EvaluateException>("'123' % '10'");

    result += checkInvalidOperator<IndicatorExpression, Constant>("IND(7) + 3", {{7, Value::Blank}});
    result += checkInvalidOperator<IndicatorExpression, Constant>("IND(7) - 3", {{7, Value::Blank}});
    result += checkInvalidOperator<IndicatorExpression, Constant>("IND(7) * 3", {{7, Value::Blank}});
    result += checkInvalidOperator<IndicatorExpression, Constant>("IND(7) / 3", {{7, Value::Blank}});
    result += checkInvalidOperator<IndicatorExpression, Constant>("IND(7) % 3", {{7, Value::Blank}});
    result += checkInvalidOperator<Constant, IndicatorExpression>("3 + IND(7)", {{7, Value::Blank}});
    result += checkInvalidOperator<Constant, IndicatorExpression>("3 - IND(7)", {{7, Value::Blank}});
    result += checkInvalidOperator<Constant, IndicatorExpression>("3 * IND(7)", {{7, Value::Blank}});
    result += checkInvalidOperator<Constant, IndicatorExpression>("3 / IND(7)", {{7, Value::Blank}});
    result += checkInvalidOperator<Constant, IndicatorExpression>("3 % IND(7)", {{7, Value::Blank}});

    result += checkDivideByZeroAnswer("3 / IND(7)", {{7, Value::createInt(0)}}, 7);
    result += checkDivideByZeroAnswer("3 % IND(7)", {{7, Value::createInt(0)}}, 7);

    return result;
}

#endif //FORMULA_TEST_EVALUATE_H
