#ifndef TEST_FORMULA_EXCEPTIONS_H
#define TEST_FORMULA_EXCEPTIONS_H

#include <stdexcept>

class InvalidIndicatorIdException : public std::runtime_error
{
public:
    InvalidIndicatorIdException(const char* what, const int indicator_id, const int position) :
            std::runtime_error(what),
            indicator_id (indicator_id),
            position(position)
    {}

    const int indicator_id;
    const int position;
};

#endif //TEST_FORMULA_EXCEPTIONS_H
