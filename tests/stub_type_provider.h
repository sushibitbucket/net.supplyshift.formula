#ifndef FORMULA_STUB_TEST_PROVIDER_H
#define FORMULA_STUB_TEST_PROVIDER_H

#include <map>
#include <sstream>

#include <type_provider.h>
#include "exceptions.h"

class StubTypeProvider : public TypeProvider
{
public:
    explicit StubTypeProvider(const std::map<int, Trait>& map) : map(map)
    {}

    Trait getIndicatorInfo(int indicator_id, int position) const override
    {
        auto iter = map.find(indicator_id);
        if (iter == map.end())
        {
            std::stringstream ss;
            ss << "Invalid indicator id " << indicator_id;
            throw InvalidIndicatorIdException(ss.str().c_str(), indicator_id, position);
        }
        return iter->second;
    }

private:
    std::map<int, Trait> map;
};

#endif //FORMULA_STUB_TEST_PROVIDER_H
