#ifndef FORMULA_TEST_API_H
#define FORMULA_TEST_API_H

#include <cstdarg>
#include <map>
#include <string.h>
#include <stdio.h>
#include <api.h>
#include <value.h>

bool equals(value_t a, value_t b)
{
    if (a.type != b.type)
        return false;
    if (a.properties != b.properties)
        return false;
    switch (a.type)
    {
        case Value::NONE:
        case Value::BLANK:
        case Value::NONE_OF_ABOVE:
        case Value::NOT_APPLICABLE:
            return true;
        case Value::INTEGER:
            return a.int_value == b.int_value;
        case Value::STRING:
            return strcmp(a.str_value, b.str_value) == 0;
        case Value::BOOLEAN:
            return a.bool_value == b.bool_value;
        case Value::FLOAT:
            return a.float_value == b.float_value;
        case Value::ARRAY:
            if (a.array.size != b.array.size)
                return false;
            for (int i = 0; i < a.array.size; ++i)
            {
                if (!equals(a.array.values[i], b.array.values[i]))
                    return false;
            }
            return true;
        case Value::MULTI_SELECT:
            if (a.multi_select.size != b.multi_select.size)
                return false;
            for (int i = 0; i < a.multi_select.size; ++i)
            {
                if (!equals(a.multi_select.values[i], b.multi_select.values[i]))
                    return false;
            }
            return true;
        case Value::LOCATION:
            return a.location.country == b.location.country && a.location.province == b.location.province;
        case Value::GPS:
            return a.gps.latitude == b.gps.latitude && a.gps.longitude == b.gps.longitude;
        default:
            return false;
    }
}

value_t na_value(int8_t properties=0)
{
    value_t value;
    memset(&value, 0, sizeof(value_t));
    value.type = Value::NOT_APPLICABLE;
    value.properties = properties;
    return value;
}

value_t noa_value()
{
    value_t value;
    memset(&value, 0, sizeof(value_t));
    value.type = Value::NONE_OF_ABOVE;
    return value;
}

value_t blank_value()
{
    value_t value;
    memset(&value, 0, sizeof(value_t));
    value.type = Value::BLANK;
    return value;
}

value_t int_value(int number, int8_t properties=0)
{
    value_t value;
    memset(&value, 0, sizeof(value_t));
    value.type = Value::INTEGER;
    value.properties = properties;
    value.int_value = number;
    return value;
}

value_t str_value(const char* text)
{
    value_t value;
    memset(&value, 0, sizeof(value_t));
    value.type = Value::STRING;
    value.str_value = strdup(text);
    return value;
}

value_t array_value(int count, ...)
{
    value_t result;
    memset(&result, 0, sizeof(value_t));
    result.type = Value::ARRAY;
    result.array.values = (value_t*) allocate(count * sizeof(value_t));
    result.array.size = count;
    va_list args;
    va_start(args, count);
    for (int i = 0; i < count; ++i)
    {
        result.array.values[i] = va_arg(args, value_t);
    }
    va_end(args);
    return result;
}

value_t multi_select_value(int count, ...)
{
    value_t result;
    memset(&result, 0, sizeof(value_t));
    result.type = Value::MULTI_SELECT;
    result.multi_select.values = (value_t*) allocate(count * sizeof(value_t));
    result.multi_select.size = count;
    va_list args;
    va_start(args, count);
    for (int i = 0; i < count; ++i)
    {
        result.multi_select.values[i] = va_arg(args, value_t);
    }
    va_end(args);
    return result;
}

value_t location_value(int country, int province)
{
    value_t value;
    memset(&value, 0, sizeof(value_t));
    value.type = Value::LOCATION;
    value.location.country = country;
    value.location.province = province;
    return value;
}

void print_value(FILE* out, value_t* value)
{
    switch(value->type)
    {
        case Value::NONE:
            fprintf(out, "%s", "None");
            break;
        case Value::NONE_OF_ABOVE:
            fprintf(out, "%s", "NoneOfAbove");
            break;
        case Value::NOT_APPLICABLE:
            fprintf(out, "%s", "NotApplicable");
            break;
        case Value::BLANK:
            fprintf(out, "%s", "Blank");
            break;
        case Value::INTEGER:
            fprintf(out, "%lld", (long long)value->int_value);
            break;
        case Value::FLOAT:
            fprintf(out, "%f", value->float_value);
            break;
        case Value::STRING:
            fprintf(out, "\"%s\"", value->str_value);
            break;
        case Value::BOOLEAN:
            fprintf(out, "%s", value->bool_value != 0 ? "true" : "false");
            break;
        case Value::ARRAY:
            fprintf(out, "%s", "[");
            for (int32_t i = 0; i < value->array.size; ++i)
            {
                if (i != 0)
                    fprintf(out, "%s", ", ");
                print_value(out, value->array.values + i);
            }
            fprintf(out, "%s", "]");
            break;
        case Value::MULTI_SELECT:
            fprintf(out, "%s", "MULTI_SELECT [");
            for (int32_t i = 0; i < value->multi_select.size; ++i)
            {
                if (i != 0)
                    fprintf(out, "%s", ", ");
                print_value(out, value->multi_select.values + i);
            }
            fprintf(out, "%s", "]");
            break;
        case Value::LOCATION:
            fprintf(out, "LOCATION{%lld, %lld}", (long long)value->location.country, (long long)value->location.province);
            break;
        default:
            fprintf(out, "%s", "<NOT IMPLEMENTED TYPE>");
            break;
    }
    fprintf(out, ", properties=%d", value->properties);
}

std::map<int, value_t> g_values;

int checkApi(const char* formula, value_t expected, std::map<int, value_t> values = {})
{
    g_values = values;
    callbacks_t callbacks;
    memset(&callbacks, 0, sizeof(callbacks_t));
    callbacks.get_ind_value = [](int indicator_id, value_t* return_value){
        *return_value = g_values[indicator_id];
        return 0;
    };
    callbacks.get_indicator_info = [](int indicator_id, indicator_info_t* info){
        value_t& value = g_values[indicator_id];
        info->type = value.type;
        info->allowed_values = array_value(3, na_value(), noa_value(), blank_value()).array;
        if (info->type == Value::ARRAY)
            info->element_type = value.array.values[0].type;
        return 0;
    };
    void* interpreter = create_interpreter(&callbacks);

    printf("%s ", formula);

    parse_result_t parse_result;
    parse_formula(interpreter, formula, &parse_result);
    if (parse_result.error.code != 0)
    {
        printf("\nUnexpected parse error: %s\n\n", parse_result.error.message);
        destroy_error(&parse_result.error);
        destroy_interpreter(interpreter);
        destroy_value(&expected);
        return 1;
    }
    destroy_error(&parse_result.error);

    analyze_result_t analyze_result;
    indicator_id_t slave_indicator_id = 0;
    analyze_formula(interpreter, parse_result.ast, slave_indicator_id, &analyze_result);
    if (analyze_result.error.code != 0)
    {
        printf("\nUnexpected analyze error: %s\n\n", analyze_result.error.message);
        destroy_error(&analyze_result.error);
        destroy_ast(parse_result.ast);
        destroy_interpreter(interpreter);
        destroy_value(&expected);
        return 1;
    }
    destroy_error(&analyze_result.error);

    if (analyze_result.report.code != 0)
    {
        printf("\nUnexpected analyze failure: %s\nPosition: %d\n", analyze_result.report.message, analyze_result.report.annotation_offset);
        destroy_error(&analyze_result.report);
        return 1;
    }
    destroy_error(&analyze_result.report);

    evaluate_result_t evaluate_result;
    evaluate_formula(interpreter, parse_result.ast, &evaluate_result);
    if (evaluate_result.error.code != 0)
    {
        printf("\nUnexpected evaluate error: %s\n\n", evaluate_result.error.message);
        destroy_error(&evaluate_result.error);
        destroy_ast(parse_result.ast);
        destroy_interpreter(interpreter);
        destroy_value(&expected);
        return 1;
    }
    destroy_error(&evaluate_result.error);

    printf("= ");
    print_value(stdout, &evaluate_result.value);
    printf("\n");
    if (!equals(evaluate_result.value, expected))
    {
        printf("Expected value: ");
        print_value(stdout, &expected);
        printf("\n\n");
        return 1;
    }
    destroy_value(&evaluate_result.value);

    destroy_ast(parse_result.ast);
    destroy_interpreter(interpreter);
    destroy_value(&expected);
    destroy_value(&values[0]);
    return 0;
}

int checkCollectInd(const char* formula, std::vector<indicator_id_t> expected)
{
    callbacks_t callbacks = {nullptr, nullptr, nullptr, nullptr};
    void* interpreter = create_interpreter(&callbacks);

    parse_result_t parse_result;
    parse_formula(interpreter, formula, &parse_result);
    if (parse_result.error.code != 0)
    {
        printf("\nUnexpected parse error: %s\n", parse_result.error.message);
        destroy_error(&parse_result.error);
        destroy_interpreter(interpreter);
        return 1;
    }
    destroy_error(&parse_result.error);

    collect_result_t collect_result;
    collect_requirements(interpreter, parse_result.ast, 1, 0, 0, &collect_result);
    if (parse_result.error.code != 0)
    {
        printf("\nUnexpected parse error: %s\n", parse_result.error.message);
        destroy_error(&collect_result.error);
        destroy_interpreter(interpreter);
        destroy_ast(parse_result.ast);
        return 1;
    }
    destroy_error(&collect_result.error);

    if (collect_result.requirements.extind_count != 0)
    {
        printf("\nextind_count should be 0, got %d\n", collect_result.requirements.extind_count);
        return 1;
    }

    if (collect_result.requirements.extscr_count != 0)
    {
        printf("\nextscr_count should be 0, got %d\n", collect_result.requirements.extscr_count);
        return 1;
    }

    if (collect_result.requirements.ind_count != expected.size())
    {
        printf("\nind_count should be %zu, got %d\n", expected.size(), collect_result.requirements.ind_count);
        return 1;
    }

    if (memcmp(collect_result.requirements.ind, expected.data(), sizeof(indicator_id_t) * expected.size()) != 0)
    {
        printf("\nWrong ind collection\n");
        return 1;
    }

    destroy_ast(parse_result.ast);
    destroy_interpreter(interpreter);
    return 0;
}

int testApi()
{
    int result = 0;

    result += checkApi("2+2", int_value(4), {
            {0, int_value(0)},
    });
    result += checkApi("'hello world'", str_value("hello world"), {
            {0, str_value("slave")},
    });
    result += checkApi("IND(2)", int_value(123), {
            {0, int_value(0)},
            {2, int_value(123)},
    });
    result += checkApi("IND(2)", str_value("hello"), {
            {0, str_value("slave")},
            {2, str_value("hello")},
    });
    result += checkApi("IND(2)", array_value(2, str_value("hello"), str_value("world")), {
            {0, array_value(1, str_value("slave"))},
            {2, array_value(2, str_value("hello"), str_value("world"))},
    });
    result += checkApi("IND(1)", multi_select_value(2, str_value("hello"), str_value("world")), {
            {0, multi_select_value(2, str_value("hello"), str_value("world"))},
            {1, multi_select_value(2, str_value("hello"), str_value("world"))},
    });
    result += checkApi("IND(1)", location_value(123, 234), {
            {0, location_value(123, 234)},
            {1, location_value(123, 234)},
    });

    result += checkApi("IF(IND(1)>0,1,IF(IND(1)<0,-1,BLANK))&HIDDEN", int_value(-1, Value::HIDDEN), {
            {0, int_value(0)},
            {1, int_value(-10)},
    });

    result += checkCollectInd("IND(1) + EXTIND(1) + EXTSCR(2) + IND(3)", {1, 3});

    return result;
}

#endif //FORMULA_TEST_API_H
