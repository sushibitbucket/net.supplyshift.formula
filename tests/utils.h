#ifndef FORMULA_TEST_UTILS_H_H
#define FORMULA_TEST_UTILS_H_H

#include <stdio.h>
#include <exceptions.h>

void printAnnotation(int offset, int length = 0)
{
    for (int i = 0; i < offset; ++i)
        printf(" ");
    printf("^");
    for (int i = 1; i < length; ++i)
        printf("~");
    printf("\n");
}

#endif //FORMULA_TEST_UTILS_H_H
