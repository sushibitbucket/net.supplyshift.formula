#ifndef FORMULA_TEST_ANALYSIS_H
#define FORMULA_TEST_ANALYSIS_H

#include <iostream>
#include <parser.h>
#include <semantic_analyzer.h>
#include "stub_type_provider.h"
#include "utils.h"

int checkAnalysis(const char* formula, Traits expected_result, std::map<int, Trait> indicators = {})
{
    Parser parser;
    bool has_provider = !indicators.empty();
    StubTypeProvider stub_type_provider(std::move(indicators));
    try
    {
        std::unique_ptr<Expression> expression = parser.parse(formula);
        TypeProvider* type_provider = has_provider ? &stub_type_provider : nullptr;
        SemanticAnalyzer analyzer(type_provider);
        const int32_t slave_indicator_id = 0;
        Traits result;
        if (indicators.find(slave_indicator_id) != indicators.end())
            result = analyzer.analyze(*expression, slave_indicator_id);
        else
            result = analyzer.analyze(*expression);
        if (result != expected_result)
        {
            std::cout << formula << "\nAnalysis result type error: ";
            std::cout << to_string(result) << ", expected " << to_string(expected_result) << "\n\n";
            return 1;
        }
        std::cout << formula << " analyzed to " << to_string(result) << std::endl;
    }
    catch (const AnalysisException& e)
    {
        std::cout << formula << std::endl;
        printAnnotation(e.position);
        std::cout << "Unexpected exception: " << e.what() << "\n\n";
        return 1;
    }
    catch (const std::exception& e)
    {
        std::cout << formula << "\nUnexpected exception: " << e.what() << "\n\n";
        return 1;
    }
    return 0;
}

template<typename EXCEPTION>
int checkAnalysisFail(const char* formula_ptr, std::map<int, Trait> indicators = {{0, Value::INTEGER}})
{
    Parser parser;
    StubTypeProvider stub_type_provider(indicators);
    std::string formula_str(formula_ptr);
    size_t pos = formula_str.find('\?');
    if (pos != std::string::npos)
        formula_str = formula_str.erase(pos, 1);
    const char* formula = formula_str.c_str();
    try
    {
        std::unique_ptr<Expression> expression = parser.parse(formula);
        int32_t slave_indicator_id = 0;
        SemanticAnalyzer analyzer(&stub_type_provider);
        Traits result = analyzer.analyze(*expression, slave_indicator_id);

        std::cout << formula << std::endl << "Analyzer didn't failed" << "\n";
        std::cout << "Analyzed as " << to_string(result) << std::endl;
        for (const auto& kv : indicators)
        {
            std::cout << "IND " << kv.first << " trait: " << to_string(kv.second) << std::endl;
        }
        std::cout << "\n";
    }
    catch (const EXCEPTION& e)
    {
        if (pos != std::string::npos && e.position != pos)
        {
            std::cout << formula << " analyzer failed as expected (" << e.what() << "), but position is wrong " << e.position << ", expected " << pos << "\n";
            printAnnotation(e.position);
            std::cout << "\n";
            return 1;
        }
        std::cout << formula << " analyzer failed as expected: " << e.what() << "\n";
        return 0;
    }
    catch (const std::exception& e)
    {
        std::cout << formula << "\nUnexpected exception: " << e.what() << "\n\n";
        return 1;
    }
    return 1;
}

int testValidExpressions()
{
    int result = 0;
    result += checkAnalysis("123", {{Value::INTEGER, {}, {Value::createInt(123)}}});
    result += checkAnalysis("-3", {{Value::INTEGER, {}, {Value::createInt(-3)}}});
    result += checkAnalysis("DATE('2/20/2017')", {{Value::DATE, {}, {Value(Date{2017, 2, 20})}}});
    result += checkAnalysis("BLANK", {{Value::BLANK, {}, {Value::Blank}}});
    result += checkAnalysis("NA", {{Value::NOT_APPLICABLE, {}, {Value::NotApplicable}}});
    result += checkAnalysis("3+3", {{Value::INTEGER}});
    result += checkAnalysis("3+3.0", {{Value::FLOAT}});
    result += checkAnalysis("3-3.0", {{Value::FLOAT}});
    result += checkAnalysis("3/3.0", {{Value::FLOAT}});
    result += checkAnalysis("3/3", {{Value::FLOAT}});
    result += checkAnalysis("3*3", {{Value::INTEGER}});
    result += checkAnalysis("5 % 2", {{Value::INTEGER}});
    result += checkAnalysis("5.0 % 2", {{Value::FLOAT}});
    result += checkAnalysis("5 % 2.0", {{Value::FLOAT}});
    result += checkAnalysis("5.0 % 2.3", {{Value::FLOAT}});
    result += checkAnalysis("1 < 2", {{Value::BOOLEAN}});
    result += checkAnalysis("1 <= 2", {{Value::BOOLEAN}});
    result += checkAnalysis("1 > 2", {{Value::BOOLEAN}});
    result += checkAnalysis("1 >= 2", {{Value::BOOLEAN}});
    result += checkAnalysis("1 = 2", {{Value::BOOLEAN}});
    result += checkAnalysis("1 != 2", {{Value::BOOLEAN}});
    result += checkAnalysis("true || false", {{Value::BOOLEAN}});
    result += checkAnalysis("true && false", {{Value::BOOLEAN}});
    result += checkAnalysis("'test'", {{Value::STRING, {}, {Value("test")}}});
    result += checkAnalysis("{1, 2, 3}", {{Value::ARRAY, {}, {Value::createInt(1), Value::createInt(2), Value::createInt(3)}, Value::INTEGER}});
    result += checkAnalysis("{}", {{Value::ARRAY, {}, {}, Value::NONE}});
    result += checkAnalysis("AND(false, true)", {{Value::BOOLEAN}});
    result += checkAnalysis("OR(true, false)", {{Value::BOOLEAN}});
    result += checkAnalysis("SUM({1, 2}, {2, 3})", {{Value::FLOAT}});
    result += checkAnalysis("SUM({1.0, 2.0}, {2.0, 3.0})", {{Value::FLOAT}});
    result += checkAnalysis("AVG({1, 2}, {2, 3})", {{Value::FLOAT}});
    result += checkAnalysis("COUNT({1, 2}, {2, 3})", {{Value::INTEGER}});
    result += checkAnalysis("CONTAINS({1, 2}, {1, 2, 3})", {{Value::BOOLEAN}});
    result += checkAnalysis("NA & HIDDEN", {{Value::NOT_APPLICABLE, {}, {Value::NotApplicable}}});
    result += checkAnalysis("IF(true, 1, 2)", {
            {Value::INTEGER, {}, {Value::createInt(1)}},
            {Value::INTEGER, {}, {Value::createInt(2)}},
    });
    result += checkAnalysis("IF(true, 1.0, 2.0)", {
            {Value::FLOAT, {}, {Value(1.0f)}},
            {Value::FLOAT, {}, {Value(2.0f)}},
    });
    result += checkAnalysis("IFERROR(1, 2)", {
            {Value::INTEGER, {}, {Value::createInt(1)}},
            {Value::INTEGER, {}, {Value::createInt(2)}},
    });
    result += checkAnalysis("IFERROR(1.0, 2.0)", {
            {Value::FLOAT, {}, {Value(1.0f)}},
            {Value::FLOAT, {}, {Value(2.0f)}},
    });
    result += checkAnalysis("IND(1)", {{Value::INTEGER}}, {
            {1, {Value::INTEGER}},
    });
    result += checkAnalysis("IND(1)", {{Value::STRING}}, {
            {1, {Value::STRING}},
    });
    result += checkAnalysis("IND(1)", {
            {Value::STRING, {Value::NotApplicable}},
    }, {
            {1, {Value::STRING, {Value::NotApplicable}}},
    });
    result += checkAnalysis("IND(1) + 1", {{Value::INTEGER}}, {
            {1, {Value::INTEGER, {Value::Blank}}},
    });
    result += checkAnalysis("IND(1) + 1", {{Value::INTEGER}}, {
            {1, {Value::INTEGER, {Value::NoneOfAbove}}},
    });
    result += checkAnalysis("IND(1) + IND(2)", {{Value::INTEGER}}, {
            {1, {Value::INTEGER, {Value::Blank}}},
            {2, {Value::INTEGER, {Value::Blank}}},
    });
    result += checkAnalysis("IND(1) + 3", {{Value::INTEGER}}, {
            {1, {Value::INTEGER, {Value::NotApplicable}}},
    });
    result += checkAnalysis("EXTIND(1)", {{Value::ARRAY, {}, {}, Value::INTEGER}}, {
            {1, {Value::INTEGER}},
    });
    result += checkAnalysis("COUNT(EXTSCR(1))", {{Value::INTEGER}}, {
            {0, {Value::INTEGER}},
    });
    result += checkAnalysis("IF(true, NA, BLANK)", {
            {Value::NOT_APPLICABLE, {}, {Value::NotApplicable}},
            {Value::BLANK, {}, {Value::Blank}},
    }, {
            {0, {Value::FLOAT, {Value::Blank, Value::NotApplicable}}},
    });
    result += checkAnalysis("(IF(AND(IND(11)!=NA, IND(11)!=BLANK), IND(11), 3.14) + IFERROR(IND(22)/IND(33), 1)) * IF(OR(IND(44)=\"Cert1\",IND(44)=\"Cert2\"), 1, 200)", {{Value::FLOAT}, {Value::INTEGER}}, {
            {0, {Value::FLOAT, {Value::Blank}}},
            {11, {Value::INTEGER, {Value::NotApplicable, Value::Blank}}},
            {22, {Value::FLOAT, {Value::NotApplicable, Value::Blank}}},
            {33, {Value::FLOAT, {Value::NotApplicable, Value::Blank}}},
            {44, {Value::STRING, {Value::NotApplicable, Value::Blank}}},
    });
    result += checkAnalysis("SUM(IND(1))", {{Value::FLOAT}}, {
            {0, {Value::FLOAT}},
            {1, {Value::ARRAY, {}, {}, Value::FLOAT}},
    });
    result += checkAnalysis("IND(1) * 2", {{Value::FLOAT}}, {
            {0, {Value::ARRAY, {}, {}, Value::FLOAT}},
            {1, {Value::ARRAY, {}, {}, Value::FLOAT}},
    });
    result += checkAnalysis("IND(1) * 2", {{Value::FLOAT}}, {
            {0, {Value::ARRAY, {}, {}, Value::FLOAT}},
            {1, {Value::FLOAT}},
    });
    return result;
}

int testAllowedValues()
{
    int result = 0;

    result += checkAnalysis("BLANK = IND(1)", {{Value::BOOLEAN}}, {
            {0, {Value::BOOLEAN, {Value::Blank}}},
            {1, {Value::INTEGER, {Value::Blank}}},
    });
    result += checkAnalysis("IND(1) = NA", {{Value::BOOLEAN}}, {
            {0, {Value::BOOLEAN, {Value::Blank}}},
            {1, {Value::INTEGER, {Value::NotApplicable}}},
    });

    result += checkAnalysis("CONTAINS({'test', 'hello'}, IND(1))", {{Value::BOOLEAN}}, {
            {0, {Value::BOOLEAN, {Value::Blank}}},
            {1, {Value::ARRAY, {Value("test"), Value("hello"), Value("foo"), Value::Blank}, {}, Value::STRING}},
    });

    result += checkAnalysis("IF(IND(1)='test', 3, 2)", {
            {Value::INTEGER, {}, {Value::createInt(3)}},
            {Value::INTEGER, {}, {Value::createInt(2)}},
    }, {
            {0, {Value::INTEGER, {Value::Blank}}},
            {1, {Value::STRING, {Value("test"), Value("hello"), Value("foo")}}},
    });
    result += checkAnalysis("IF('test'=IND(1), 3, 2)", {
            {Value::INTEGER, {}, {Value::createInt(3)}},
            {Value::INTEGER, {}, {Value::createInt(2)}},
    }, {
            {0, {Value::INTEGER, {Value::Blank}}},
            {1, {Value::STRING, {Value("test"), Value("hello"), Value("foo")}}},
    });

    result += checkAnalysis("IF(IND(1)=123, 3, 2)", {
            {Value::INTEGER, {}, {Value::createInt(3)}},
            {Value::INTEGER, {}, {Value::createInt(2)}},
    }, {
            {0, {Value::INTEGER, {Value::Blank}}},
            {1, {Value::INTEGER, {Value::NotApplicable, Value::Blank}}},
    });
    result += checkAnalysis("IF(123=IND(1), 3, 2)", {
            {Value::INTEGER, {}, {Value::createInt(3)}},
            {Value::INTEGER, {}, {Value::createInt(2)}},
    }, {
            {0, {Value::INTEGER, {Value::Blank}}},
            {1, {Value::INTEGER, {Value::NotApplicable, Value::Blank}}},
    });

    result += checkAnalysis("IF(IND(1)=NA, 3, 2)", {
            {Value::INTEGER, {}, {Value::createInt(3)}},
            {Value::INTEGER, {}, {Value::createInt(2)}},
    }, {
            {0, {Value::INTEGER, {Value::Blank}}},
            {1, {Value::STRING, {Value::NotApplicable}}},
    });

    result += checkAnalysis("IF(IND(1)=NA, 3, 2)", {
            {Value::INTEGER, {}, {Value::createInt(3)}},
            {Value::INTEGER, {}, {Value::createInt(2)}},
    }, {
            {0, {Value::INTEGER, {Value::Blank}}},
            {1, {Value::STRING, {Value::NotApplicable, Value::Blank}}},
    });

    result += checkAnalysis("IF(IND(1)=IND(2), 3, 2)", {
            {Value::INTEGER, {}, {Value::createInt(3)}},
            {Value::INTEGER, {}, {Value::createInt(2)}},
    }, {
            {0, {Value::INTEGER, {Value::Blank}}},
            {1, {Value::STRING, {Value("test"), Value("hello")}}},
            {2, {Value::STRING, {}}},
    });

    result += checkAnalysis("IF(IND(1)=IND(2), 3, 2)", {
            {Value::INTEGER, {}, {Value::createInt(3)}},
            {Value::INTEGER, {}, {Value::createInt(2)}},
    }, {
            {0, {Value::INTEGER, {Value::Blank}}},
            {1, {Value::STRING, {Value("test"), Value("hello"), Value::NotApplicable}}},
            {2, {Value::STRING, {}}},
    });

    result += checkAnalysis("IF(IND(1)=IND(2), 3, 2)", {
            {Value::INTEGER, {}, {Value::createInt(3)}},
            {Value::INTEGER, {}, {Value::createInt(2)}},
    }, {
            {0, {Value::INTEGER, {Value::Blank}}},
            {1, {Value::STRING, {Value("test"), Value("hello"), Value::NotApplicable}}},
            {2, {Value::STRING, {Value("test"), Value("hello"), Value::NotApplicable}}},
    });

    result += checkAnalysis("IF(IND(1)=IND(2), 3, 2)", {
            {Value::INTEGER, {}, {Value::createInt(3)}},
            {Value::INTEGER, {}, {Value::createInt(2)}},
    }, {
            {0, {Value::INTEGER, {Value::Blank}}},
            {1, {Value::STRING, {Value("test"), Value("foo"), Value::NotApplicable}}},
            {2, {Value::STRING, {Value("bar"), Value("foo"), Value::NotApplicable}}},
    });

    result += checkAnalysis("CONTAINS(IND(1), {'test', 'hello'})", {{Value::BOOLEAN}}, {
            {0, {Value::BOOLEAN, {Value::Blank}}},
            {1, {Value::ARRAY, {Value("test"), Value("hello"), Value("foo")}, {}, Value::STRING}},
    });

    result += checkAnalysis("IND(1) = NA", {{Value::BOOLEAN}}, {
            {0, {Value::BOOLEAN, {Value::Blank}}},
            {1, {Value::ARRAY, {Value::NotApplicable}, {}, Value::STRING}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("IF(IND(1)\?='test', 3, 2)", {
            {0, {Value::INTEGER}},
            {1, {Value::STRING, {Value("hello"), Value("foo")}}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("IF(IND(1)\?=NA, 3, 2)", {
            {0, {Value::INTEGER}},
            {1, {Value::STRING}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("IF(IND(1)\?=NA, 3, 2)", {
            {0, {Value::INTEGER}},
            {1, {Value::STRING, {Value::Blank}}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("IF('test'\?=IND(1), 3, 2)", {
            {0, {Value::INTEGER}},
            {1, {Value::STRING, {Value("hello"), Value("foo")}}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("IF(IND(1)=BLANK, \?NA, 2)", {
            {0, {Value::INTEGER}},
            {1, {Value::STRING, {Value::Blank}}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("IF(IND(1)\?=BLANK, 1, 2)", {
            {0, {Value::INTEGER}},
            {1, {Value::STRING}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("CONTAINS({'test', 'hello', \?'foo'}, IND(1))", {
            {0, {Value::INTEGER}},
            {1, {Value::ARRAY, {Value("test"), Value("hello")}, {}, {Value::STRING}}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("CONTAINS(IND(1), {'test', 'hello', \?'foo'})", {
            {0, {Value::INTEGER}},
            {1, {Value::ARRAY, {Value("test"), Value("hello"), Value::Blank}, {}, {Value::STRING}}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("IF(IND(1)\?=IND(2), 3, 2)", {
            {0, {Value::INTEGER}},
            {1, {Value::STRING, {Value("test"), Value("foo")}}},
            {2, {Value::STRING, {Value("hello"), Value("bar")}}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("IF(IND(1)=IND(2), 'test', \?'hello')", {
            {0, {Value::STRING, {Value("test"), Value("foo")}}},
            {1, {Value::STRING, {}}},
            {2, {Value::STRING, {}}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("IF(IND(33)=NA, NA, IF(IND(33)='Yes', 'Yes', IF(IND(33)='No', \?'No', BLANK)))", {
            {0, {Value::STRING, {Value("Yes"), Value::NotApplicable}}},
            {33, {Value::STRING, {Value::NotApplicable}}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("IF(IND(33)=1, {'Russia', \?'USA'}, NA)", {
            {0, {Value::MULTI_SELECT, {Value("Russia"), Value("China"), Value::NotApplicable}, {}, Value::STRING}},
            {33, {Value::INTEGER, {Value::NotApplicable}}},
    });

    result += checkAnalysisFail<AllowedValuesMismatchException>("IND(1)=NA", {
            {0, {Value::BOOLEAN}},
            {1, {Value::NOT_APPLICABLE}},
    });

    return result;
}

int testTypeMismatch()
{
    int result = 0;
    result += checkAnalysisFail<TypeMismatchException>("5.0 \?% NA");
    result += checkAnalysisFail<TypeMismatchException>("NA \?% NA");
    result += checkAnalysisFail<TypeMismatchException>("NA \?% 5.0");
    result += checkAnalysisFail<TypeMismatchException>("NA \?> 5.0");
    result += checkAnalysisFail<TypeMismatchException>("NA \?>= 5.0");
    result += checkAnalysisFail<TypeMismatchException>("NA \?< 5.0");
    result += checkAnalysisFail<TypeMismatchException>("NA \?<= 5.0");
    result += checkAnalysisFail<TypeMismatchException>("IF(\?123, 1, 2)");
    result += checkAnalysisFail<TypeMismatchException>("IF(\?IND(33), 2, 3)", {
            {0, {Value::INTEGER}},
            {33, {Value::INTEGER}},
    });
    result += checkAnalysisFail<TypeMismatchException>("IND(33)\?='Yes'", {
            {0, {Value::INTEGER}},
            {33, {Value::INTEGER}},
    });
    result += checkAnalysisFail<TypeMismatchException>("AND(true, \?1, 2)");
    result += checkAnalysisFail<TypeMismatchException>("OR(true, \?1, 2)");
    result += checkAnalysisFail<TypeMismatchException>("SUM(\?'123')");
    result += checkAnalysisFail<TypeMismatchException>("SUM(\?123, 2)");
    result += checkAnalysisFail<TypeMismatchException>("SUM(IND(11), \?IND(22))", {
            {0, {Value::INTEGER}},
            {11, {Value::ARRAY}},
            {22, {Value::INTEGER}},
    });
    result += checkAnalysisFail<TypeMismatchException>("AVG(\?'123')");
    result += checkAnalysisFail<TypeMismatchException>("AVG(\?123, 2)");
    result += checkAnalysisFail<TypeMismatchException>("COUNT(\?'123')");
    result += checkAnalysisFail<TypeMismatchException>("COUNT(\?123, 2)");
    result += checkAnalysisFail<TypeMismatchException>("CONTAINS(\?'123', 2)");
    result += checkAnalysisFail<TypeMismatchException>("CONTAINS(\?123, 2)");
    result += checkAnalysisFail<TypeMismatchException>("CONTAINS(\?2, 2)");
    result += checkAnalysisFail<TypeMismatchException>("CONTAINS(\?2, {2})");
    result += checkAnalysisFail<TypeMismatchException>("CONTAINS({2}, \?2)");

    result += checkAnalysisFail<TypeMismatchException>("1 \?+ '123'");
    result += checkAnalysisFail<TypeMismatchException>("1 \?- '123'");
    result += checkAnalysisFail<TypeMismatchException>("1 \?/ '123'");
    result += checkAnalysisFail<TypeMismatchException>("-\?'1'");
    result += checkAnalysisFail<TypeMismatchException>("BLANK \?+ BLANK");
    result += checkAnalysisFail<TypeMismatchException>("'123' \?- '10'");
    result += checkAnalysisFail<TypeMismatchException>("'123' \?/ '10'");
    result += checkAnalysisFail<TypeMismatchException>("'123' \?% '10'");
    result += checkAnalysisFail<TypeMismatchException>("'123' \?% '10'");
    result += checkAnalysisFail<TypeMismatchException>("'123' \?* '10'");
    result += checkAnalysisFail<TypeMismatchException>("1 \?< '10'");
    result += checkAnalysisFail<TypeMismatchException>("1 \?> '10'");
    result += checkAnalysisFail<TypeMismatchException>("1 \?<= '10'");
    result += checkAnalysisFail<TypeMismatchException>("1 \?>= '10'");
    result += checkAnalysisFail<TypeMismatchException>("\?1 || '10'");
    result += checkAnalysisFail<TypeMismatchException>("true || \?'10'");
    result += checkAnalysisFail<TypeMismatchException>("\?1 && '10'");
    result += checkAnalysisFail<TypeMismatchException>("true && \?'10'");
    result += checkAnalysisFail<TypeMismatchException>("true & \?'10'");
    result += checkAnalysisFail<TypeMismatchException>("true & (2\?+2)");

    result += checkAnalysisFail<TypeMismatchException>("IND(1) \?= 'test'", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
    });
    result += checkAnalysisFail<TypeMismatchException>("IND(1) \?+ 'test'", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
    });

    result += checkAnalysisFail<TypeMismatchException>("NA \?+ IND(1)", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
    });
    result += checkAnalysisFail<TypeMismatchException>("NA \?+ IND(1)", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
    });

    result += checkAnalysisFail<TypeMismatchException>("NA \?* IND(1)", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
    });

    result += checkAnalysisFail<TypeMismatchException>("NA \?/ IND(1)", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
    });

    result += checkAnalysisFail<TypeMismatchException>("NA \?- IND(1)", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
    });

    result += checkAnalysisFail<TypeMismatchException>("IF(IND(1), 'test', 3) \?+ 2", {
            {0, {Value::INTEGER}},
            {1, {Value::BOOLEAN}},
    });

    result += checkAnalysisFail<TypeMismatchException>("IF(IND(1), 3, 'test') \?+ 2", {
            {0, {Value::INTEGER}},
            {1, {Value::BOOLEAN}},
    });

    result += checkAnalysisFail<TypeMismatchException>("2 + 2", {
            {0, {Value::STRING}},
    });
    result += checkAnalysisFail<TypeMismatchException>("IF(true, \?123, 'test')", {
            {0, {Value::STRING}},
    });
    result += checkAnalysisFail<TypeMismatchException>("IF(true, 123, \?'test')", {
            {0, {Value::INTEGER}},
    });
    return result;
}

int testArrayTypeMismatch()
{
    int result = 0;
    result += checkAnalysis("IND(1) = {'a', 'b', 'c'}", {{Value::BOOLEAN}}, {
            {1, {Value::ARRAY, {}, {}, Value::STRING}},
    });
    result += checkAnalysis("CONTAINS(IND(1), {'a', 'b', 'c'})", {{Value::BOOLEAN}}, {
            {1, {Value::ARRAY, {}, {}, Value::STRING}},
    });
    result += checkAnalysis("CONTAINS(EXTIND(1), {'a', 'b', 'c'})", {{Value::BOOLEAN}}, {
            {1, {Value::STRING}},
    });

    result += checkAnalysisFail<TypeMismatchException>("{1, 2, 3} \?= {'a', 'b', 'c'}");
    result += checkAnalysisFail<TypeMismatchException>("EXTIND(1) \?= {'a', 'b', 'c'}", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
    });
    result += checkAnalysisFail<TypeMismatchException>("{'a', 'b', 'c'} \?= EXTIND(1)", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
    });
    result += checkAnalysisFail<TypeMismatchException>("EXTIND(1) \?= EXTIND(2)", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
            {2, {Value::STRING}},
    });

    result += checkAnalysisFail<TypeMismatchException>("\?CONTAINS({1, 2, 3}, {'a', 'b', 'c'})");
    result += checkAnalysisFail<TypeMismatchException>("\?CONTAINS(EXTIND(1), {'a', 'b', 'c'})", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
    });
    result += checkAnalysisFail<TypeMismatchException>("\?CONTAINS({'a', 'b', 'c'}, EXTIND(1))", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
    });
    result += checkAnalysisFail<TypeMismatchException>("\?CONTAINS(EXTIND(1), EXTIND(2))", {
            {0, {Value::INTEGER}},
            {1, {Value::INTEGER}},
            {2, {Value::STRING}},
    });

    result += checkAnalysisFail<TypeMismatchException>("IND(1) \?= {'a', 'b', 'c'}", {
            {0, {Value::INTEGER}},
            {1, {Value::ARRAY, {}, {}, Value::INTEGER}},
    });
    return result;
}

int testAnalysis()
{
    int result = 0;

    result += testValidExpressions();
    result += testTypeMismatch();
    result += testArrayTypeMismatch();
    result += testAllowedValues();
    result += checkAnalysisFail<InvalidIndicatorIdException>("2 + \?IND(1)", {{0, {Value::INTEGER}}});
    result += checkAnalysisFail<InvalidIndicatorIdException>("2 + \?IND(-1)", {{0, {Value::INTEGER}}});

    return result;
}

#endif //FORMULA_TEST_ANALYSIS_H
