#include <iostream>
#include "test_parser.h"
#include "test_evaluate.h"
#include "test_analysis.h"
#include "test_api.h"

int main(int argc, char *argv[])
{
    int result = 0;
    int parserFailures = testParser();
    int evaluateFailures = testEvaluate();
    int analysisFailures = testAnalysis();
    int apiFailures = testApi();
    result = parserFailures + evaluateFailures + analysisFailures + apiFailures;
    std::cout << "\nTotal failures: " << result << std::endl;
    std::cout << "Parser: " << parserFailures << std::endl;
    std::cout << "Evaluator: " << evaluateFailures << std::endl;
    std::cout << "Analyzer: " << analysisFailures << std::endl;
    std::cout << "API: " << apiFailures << std::endl;
    return result;
}
