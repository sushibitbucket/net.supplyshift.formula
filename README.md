# SupplyShift Indicator Formula Math

## Overall architecture

Formulas math processing is performed in a several steps shown in a flowchart:
![Flowchart](docs/flowchart.png)

Every stage has it's own implementation in each layer:

| Stage                | C++ core         | C API                   | Python API                       |
| -------------        | ------------     | ------------            | ------------                     |
| Parse                | Parser           | parse_formula           | Interpreter.parse                |
| Validate             | SemanticAnalyzer | analyze_formula         | Interpreter.analyze              |
| Evaluate             | Evaluator        | evaluate_formula        | Interpreter.evaluate             |
| Collect Requirements | -                | collect_requirements    | Interpreter.collect_requirements |


## How to build
First install `CMake` (minimum version is 3.5) and `re2c`:
```
sudo apt install -y cmake re2c
```
And then build the library using cmake:
```
cmake . -DCMAKE_BUILD_TYPE=Release
make
```

Use `-DCMAKE_BUILD_TYPE=Debug` if you want to build debug version.

## How to build javascript wrapper
First install Emscripten. Then run:
```
emconfigure cmake .
emmake make
```

This command will generate `libformula.js` library.

Run `api/js/test.js` file to verify your build.

## How to install python package
To install python package you need only python and pip.
From local directory:
```
pip install .
```

This command will build the library, run tests and only then install `supplyshift_formula` package.

To verify your installation run the following code:
```python
from supplyshift_formula import Interpreter

interpreter = Interpreter()
ast = interpreter.parse('SUM({1, 2, 3, 4})')
result, properties = interpreter.evaluate(ast)
print(result)
```

If output is `10` then everything is ok!

## How to run tests
Unit tests can be runned with the following command:
```
ctest
```

If you want to run tests under valgrind you need to build debug version and cmake will automatically generate the test.

Use the following command to generate tests coverage (needs debug build):
```
make ctest_coverage
```

HTML report will be saved to ctest_coverage directory.

## How to run python tests and benchmarks
First you need to specify where library (so file) is located:
```
export SUPPLYSHIFT_FORMULA_DIR=<build_dir>/api/c
```

Then to run python API tests and benchmarks simply run:
```
pytest
```

To run coverage you need to install pytest-cov:
```
pip install pytest-cov
```
And then run
```
pytest --cov supplyshift_formula
```

It will output text report to the console:
```
Name                                            Stmts   Miss  Cover
-------------------------------------------------------------------
api/python/supplyshift_formula/__init__.py          5      0   100%
api/python/supplyshift_formula/api.py              92      0   100%
api/python/supplyshift_formula/conftest.py          0      0   100%
api/python/supplyshift_formula/constants.py        29      0   100%
api/python/supplyshift_formula/exceptions.py       27      0   100%
api/python/supplyshift_formula/interpreter.py     171     37    78%
api/python/supplyshift_formula/marshaling.py      177     17    90%
-------------------------------------------------------------------
TOTAL                                             501     54    89%
```

To generate HTML report, add `--cov-report=html`.

## How to run fuzzer

Prepare fuzzer: https://llvm.org/docs/LibFuzzer.html

fuzzer application will be built automatically if you are using clang compiler.

Run fuzz_formula.

## Question types and value types mapping

| Question Type  | Data Type    |
| -------------  | ------------ |
| Numeric        | FLOAT        |
| Altitude       | FLOAT        |
| Year           | INTEGER      |
| MultipleChoice | STRING       |
| TextResponse   | STRING       |
| YesNo          | STRING       |
| RwandaID       | STRING       |
| Date           | DATE         |
| Gps            | GPS          |
| Location       | LOCATION     |
| MultiSelect    | MULTI_SELECT |

Matrix columns should be mapped to ARRAY.
